$("input.hp").on("change", function (event) {
    const value = $(event.target)[0];
    const row = value.parentNode.parentElement;

    if (parseInt(value.value) === 0) {
        row.className = dead_creature_class;
    } else if (row.classList.contains(dead_creature_class)
        && parseInt(value.value) > 0) {
        row.className = active_creature_class;
    }
    // Update fight tracking
    update_fight(event);
});

$(".hp-modal-validate").on("click", function () {
    let hp_diff = parseInt($(this).parents(".modal-body").find(".hp-modal-value").val())
    if ($(this).data("reduce"))
        hp_diff = -1 * hp_diff // Remove hp

    const creature_row = $($(this).data("creature"))
    const hp_input = creature_row.find("input.hp")
    const current_hp = Math.max(parseInt(hp_input.val()) + hp_diff, 0)

    if (isNaN(current_hp))
        return
    hp_input.val(current_hp).change()
})

$(() => {
    $("i.fa-heartbeat").tooltip()
})
