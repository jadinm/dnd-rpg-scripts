let socket = io('/dm')

// Update initiative encoded by players

socket.on('update-initiative', data => {

    // Find corresponding row
    const rows = $('tbody.tracker-table-body').find('tr')
    for (let i = 0; i < rows.length; i++) {
        if (data.pc === get_alias(rows[i])) {
            // Update value of initiative
            rows[i].children[1].children[0].value = data.init
            break
        }
    }

    // Sort by initiative
    sort_rows(true)
});

// Toggle condition of players

socket.on('condition-toggle', data => {
    // Find corresponding row
    const rows = $('tbody.tracker-table-body').find('tr')
    for (let i = 0; i < rows.length; i++) {
        if (data.pc === get_alias(rows[i])) {
            // Update the condition
            let condition_elems = $(rows[i]).find(".condition");
            for (let j = 0; j < condition_elems.length; j++) {
                if (is_toggleable_active(condition_elems[j])
                    && data.condition === condition_elems[j].getAttribute("data-condition")
                    && !data.up) {
                    // Need to disable
                    disable_toggleable(condition_elems[j]);
                    break
                } else if (!is_toggleable_active(condition_elems[j])
                    && data.condition === condition_elems[j].getAttribute("data-condition")
                    && data.up) {
                    // Need to enable
                    enable_toggleable(condition_elems[j]);
                    break
                }
            }
            break
        }
    }
})

// Show messages from players

socket.on('player-message', data => {
    const base_toast = $("#elem-toast")
    const parent_container = base_toast.parent()

    // Change data and append toast

    const new_toast = base_toast.clone()
    new_toast.removeClass("d-none")
    new_toast.attr("id", null)
    parent_container.prepend(new_toast)

    new_toast.find("strong.toast-header-text")[0].textContent = data.message.header
    new_toast.find("img.rounded")[0].src = data.message.player
    new_toast.find("span.toast-body-text")[0].textContent = data.message.message

    // Remove auto hide
    new_toast.attr("data-autohide", false)

    // Show
    new_toast.toast({delay: 10000}).toast("show")
})

$('.toast').toast({delay: 10000})
