function get_alias(row) {
    return row.children[0].firstChild.data.trim();
}

function initiative(row) {
    let init = row.children[1].firstChild.data;
    init.replace("\n", "");
    init.replace(" ", "");
    init = parseInt(init);
    if (isNaN(init)) {
        init = row.children[1].children[0].value;
        init = parseInt(init);
    }
    return init;
}

const group_alias_regex = /(.*) (\d+)/;

function get_group(row) {
    const alias = get_alias(row);

    const match = alias.match(group_alias_regex);
    if (match != null) { // e.g., "Drow 1"
        return match[1];
    }
    return alias;
}

function get_group_index(row) {
    const alias = get_alias(row);

    const match = alias.match(group_alias_regex);
    if (match != null) { // e.g., "Drow 1"
        return match[2];
    }
    return 0;
}

function sort_rows(shuffle_ties) {
    let tbody = $('tbody.tracker-table-body');

    let initiatives = {};
    const rows = tbody.find('tr').not("." + invisible_class).not("." + dead_creature_class);
    for (let i = 0; i < rows.length; i++) {
        const init = initiative(rows[i]);
        init in initiatives || (initiatives[init] = []);

        const group_name = get_group(rows[i]);
        let existing_group = false;
        for (let j = 0; j < initiatives[init].length; j++) {
            if (group_name === initiatives[init][j].group) {
                initiatives[init][j].list.push(rows[i]);
                existing_group = true;
                break;
            }
        }
        if (!existing_group)
            initiatives[init].push({group: group_name, list: [rows[i]]});
    }

    // Shuffle ties (in the beginning of the fight)
    if (shuffle_ties) {
        Object.keys(initiatives).forEach((key, index) => {
            const array = initiatives[key];
            for (let i = array.length - 1; i > 0; i--) {
                const j = Math.floor(Math.random() * i);
                const temp = array[i];
                array[i] = array[j];
                array[j] = temp;
            }
        })
    } else { // Or slide the first creature to the last position (each round)
        Object.values(initiatives).forEach((value, index) => {
            const first_elem = value.shift();
            value.push(first_elem);
        })
    }

    tbody.find('tr').not("." + invisible_class).not("." + dead_creature_class).sort((a, b) => {
        const init_a = initiative(a);
        const init_b = initiative(b);
        if (init_a === init_b) { // Use indices of initiatives object
            const group_a = get_group(a);
            let group_index_a = -1;
            for (let i = 0; i < initiatives[init_a].length; i++) {
                if (group_a === initiatives[init_a][i].group) {
                    group_index_a = i;
                    break;
                }
            }
            const group_b = get_group(b);
            let group_index_b = -1;
            for (let i = 0; i < initiatives[init_b].length; i++) {
                if (group_b === initiatives[init_b][i].group) {
                    group_index_b = i;
                    break;
                }
            }
            if (group_index_a === group_index_b) { // Same group
                // e.g., "Drow 1" needs to go before "Drow 2"
                return get_group_index(a) - get_group_index(b);
            }
            return group_index_a - group_index_b;
        }
        return init_b - init_a;
    }).prependTo(tbody);
}

$("input.initiative").on("change", function (event) {
    // Update initiative order
    sort_rows(true);
    // Update fight tracking
    update_fight(event);
});

$("div.next-round").on("click", function (event) {
    let creatures = $("tr." + active_creature_class).not("." + invisible_class);

    let cur_initiative = 50;
    let closest_initiative = -50;
    let closest_idx = 0;

    // Find the current creature initiative and end its turn
    const currentPlayer = $("tr." + playing_creature_class);
    if (currentPlayer.length > 0) {
        cur_initiative = initiative(currentPlayer[0]);
        currentPlayer[0].className = played_creature_class;
    }

    // Reset all rows after a round (== no active creatures left)
    if (creatures.length === 0) {
        let old_creatures = $("tr." + played_creature_class);
        for (let i = 0; i < old_creatures.length; i++) {
            old_creatures[i].className = active_creature_class;
        }
        cur_initiative = 50;

        // Upgrade the round value
        const value = $("i.turn")[0].firstChild;
        const round = parseInt(value.data) + 1;
        value.data = " " + round;

        // Update initiative order (maybe in the future a random for ties)
        sort_rows(false);
    }
    creatures = $("tr." + active_creature_class).not("." + invisible_class);

    // Find the next player and begin its turn
    for (let i = 0; i < creatures.length; i++) {
        let init = initiative(creatures[i]);

        if (cur_initiative >= init && closest_initiative < init) {
            closest_initiative = init;
            closest_idx = i;
        }
    }
    creatures[closest_idx].className = playing_creature_class;

    // Add alert for lair actions if the initiative is just past the 20
    // because lair actions always lose to creature initiatives
    if ($("div.lair-actions").length > 0 && cur_initiative >= 20 && closest_initiative < 20) {
        const dialog = $("#lair-action-alert");
        dialog.modal('show');
    }

    // Update fight state for players
    update_fight(event);
});

// When starting fight
sort_rows(false)

$(() => {
    $(".condition").tooltip()
})
