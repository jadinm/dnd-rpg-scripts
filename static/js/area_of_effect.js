let rolls = {}

const AOE_DAMAGE_SEL = "#aoe-damage"
const AOE_DC_SEL = "#aoe-dc"
const AOE_SAVING_THROW = "#aoe-saving-throw"

const AOE_VALIDATE = ".aoe-validate"

function compute_creature_damage(row) {
    const creature_id = $(row).data("creature")
    if (!(creature_id in rolls)) {
        /* Find saving throw modifier */
        const saving_throw_id = $(AOE_SAVING_THROW).val()
        let modifier = parseInt($(row).data("saving-throw-" + saving_throw_id))
        if (isNaN(modifier))
            return

        /* Roll */
        rolls[creature_id] = []
        for (let i = 0; i < 4; i++) // 4 dices for combinations
            rolls[creature_id].push(roll_dice(1, 20, modifier))
    }

    /* Find the score */

    const second_roll = is_toggleable_active(row.find(".aoe-reroll"))
    const advantage = is_toggleable_active(row.find(".aoe-advantage"))
    const disadvantage = is_toggleable_active(row.find(".aoe-disadvantage"))

    const roll_index = second_roll ? 2 : 0
    let score
    if (advantage && !disadvantage) {
        score = Math.max(rolls[creature_id][roll_index], rolls[creature_id][roll_index + 1])
    } else if (disadvantage && !advantage) {
        score = Math.min(rolls[creature_id][roll_index], rolls[creature_id][roll_index + 1])
    } else {
        score = rolls[creature_id][roll_index]
    }

    /* Mark success or failure */
    const difficulty = parseInt($(AOE_DC_SEL).val())
    if (score >= difficulty)
        row.find(".aoe-score").removeClass("text-danger").addClass("text-success").text(score)
    else
        row.find(".aoe-score").addClass("text-danger").removeClass("text-success").text(score)

    /* Compute damage if any */
    let damage = parseInt($(AOE_DAMAGE_SEL).val())
    if (score >= difficulty)
        damage = Math.floor(damage / 2)
    if (is_toggleable_active(row.find(".aoe-resistance")))
        damage = Math.floor(damage / 2)
    if (damage === 0)
        row.find(".aoe-damage").addClass("d-none")
    else
        row.find(".aoe-damage").removeClass("d-none")
    row.find(".aoe-damage-value").text("-" + damage)
}

$(".aoe-resistance").tooltip().on("click", function (_) {
    compute_creature_damage($(this).parents(".aoe-row"))
})

$(".aoe-reroll").tooltip().on("click", function (_) {
    compute_creature_damage($(this).parents(".aoe-row"))
})

$(".aoe-advantage").tooltip().on("click", function (_) {
    compute_creature_damage($(this).parents(".aoe-row"))
})

$(".aoe-disadvantage").tooltip().on("click", function (_) {
    compute_creature_damage($(this).parents(".aoe-row"))
})

$(AOE_DAMAGE_SEL).on("change", _ => {
    $(".aoe-row").each((i, elem) => {
        compute_creature_damage($(elem))
    })
})

$(AOE_DC_SEL).on("change", _ => {
    $(".aoe-row").each((i, elem) => {
        compute_creature_damage($(elem))
    })
})

$(AOE_SAVING_THROW).on("changed.bs.select", _ => {
    rolls = {} // Clearing rolls will force new rolls
    $(".aoe-row").each((i, elem) => {
        compute_creature_damage($(elem))
    })
    $(AOE_VALIDATE).removeAttr("disabled")
})

$("#aoe-redo").on("click", _ => {
    rolls = {} // Clearing rolls will force new rolls
    $(".aoe-row").each((i, elem) => {
        compute_creature_damage($(elem))
    })
    $(AOE_VALIDATE).removeAttr("disabled")
})

$(AOE_VALIDATE).on("click", function (_) {
    const row = $(this).parents(".aoe-row")
    const creature_row = $($(row).data("creature"))

    /* Apply damage */
    const damage = parseInt(row.find(".aoe-damage-value").text())
    if (damage !== 0) {
        const hp_input = creature_row.find("input.hp")
        const current_hp = Math.max(parseInt(hp_input.val()) + damage, 0)
        hp_input.val(current_hp).change()
    }

    /* Apply condition */
    if (row.find(".aoe-score").hasClass("text-danger")) { // Failed the saving throw
        $(".aoe-conditions .condition").each((i, elem) => {
            if (is_toggleable_active(elem)) {
                const condition = creature_row.find("img[src='" + $(elem).attr("src") + "']")
                if (!is_toggleable_active(condition)) {
                    condition.click()
                }
            }
        })
    }

    /* Do not apply twice */
    this.setAttribute("disabled", "disabled")
})
