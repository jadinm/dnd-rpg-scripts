const invisible_class = "d-none"

// Needed for copy on clipboard on click to work
const clipboard = new ClipboardJS('.copy-text');

// Set the right version of bootstrap for bootstrap-select
$.fn.selectpicker.Constructor.BootstrapVersion = '4';

// Toggleable

function is_toggleable_active(elem) {
    return getComputedStyle($(elem)[0]).opacity === "1";
}

function enable_toggleable(elem) {
    $(elem)[0].style.opacity = "1";
}

function disable_toggleable(elem) {
    $(elem)[0].style.opacity = "0.2";
}

$(".toggleable").on("click", function (event) {
    const target = $(event.target)[0];
    if (is_toggleable_active(target)) {
        disable_toggleable(target);
    } else {
        enable_toggleable(target);
    }

    if (target.classList.contains("condition")) {
        // A new condition was applied => sync player fight tracking
        update_fight(event);
    }
});

// Tracker variables

const dead_creature_class = "table-active"
const active_creature_class = "table-secondary"
const played_creature_class = "table-light"
const playing_creature_class = "table-info"

// Tooltips
$(() => {
   $(".nav-link [title]").tooltip()
})
