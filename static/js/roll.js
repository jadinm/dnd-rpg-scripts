// Dice handling

function roll_dice(number, type, modifier) {
    let sum = 0;
    for (let i = 0; i < number; i++) {
        let random = Math.random();
        sum += Math.floor(random * type) + 1;
    }
    sum = sum + modifier;
    return sum;
}

function mark_critical_rolls(selector, raw_roll) {
    selector.removeClass("text-success text-danger");
    if (raw_roll === 1) {
        const cls = "text-danger";
        selector.addClass(cls);
        return cls;
    } else if (raw_roll === 20) {
        const cls = "text-success";
        selector.addClass(cls);
        return cls;
    }
    return "";
}

/**
 * Play between [number, 10] random sounds of dices
 */
function play_audio_sound(number) {
    // Get all dice audio
    const audio_base = $('.audio-roll');
    for (let i = 0; i < number && i < 10; i++) {
        setTimeout(function () {
            // Generate random number in [0, len(audio_base)-1]
            const rand = Math.floor(Math.random() * audio_base.length);
            audio_base[rand].cloneNode(true).play();
        }, 200 * i);
    }
}

function roll_dice_type(dice, change_top_block = true) {
    // Compute the random number
    const number = parseInt(dice.attr("number"));
    const type = parseInt(dice.attr("type"));
    const modifier = parseInt(dice.attr("modifier"));

    const dice1 = roll_dice(number, type, modifier);
    const dice2 = roll_dice(number, type, modifier);

    let max_dice;
    let min_dice;
    if (dice1 <= dice2) {
        max_dice = dice2;
        min_dice = dice1;
    } else {
        max_dice = dice1;
        min_dice = dice2;
    }

    // Show it next to the stat title
    const span = change_top_block ? dice.closest("div.creature-stat").find("span.creature-roll") : $();
    const adv = change_top_block ? dice.closest("div.creature-stat").find("span.creature-roll-adv") : $();
    const dis = change_top_block ? dice.closest("div.creature-stat").find("span.creature-roll-dis") : $();
    let text = "Roll: " + dice1.toString();
    span.text(text);
    if (span.length > 0)
        span[0].classList.remove("text-success", "text-danger");

    if (type === 20) {
        const base_class = mark_critical_rolls(span, dice1 - modifier);
        adv.text("(Avantage: " + max_dice.toString() + ")");
        const adv_class = mark_critical_rolls(adv, max_dice - modifier);
        dis.text("(Désavantage: " + min_dice.toString() + ")");
        const dis_class = mark_critical_rolls(dis, min_dice - modifier);
        text = "<span class='" + base_class + "'>" + text + "</span>"
            + "<br/><span class='" + adv_class + "'> Avantage: " + max_dice.toString() + "</span>"
            + "<br/><span class='" + dis_class + "'>Désavantage: " + min_dice.toString() + "</span>";
        dice.attr("data-original-title", text);
        dice.attr("title", text);
    } else {
        adv.text("(Critical hit: " + (dice1 + dice2).toString() + ")");
        dis.text("");
        text += "<br/><span class='text-success'> Critical hit: " + (dice1 + dice2).toString() + "</span>";
        dice.attr("data-original-title", text);
        dice.attr("title", text);
    }
    dice.tooltip();

    play_audio_sound(number);
    return text;
}

$("i.dice").tooltip().on("click", function (event) {
    roll_dice_type($(event.target));
});
