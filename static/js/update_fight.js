// Getters

function get_conditions(row) {
    let condition_elems = $(row).find(".condition");
    let conditions = [];
    for (let i = 0; i < condition_elems.length; i++) {
        if (is_toggleable_active(condition_elems[i]))
            conditions.push(condition_elems[i].getAttribute("data-condition"));
    }
    return conditions;
}

function get_visibility(row) {
    let checkbox = $(row).find("input[type='checkbox']");
    return checkbox.prop("checked");
}

function get_activity(row) {
    return row.classList.contains(playing_creature_class);
}

function get_hp(row) {
    let hp = $(row).find("input.hp").val();
    return parseInt(hp);
}

function get_round() {
    const value = $("i.turn")[0].firstChild;
    return parseInt(value.data);
}

// Update the expanded uses of a set of abilities

const expandable_abilities = [
    "traits", "spells", "actions", "reactions",
    "variant", "legendary_actions", "mythic-actions", "lair_actions", "regional_effects"
]

function update_trait_expanded_uses(creature_row, selector) {
    const creature_stat_block = $($(creature_row).children().first().data("target"))
    return creature_stat_block.find(selector).filter((_, elem) => {
        return !is_toggleable_active(elem);
    }).length;
}

function update_expanded_uses(creature_row, creature, ability_list_name) {
    for (let i = 0; i < (creature[ability_list_name] || []).length; i++) {
        if ((creature[ability_list_name][i].limit || 0) === 0)
            continue;

        // i + 1 because jinja starts counters at 1
        const selector = "[data-" + ability_list_name + "-index='" + (i + 1) + "']";
        creature[ability_list_name][i].expanded = update_trait_expanded_uses(creature_row, selector);
    }
}

function update_trait_spells(creature_row, creature) {
    const creature_stat_block = $($(creature_row).children().first().data("target"))
    for (let ability of expandable_abilities) {
        if (ability === "spells")
            continue;
        for (let i = 0; i < (creature[ability] || []).length; i++) {
            if ((creature[ability][i].limit || 0) === 0 && !("spells" in creature[ability][i]))
                continue;
            for (let j = 0; j < (creature[ability][i].spells || []).length; j++) {
                creature[ability][i].spells[j].expanded = creature_stat_block.find("[data-" + ability + "-index-" + (i + 1) + "-spells-index='" + (j + 1) + "']").filter((_, elem) => {
                    return !is_toggleable_active(elem);
                }).length;
            }
        }
    }
}

// Update player view of the fight

function update_fight(event) {
    const tbody = $('tbody.tracker-table-body');
    const creature_rows = tbody.find("tr").not("." + invisible_class).not("." + dead_creature_class);

    let post_data = {};
    let new_creatures = [];
    for (let i = 0; i < creature_rows.length; i++) {
        const alias = get_alias(creature_rows[i]);
        for (let j = 0; j < creatures.length; j++) {
            if (creatures[j].alias === alias) { // Found creature to update
                creatures[j].initiative = initiative(creature_rows[i]);
                creatures[j].conditions = get_conditions(creature_rows[i]);
                creatures[j].visible = get_visibility(creature_rows[i]);
                creatures[j].active = get_activity(creature_rows[i]);
                creatures[j].hp = get_hp(creature_rows[i]);

                // Record remaining limits
                if ("legendary_actions_limit" in creatures[j] && creatures[j].legendary_actions_limit !== 0)
                    creatures[j].legendary_actions_expanded = update_trait_expanded_uses(creature_rows[i], "[data-legendary-actions='true']");
                for (let ability of expandable_abilities)
                    update_expanded_uses(creature_rows[i], creatures[j], ability);
                update_trait_spells(creature_rows[i], creatures[j]);
                new_creatures.push(creatures[j]);
                break;
            }
        }
    }
    post_data.creatures = new_creatures;
    post_data.round = get_round();

    const update_fight_url = $(".update-fight-url")[0].href;
    $.ajax({
        type: "POST",
        url: update_fight_url,
        data: JSON.stringify(post_data),
        contentType: "application/json; charset=utf-8",
        dataType: "json"
    });
}

// Whenever the name of a custom creature is changed
$("input.custom-npc").on("change", function (event) {
    update_fight(event);
});

// Whenever the visibility of a creature is changed

$(".visibility").on("click", function (event) {
    update_fight(event);
});

// Select or deselect all pnj checkbox

$("input.select-all-pnj").on("click", function (event) {
    // Copy value to all pnj checkboxes
    const checkboxes = $("input.select-pnj");
    for (let i = 0; i < checkboxes.length; i++) {
        checkboxes[i].checked = event.target.checked;
    }
    update_fight(event);
});

// Expand a use of an ability or spell

$(".creature-stat .fa-square.toggleable").on("click", function (event) {
    update_fight(event);
})

// Start fight for players

$(document).ready(function () {
    update_fight(null);
});
