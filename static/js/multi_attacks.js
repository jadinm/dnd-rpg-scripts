const MULTI_ROLL_NUM_SEL = "#multi-rolls-num"
const MULTI_ROLL_TEXT_SEL = "#multi-rolls-text"
const MULTI_ROLL_RESULTS = "#multi-rolls-results"
const MULTI_ROLL_MODAL = "#multi-rolls"


function do_multiple_rolls(nbr_rolls, reroll = false) {
    let source = $(MULTI_ROLL_TEXT_SEL).children().first()
    let list = $(MULTI_ROLL_RESULTS)

    if (reroll)
        list.children().remove()

    for (let i = list.children().length; i < nbr_rolls; i++) {
        const clone = source.clone(false, false)
        const list_elem = $("<li class=\"list-group-item mt-5\"></li>")
        list_elem.append(clone)
        list.append(list_elem)
        // We reverse so the first tooltip in the text (likely the roll to hit) is always visible
        $(clone.find(".dice").get().reverse()).each((i, elem) => {
            // Roll all the dices
            roll_dice_type($(elem), false)
            $(elem).tooltip({trigger: "manual", placement: "bottom"}).tooltip('show')
        })
    }

    // Remove if more rolls than needed
    for (let i = list.children().length; i > nbr_rolls; i--) {
        const elem = list.children().last()
        elem.find(".dice").tooltip("hide")
        elem.remove()
    }
}


function show_multiple_roll(event) {

    // Fill in variables
    let hit_dice = $(event.target)
    const parent = hit_dice.parent()
    $(MULTI_ROLL_TEXT_SEL).children().remove()
    $(MULTI_ROLL_TEXT_SEL).append(parent.clone(false, false))
    $(MULTI_ROLL_RESULTS).children().remove()

    // Show modal
    $(MULTI_ROLL_MODAL).one("shown.bs.modal", _ => {
        // Roll
        do_multiple_rolls($(MULTI_ROLL_NUM_SEL).val())
    }).modal()
}

$(MULTI_ROLL_NUM_SEL).on("change", event => {
    // Changing the number of rolls
    do_multiple_rolls($(event.target).val())
})

$("#multi-rolls-redo").on("click", _ => {
    // Reroll without changing the sentence
    do_multiple_rolls($(MULTI_ROLL_NUM_SEL).val(), true)
})

$(".dice[type]").on("contextmenu", event => {
    event.preventDefault()
    show_multiple_roll(event)
})

$(MULTI_ROLL_MODAL).on("hidden.bs.modal", event => {
    // Since the scroll is lost on hide, hide all parent modals
    $(".modal").each((i, elem) => {
        if (elem.id !== event.target.id) {
            $(elem).modal("hide")
        }
    })
})
