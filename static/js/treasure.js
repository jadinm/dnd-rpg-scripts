const gem_numbers = $("#gem-number")
const gem_list = $("#gem-list")

const gems = [
    ["Azurite", "opaque mottled deep blue"],
    ["Banded agate", "translucent striped brown, blue, white, or red"],
    ["Blue quartz", "transparent pale blue"],
    ["Eye agate", "translucent circles of gray, white, brown, blue, or green"],
    ["Hematite", "opaque gray-black"],
    ["Lapis lazuli", "opaque light and dark blue with yellow flecks"],
    ["Malachite", "opaque striated light and dark green"],
    ["Moss agate", "translucent pink or yellow-white with mossy gray or green markings"],
    ["Obsidian", "opaque black"],
    ["Rhodochrosite", "opaque light pink"],
    ["Tiger eye", "translucent brown with golden center"],
    ["Turquoise", "opaque light blue-green"],
    ["Bloodstone", "opaque dark gray with red flecks"],
    ["Carnelian", "opaque orange to red-brown"],
    ["Chalcedony", "opaque white"],
    ["Chrysoprase", "translucent green"],
    ["Citrine", "transparent pale yellow-brown"],
    ["Jasper", "opaque blue, black, or brown"],
    ["Moonstone", "translucent white with pale blue glow"],
    ["Onyx", "opaque bands of black and white, or pure black or white"],
    ["Quartz", "transparent white, smoky gray, or yellow"],
    ["Sardonyx", "opaque bands of red and white"],
    ["Star rose quartz", "translucent rosy stone with white star-shaped center"],
    ["Zircon", "transparent pale blue-green"],
    ["Alexandrite", "transparent dark green"],
    ["Aquamarine", "transparent pale blue-green"],
    ["Black pearl", "opaque pure black"],
    ["Blue spinel", "transparent deep blue"],
    ["Peridot", "transparent rich olive green"],
    ["Topaz", "transparent golden yellow"],
    ["Black opal", "translucent dark green with black mottling and golden flecks"],
    ["Blue sapphire", "transparent blue-white to medium blue"],
    ["Emerald", "transparent deep bright green"],
    ["Fire opal", "translucent fiery red"],
    ["Opal", "translucent pale blue with green and golden mottling"],
    ["Star ruby", "translucent ruby with white star-shaped center"],
    ["Star sapphire", "translucent blue sapphire with white star-shaped center"],
    ["Yellow sapphire", "transparent fiery yellow or yellow green"],
    ["Black sapphire", "translucent lustrous black with glowing highlights"],
    ["Diamond", "transparent blue-white, canary, pink, brown, or blue"],
    ["Jacinth", "transparent fiery orange"],
    ["Ruby", "transparent clear red to deep crimson"],
]

gem_numbers.on("change", _ => {
    const number = parseInt(gem_numbers.val())
    const rolled_gems = {}
    for (let i = 0; i < number; i++) {
        const random_idx = Math.floor(Math.random() * gems.length)
        if (!(random_idx in rolled_gems)) {
            rolled_gems[random_idx] = 0
        }
        rolled_gems[random_idx] += 1
    }

    gem_list.children().remove()
    for (const [key, value] of Object.entries(rolled_gems)) {
        const gem_name = gems[key][0]
        const plural = (value > 1 && !["x", "z", "s"].includes(gem_name[gem_name.length - 1])) ? "s" : ""
        gem_list.append($("<li class=\"list-group-item\">"
            + value + " " + gems[key][0] + plural + ": " + gems[key][1]
            + "</li>"))
    }
})
gem_numbers.trigger("change")
