'use strict';

let socket = io();

const file_mapping = {
    Surprised: "/static/img/surprised.svg",
    "Opportunity attack used": "/static/img/opportunity_attack.svg",
    "Reaction used": "/static/img/reaction.svg",
    "Readied action": "/static/img/readied_action.svg",
    "Concentrating on a spell": "/static/img/concentrating.svg",
    Blinded: "/static/img/blinded.svg",
    Charmed: "/static/img/charmed.svg",
    Deafened: "/static/img/deafened.svg",
    Frightened: "/static/img/frightened.svg",
    Grappled: "/static/img/grappled.png",
    Invisible: "/static/img/invisible.svg",
    "On fire": "/static/img/on_fire.svg",
    Hasted: "/static/img/hasted.svg",
    Paralyzed: "/static/img/paralyzed.svg",
    Petrified: "/static/img/petrified.svg",
    Poisoned: "/static/img/poisoned.svg",
    Prone: "/static/img/prone.svg",
    Restrained: "/static/img/restrained.svg",
    Slowed: "/static/img/slowed.svg",
    Stunned: "/static/img/stunned.svg",
    Unconscious: "/static/img/unconscious.svg",
    Transformed: "/static/img/transformed.svg",
    "Other magical effect": "/static/img/magical_effect.svg",
    "Attacked with advantage": "/static/img/attacked_with_advantage.svg",
    "Attacked with disadvantage": "/static/img/attacked_with_disadvantage.svg",
    "Attacks with advantage": "/static/img/attacks_with_advantage.svg",
    "Attacks with disadvantage": "/static/img/attacks_with_disadvantage.svg"
}

const editable_conditions = ["Opportunity attack used", "Reaction used", "Readied action", "Concentrating on a spell",
    "Hasted", "Invisible", "On fire", "Transformed"]

const implied_advantages = {
    Blinded: ["Attacked with advantage", "Attacks with disadvantage"],
    Frightened: [{name: "Attacks with disadvantage", restriction: ["if the source of fear is in line of sight"]}],
    Invisible: ["Attacks with advantage", "Attacked with disadvantage"],
    Paralyzed: ["Attacked with advantage"],
    Petrified: ["Attacked with advantage"],
    Poisoned: ["Attacks with disadvantage"],
    Prone: ["Attacks with disadvantage",
        {name: "Attacked with disadvantage", restriction: ["if &gt; 1.5 meters"]},
        {name: "Attacked with advantage", restriction: ["if &le; 1.5 meters"]}],
    Restrained: ["Attacked with advantage", "Attacks with disadvantage"],
    Stunned: ["Attacked with advantage"],
    Unconscious: [{name: "Attacked with advantage", restriction: ["if &le; 1.5 meters"]}],
}

socket.on('initiative-updated', data => {
    // Find corresponding row
    const rows = $('tbody.tracker-table-body').find('tr')
    for (let i = 0; i < rows.length; i++) {
        let alias = rows[i].children[0].firstChild.data
        if (alias == null)
            continue
        alias = alias.trim()
        if (data.pc === alias) {
            // Update value of initiative
            rows[i].children[1].children[0].value = data.init
            break
        }
    }
})

function update_initiative(e) {
    const pc = e.target.parentElement.parentElement.firstChild.firstChild.textContent
    const init = e.target.value
    if (!isNaN(init))
        socket.emit("update-initiative", {pc: pc, init: init})
}

function roll_insight(e) {
    const pc = e.target.parentElement.parentElement.firstChild.firstChild.textContent
    socket.emit("roll-insight", {pc: pc})
    play_audio_sound(1)
}

function condition_toggle(e) {
    const pc = e.target.parentElement.parentElement.firstChild.firstChild.textContent
    const condition = e.target.getAttribute("data-condition")

    if (is_toggleable_active(e.target)) {
        disable_toggleable(e.target);
    } else {
        enable_toggleable(e.target);
    }

    socket.emit("condition-toggle", {pc: pc, condition: condition, up: is_toggleable_active(e.target)})
}

class PlayerSideTracker extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            creatures: this.props.creatures,
            round: this.props.round
        }
        this.intervalID = 5000;
    }

    componentDidMount() {
        socket.on('update-fight', data => {
            creatures = data.creatures
            round = data.round
            this.setState({creatures: creatures, round: round});
        });

        socket.on('condition-toggled', data => {
            /* Remove or add condition to creature data and rebuild UI */
            for (let i = 0; i < creatures.length; i++) {
                if (creatures[i].alias === data.pc) {
                    if (data.up && !creatures[i].conditions.includes(data.condition)) {
                        creatures[i].conditions.unshift(data.condition)
                        break
                    } else if (!data.up && creatures[i].conditions.includes(data.condition)) {
                        creatures[i].conditions = creatures[i].conditions.filter(cond => cond !== data.condition)
                        break
                    }
                }
            }
            this.setState({creatures: creatures, round: round})
        })
        $(".condition").tooltip()
        $(".player-view-condition").tooltip()
        $(".fa-dice-d20.clickable").tooltip()
    }

    componentDidUpdate(prevProps: Readonly<P>, prevState: Readonly<S>, snapshot: SS) {
        $(".condition").tooltip()
        $(".player-view-condition").tooltip()
        $(".fa-dice-d20.clickable").tooltip()
    }

    componentWillUnmount() {
        clearTimeout(this.intervalID);
    }

    renderTableHeader() {
        return (
            <tr>
                <th scope="col" className="align-middle" style={{width: "10%"}}>
                    Name
                </th>
                <th scope="col" className="align-middle" style={{width: "5%"}}>
                    Initiative
                </th>
                <th scope="col" className="align-middle" style={{width: "40%"}}>
                    Conditions
                </th>
                <th scope="col" className="align-middle" style={{width: "5%"}}>
                    Insight
                </th>
            </tr>
        )
    }

    renderCondition(creature, condition, active, force_tooltip = null) {
        const key = creature.key + "-" + condition
        const tooltip = force_tooltip == null ? conditions_tooltips[condition] : force_tooltip
        if (editable_conditions.includes(condition)) {
            // Toggleable conditions
            const style = active ? {opacity: 1} : {opacity: 0.2}
            return (<img src={file_mapping[condition]} key={key}
                         alt="" className="condition toggleable" onClick={condition_toggle}
                         title={condition + tooltip} style={style} data-toggle="tooltip"
                         data-condition={condition} data-placement="bottom" data-html="true"/>)
        }
        return (<img src={file_mapping[condition]} key={key}
                     alt="" className="player-view-condition"
                     title={condition + tooltip} data-toggle="tooltip"
                     data-condition={condition} data-placement="bottom" data-html="true" data-container="body"/>)
    }

    add_advantages(current_advantages, condition) {
        if (condition in implied_advantages) {
            const condition_advantages = implied_advantages[condition]
            for (let i = 0; i < condition_advantages.length; i++) {
                let advantage = condition_advantages[i]
                // with or without restriction
                if (typeof advantage == "string" || advantage instanceof String) {
                    advantage = {name: advantage, restriction: []}
                }

                if (current_advantages[advantage.name] === null) {
                    current_advantages[advantage.name] = advantage.restriction
                } else if (current_advantages[advantage.name].length !== 0) {
                    if (advantage.restriction.length === 0)
                        current_advantages[advantage.name] = advantage.restriction
                    else {
                        current_advantages[advantage.name].push(advantage.restriction)
                    }
                }
            }
        }
    }

    renderTableData() {
        return this.state.creatures.map(creature => {
            const row_class = creature.active ? playing_creature_class : active_creature_class;
            const elem_class = creature.image ? "align-middle npc" : "align-middle";
            const image_target = "#image" + creature.image_hash.toString();
            const first_elem = creature.alias ? creature.alias :
                (<img src={creature.image} width="15%"
                      className="rounded mx-auto" alt=""
                      title={creature.alias}/>)

            // Potential advantages
            // null means no advantage, [] means advantage without restriction,
            // '["restriction 1",...]' lists potential restrictions
            const current_advantages = {
                "Attacked with advantage": null,
                "Attacked with disadvantage": null,
                "Attacks with advantage": null,
                "Attacks with disadvantage": null
            }

            // Conditions
            const conditions = Object.keys(file_mapping).map(condition => {
                const active = creature.conditions.includes(condition)
                if (active || (editable_conditions.includes(condition) && creature.alias)) {
                    if (active) {
                        this.add_advantages(current_advantages, condition)
                    }
                    return this.renderCondition(creature, condition, active)
                }
            })

            // Implied advantages
            for (let [key, value] of Object.entries(current_advantages)) {
                if (value != null) {
                    value = value.length > 0 ? ("<br/>" + value.join("<br/>")) : value.join("<br/>")
                    conditions.push(this.renderCondition(creature, key, true, value))
                }
            }

            const init = creature.alias ? <input className="form-control form-control-sm initiative" min="0" step="1"
                                                 defaultValue={creature.initiative} name="init" aria-label=""
                                                 onChange={update_initiative}/>
                : creature.initiative

            const roll = creature.alias ? <i className="fas fa-dice-d20 fa-2x clickable" title="Roll insight"
                                             data-placement="bottom" onClick={roll_insight}></i> : ""

            return (<tr className={row_class} key={creature.key}>
                <th scope="row" className={elem_class}
                    data-toggle="modal"
                    data-target={image_target}>
                    {first_elem}
                </th>
                <td className="align-middle">
                    {init}
                </td>
                <td className="align-middle">
                    {conditions}
                </td>
                <td className="align-middle">
                    {roll}
                </td>
            </tr>)
        })
    }

    render() {
        $("#current-round")[0].firstChild.data = "Round " + this.state.round
        return (<table className="table table-hover">
            <thead>
            {this.renderTableHeader()}
            </thead>
            <tbody className="tracker-table-body">
            {this.renderTableData()}
            </tbody>
        </table>)
    }
}

class CreatureImageModals extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            creatures: this.props.creatures
        }
    }

    componentDidMount() {
        socket.on('update-fight', data => {
            this.setState({creatures: data.creatures});
        });
    }

    render() {
        return this.state.creatures.map(creature => {
            if (creature.image) {
                const image_target = "image" + creature.image_hash.toString()
                const modal_key = "image" + creature.key.toString()
                return (
                    <div className="modal" tabIndex="-1" role="dialog"
                         id={image_target} key={modal_key}>
                        <div className="modal-dialog" role="document">
                            <div className="modal-content">
                                <div className="modal-body container">
                                    <h2 className="text-center">
                                        {creature.alias}
                                    </h2>
                                    <br/>
                                    <img src={creature.image}
                                         width="50%"
                                         className="rounded mx-auto d-block"
                                         alt=""
                                         title={creature.alias}/>
                                    <br/>
                                    <div className="col-md-12 text-center">
                                        <button className="btn btn-info"
                                                data-dismiss="modal"
                                                aria-label="Close">
                                            Close
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>)
            }
            return ""
        })
    }

}

ReactDOM.render(<PlayerSideTracker creatures={creatures} round={round}/>,
    document.getElementById("tracker-root"));
ReactDOM.render(<CreatureImageModals creatures={creatures}/>,
    document.getElementById("tracker-modals"));
