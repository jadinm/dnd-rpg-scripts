import copy
import os
import re
from dataclasses import dataclass, field
from typing import List, Dict, Any, Union, ClassVar, Optional, Tuple

import yaml
from flask import request

from db.query import DB


@dataclass
class SpellLevel:
    name: str
    spells: List[Union['Spell', str]] = field(default_factory=list)
    limit: int = 0
    expanded: int = 0

    def __post_init__(self):
        self.expanded = min(self.expanded, self.limit)
        self.spells = [Spell(s) for s in self.spells if len(s) > 0]

        # Replace banned spells
        for i, spell in enumerate(self.spells):
            if spell.banned:
                for alternative in spell.alternatives:
                    if alternative not in self.spells:
                        self.spells[i] = alternative
                        break
                assert not self.spells[i].banned, f"Cannot replace {spell} by any other spell alternative"

    def serialize(self) -> Dict[str, Any]:
        return {
            "name": self.name,
            "spells": [s.serialize() for s in self.spells],
            "limit": self.limit,
            "expanded": self.expanded
        }

    @property
    def common_classes(self) -> List[str]:
        """
        :return: The list of common classes between spells
        """
        classes = set()
        for spell in self.spells:
            classes = classes.union(spell.accessible_to_classes)
        return list(classes)


@dataclass(eq=False)
class Spell:
    name: str
    link: Optional[str] = field(repr=False, default=None)
    original_name: Optional[str] = field(repr=False, default=None)
    accessible_to_classes: List[str] = field(repr=False, default_factory=list)
    banned: bool = field(repr=False, default=False)
    alternatives: List['Spell'] = field(repr=False, default_factory=list)

    SPELL_DB: ClassVar[str] = os.path.join(DB, "spells.yaml")
    BANNED_SPELL_DB: ClassVar[str] = os.path.join(DB, "banned_spells.yaml")
    SPELL_LINK_REGEX: ClassVar[re.Pattern] = re.compile(r"({@spell ([\w ()'/-]+)(?:\|([\w-]+))?})( \([ \w]+\)|[*+-]?)")
    FILTER_REGEX: ClassVar[re.Pattern] = re.compile(r"Any {@filter ([\w ()'/-]+)\|spells\|level=([0-9;]+)\|class=(\w+)}")

    with open(SPELL_DB) as file_obj:
        data: ClassVar = yaml.load(file_obj, Loader=yaml.CSafeLoader)

    with open(BANNED_SPELL_DB) as file_obj:
        banned_spells: ClassVar[Dict[str, str]] = yaml.load(file_obj, Loader=yaml.CSafeLoader)

    def __post_init__(self):
        self.original_name = self.name

        match = self.SPELL_LINK_REGEX.search(self.name)
        if match:
            self.name, self.link = self.to_spell_link(match)
            data_key = self.expanded_tag(match.group(1))
            assert data_key in Spell.data, f"Cannot find spell {self.original_name} in DB"

            self.accessible_to_classes = Spell.data[data_key]["classes"]
            self.level = Spell.data[data_key]["level"]
            self.banned = data_key in Spell.banned_spells
            if self.banned:
                self.alternatives = [Spell(x) for x in Spell.banned_spells[data_key]]
        else:
            match = self.FILTER_REGEX.search(self.name)
            assert match, f"Cannot parse spell {self.original_name}"

            self.name = match.group(1)
            levels = "~".join(map(lambda x: x + "=1", match.group(2).split(';')))
            self.link = f"https://{request.host + '/5e' if request else '5e.tools'}/spells.html#blankhash,flstlevel:{levels},flstclass:{match.group(3)}=1"

    @staticmethod
    def expanded_tag(spell_tag):
        if "|" in spell_tag:
            return spell_tag.lower()
        else:
            return spell_tag[:-1].lower() + "|phb}"

    @classmethod
    def to_spell_link(cls, match: re.Match) -> Tuple[str, str]:
        source = match.group(3)
        if not source:
            source = "phb"
        return match.group(2) + match.group(4), f"https://{request.host + '/5e' if request else '5e.tools'}" \
                                                f"/spells.html#{match.group(2)}_{source.lower()}"

    @classmethod
    def filter(cls, min_level=1, max_level=9, filter_out_ua: bool = True, dnd_class: Optional[str] = "Wizard") \
            -> List['Spell']:
        return [cls(key) for key, value in copy.deepcopy(cls.data).items()
                if min_level <= value["level"] <= max_level and
                ("(ua)" not in key and "UA" != value["source"][:2] or not filter_out_ua) and
                (dnd_class is None or dnd_class in value["classes"])]

    def serialize(self) -> str:
        return self.original_name

    def __str__(self):
        return self.name

    def __hash__(self):
        return hash(self.name)

    def __eq__(self, other):
        return self.link == other.link if self.link is not None else self.name

    def __lt__(self, other):
        return self.name < other.name
