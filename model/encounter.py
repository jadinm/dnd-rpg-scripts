import copy
import os
from typing import List

import yaml

from db import get_db_data
from model.creature import Creature
from utils import YAML_ARGS


class Encounter:
    def __init__(self, encounter_file: str):
        self.file = os.path.abspath(encounter_file)
        self._data = None
        self._creatures = None

    @staticmethod
    def is_encounter_file(file_name) -> bool:
        return ".yaml" == file_name[-5:] and ".xp.yaml" not in file_name

    @property
    def name(self) -> str:
        return ".".join(os.path.basename(self.file).split(".")[:-1])

    @property
    def data(self):
        if self._data is None:
            with open(self.file) as file_obj:
                self._data = yaml.load(file_obj, Loader=yaml.CSafeLoader)
        return copy.deepcopy(self._data)

    @property
    def creatures(self) -> List[Creature]:
        if self._creatures is None:
            self._creatures = []
            for d in self.data:
                infos = {}
                if type(d) == str:  # "Creature name"
                    number = 1
                    name = d
                elif type(d) == list and type(d[0]) == int:
                    # [num, "Creature name"]
                    number = d[0]
                    name = d[1]
                elif type(d) == list:
                    # ["Creature name", "Creature alias"]
                    number = 1
                    name = d[0]
                    infos["alias"] = d[1]
                else:  # Dictionary with some modified properties
                    if d.pop("inactive", False):
                        continue  # Inactive creature (for instance an invited player)
                    number = d.pop("number", 1)
                    name = d.pop("name")
                    infos = d

                # Search database
                db_data = get_db_data(name)
                if db_data is not None:
                    del db_data["name"]
                    db_data.update(infos)
                    infos = db_data

                for _ in range(number):
                    self._creatures.append(Creature(name, **infos))
        return self._creatures

    def swap_extension(self, new_extension) -> str:
        """Return the same file path with a new file extension"""
        return ".".join(self.file.split(".")[:-1]) + "." + new_extension

    def reformat(self):
        """Reformat correctly the file if needed"""
        with open(self.file) as file_obj:
            origin_yaml = file_obj.read()

        new_yaml = yaml.dump(self.data, **YAML_ARGS)
        if origin_yaml != new_yaml:
            with open(self.file, "w") as file_obj:
                file_obj.write(new_yaml)

    def __hash__(self):
        return self.file

    def __eq__(self, other):
        return isinstance(other, Encounter) \
               and self.file == other.file

    def __lt__(self, other):
        return isinstance(other, Encounter) \
               and self.file < other.file

    def __str__(self):
        return self.file
