conditions_tooltips = {
    "Opportunity attack used": "",
    "Reaction used": "<br/>Can possibly attack a marked creature",
    "Readied action": "",
    "Concentrating on a spell": "<br/>Broken by:"
                                "<li class='text-left ml-2'>attacks if missed DD max(damage/2, 10)</li>"
                                "<li class='text-left ml-2'>concentrating on another concentration spell</li>"
                                "<li class='text-left ml-2'>concentrating by holding any spell</li>"
                                "<li class='text-left ml-2'>concentrating by casting any spell for longer"
                                " than 1 action</li>"
                                "<li class='text-left ml-2'>death</li>"
                                "<li class='text-left ml-2'>incapacitated state</li>"
                                "<li class='text-left ml-2'>"
                                "<img src='/static/img/petrified.svg' height='20em'/>"
                                "<img src='/static/img/paralyzed.svg' height='20em'/>"
                                "<img src='/static/img/stunned.svg' height='20em'/>"
                                "<img src='/static/img/unconscious.svg' height='20em'/>"
                                "</li>",
    "Surprised": "<br/>Cannot move, take actions or reaction during the first round",
    "Blinded": "<li class='text-left ml-2'>Advantage against it</li>"
               "<li class='text-left ml-2'>It has disadvantage</li>",
    "Charmed": "<li class='text-left ml-2'>Cannot harm the charmer in any way</li>"
               "<li class='text-left ml-2'>The charmer has advantage on any ability check"
               " to interact socially with the creature</li>",
    "Deafened": "",
    "Frightened": "<li class='text-left ml-2'>Disadvantage on ability checks and attack rolls"
                  "while the source of its fear is within line of sight.</li>"
                  "<li class='text-left ml-2'>The creature can't willingly move closer to the source</li>",
    "Grappled": "<li class='text-left ml-2'>Speed is 0</li>"
                "<li class='text-left ml-2'>Ends if grappler is incapacitated, petrified, paralyzed, stunned"
                " or unconscious.</li>"
                "<li class='text-left ml-2'>Ends if an effect removes the grappled from the reach of the grappler</li>",
    "Hasted": "<li class='text-left ml-2'>Speed &times; 2</li>"
              "<li class='text-left ml-2'>AC + 2</li>"
              "<li class='text-left ml-2'>Advantage Dexterity saving throws</li>"
              "<li class='text-left ml-2'>Get one action for:"
              "<ul>"
              "<li>Attack (one weapon attack only)</li>"
              "<li>Dash</li>"
              "<li>Disengage</li>"
              "<li>Hide</li>"
              "<li>Use an Object</li>"
              "</ul>"
              "</li>",
    "Invisible": "<li class='text-left ml-2'>Disadvantage against it</li>"
                 "<li class='text-left ml-2'>Advantage on its attacks</li>"
                 "<li class='text-left ml-2'>Can be detected by any noise it makes or any tracks it leaves.</li>",
    "On fire": "",
    "Paralyzed": "<li class='text-left ml-2'>Cannot move, talk, take actions or reactions</li>"
                 "<li class='text-left ml-2'>Cannot concentrate on a spell</li>"
                 "<li class='text-left ml-2'>Fails Strength and Dexterity saving throws.</li>"
                 "<li class='text-left ml-2'>Attack rolls against the creature have advantage</li>"
                 "<li class='text-left ml-2'>Any attack that hits is a critical hit if within 1.5 meters</li>",
    "Petrified": "<li class='text-left ml-2'>Transformed with its non-magical equipment into stone</li>"
                 "<li class='text-left ml-2'>It is unaware of its surroundings</li>"
                 "<li class='text-left ml-2'>Cannot move, talk, take actions or reactions</li>"
                 "<li class='text-left ml-2'>Cannot concentrate on a spell</li>"
                 "<li class='text-left ml-2'>Fails Strength and Dexterity saving throws.</li>"
                 "<li class='text-left ml-2'>Attack rolls against the creature have advantage</li>"
                 "<li class='text-left ml-2'>It has resistance to all damage</li>",
    "Poisoned": "<br/>Disadvantage on attack rolls and ability checks",
    "Prone": "<li class='text-left ml-2'>Crawl (half speed) or stands up (half of movement)</li>"
             "<li class='text-left ml-2'>It has disadvantage on attack rolls.</li>"
             "<li class='text-left ml-2'>Advantage against it if within 1.5 m</li>"
             "<li class='text-left ml-2'>Disadvantage against it otherwise</li>",
    "Restrained": "<li class='text-left ml-2'>Speed is 0</li>"
                  "<li class='text-left ml-2'>It has disadvantage on attack rolls.</li>"
                  "<li class='text-left ml-2'>Advantage against it</li>"
                  "<li class='text-left ml-2'>Disadvantage on Dexterity saving throws</li>",
    "Slowed": "<li class='text-left ml-2'>Speed &divide; 2</li>"
              "<li class='text-left ml-2'>AC - 2</li>"
              "<li class='text-left ml-2'>Malus on dexterity saving throws of -2</li>"
              "<li class='text-left ml-2'>Cannot use reactions</li>"
              "<li class='text-left ml-2'>Can use either action or bonus action, not both</li>"
              "<li class='text-left ml-2'>Only one melee or range attacks max</li>",
    "Stunned": "<li class='text-left ml-2'>Cannot move, take actions or reactions</li>"
               "<li class='text-left ml-2'>Cannot concentrate on a spell</li>"
               "<li class='text-left ml-2'>Can only speak falteringly</li>"
               "<li class='text-left ml-2'>Fails Strength and Dexterity saving throws.</li>"
               "<li class='text-left ml-2'>Attack rolls against the creature have advantage</li>",
    "Unconscious": "<li class='text-left ml-2'>It is unaware of its surroundings and is prone (already included)</li>"
                   "<li class='text-left ml-2'>Cannot move, talk, take actions or reactions</li>"
                   "<li class='text-left ml-2'>Cannot concentrate on a spell</li>"
                   "<li class='text-left ml-2'>Fails Strength and Dexterity saving throws.</li>"
                   "<li class='text-left ml-2'>Attack rolls against the creature have advantage if within 1.5m</li>"
                   "<li class='text-left ml-2'>Straight roll otherwise</li>"
                   "<li class='text-left ml-2'>Any attack that hits is a critical hit if within 1.5 meters</li>",
    "Transformed": "",
    "Other magical effect": ""
}

for key in list(conditions_tooltips):  # Allow lower case condition names
    conditions_tooltips[key.lower()] = conditions_tooltips[key]
