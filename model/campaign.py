import imghdr
import os
from typing import List, Dict, Tuple, Hashable, Optional

from natsort import natsorted

from utils import is_inside_dir
from .creature import Creature
from .encounter import Encounter


class Campaign:

    def __init__(self, campaign_dir: str, reference_dir: Optional[str]):
        self.dir = os.path.abspath(campaign_dir)
        self.reference_dir = reference_dir

        self.chapters = []
        for root, dir_names, file_names in os.walk(self.dir):
            for name in dir_names:
                chapter = Chapter(os.path.join(root, name), self)
                if len(chapter.encounters) > 0:
                    self.chapters.append(chapter)
        self.chapters = natsorted(self.chapters, lambda x: x.dir)

        self.characters = Encounter(os.path.join(self.dir, "characters.yaml"))

        self.image_hashes = {}  # image -> hash
        self.reverse_image_hash = {}  # hash -> image
        self._prepare_image_hashes()

    @property
    def encounters(self) -> List[Encounter]:
        """Returns every encounter"""
        for chapter in self.chapters:
            for encounter in chapter.encounters:
                yield encounter

    def max_appearance_frequencies(self) \
            -> Dict[Hashable, Dict[str, Tuple[Creature, int]]]:
        """
        :return: A dictionary mapping a hash of the real and fake appearances of
        a creature to its maximum frequency in any chapter grouped by its
        different sizes. Indeed a creature with the same name and images can
        be customized to have a different size.
        """
        max_freq = {}
        Chapter.add_encounter_frequencies(self.characters, max_freq)
        for chapter in self.chapters:
            for h_value, freq in chapter.appearance_frequencies().items():
                if h_value not in max_freq:
                    max_freq[h_value] = freq
                else:
                    for size, size_freq in freq.items():
                        if size not in max_freq[h_value] \
                                or max_freq[h_value][size][1] < size_freq[1]:
                            max_freq[h_value][size] = size_freq
        return max_freq

    @property
    def image_dir(self) -> str:
        return os.path.join(self.dir, "images")

    @property
    def figurine_dir(self) -> str:
        return os.path.join(self.dir, "figurines")

    def _prepare_image_hashes(self):
        images = set(self.retrieve_existing_images(include_creatures=True,
                                                   include_npc=False,
                                                   include_pc=False,
                                                   use_duplicates=True).values())
        npc = set(self.retrieve_existing_images(include_creatures=False,
                                                include_npc=True,
                                                include_pc=False,
                                                use_duplicates=True).values())
        pc = set(self.retrieve_existing_images(include_creatures=False,
                                               include_npc=False,
                                               include_pc=True,
                                               use_duplicates=True).values())
        references = set()
        if self.reference_dir:
            references = set(self.retrieve_existing_images(include_creatures=True, include_npc=True, include_pc=True,
                                                           dir_path=self.reference_dir, use_duplicates=True)
                             .values())
        images = images.union(npc, pc, references)
        for img in images:
            hash_img = str(hash(img))
            self.reverse_image_hash[hash_img] = img
            self.image_hashes[img] = hash_img

    def retrieve_existing_images(self, include_creatures=True,
                                 include_npc=True, include_pc=False,
                                 dir_path: Optional[str] = None,
                                 use_duplicates: bool = False) \
            -> Dict[str, str]:
        paths = {}
        dir_path = dir_path if dir_path is not None else self.image_dir
        for root, folders, files in os.walk(dir_path):
            if not include_pc and is_inside_dir(root, "pc"):
                continue
            if not include_npc and is_inside_dir(root, "npc"):
                continue
            if not include_creatures \
                    and not is_inside_dir(root, "pc") \
                    and not is_inside_dir(root, "npc"):
                continue
            for file in files:
                file_path = os.path.join(root, file)
                image_type = imghdr.what(file_path)
                if "grayscale" in file or image_type is None:
                    continue
                paths[str(file.split(".")[0]) if not use_duplicates else file_path] = file_path
        return paths

    def __hash__(self):
        return self.dir

    def __eq__(self, other):
        return isinstance(other, Campaign) \
               and self.dir == other.dir

    def __lt__(self, other):
        return isinstance(other, Chapter) \
               and self.dir < other.dir

    def __str__(self):
        return self.dir


class Chapter:

    def __init__(self, chapter_dir: str, campaign: Campaign):
        self.campaign = campaign
        self.dir = os.path.abspath(chapter_dir)
        self.encounters = []
        for root, dir_names, file_names in os.walk(self.dir):
            for file_name in file_names:
                file_name = os.path.join(root, file_name)
                if Encounter.is_encounter_file(file_name):
                    self.encounters.append(Encounter(file_name))
            break
        self.encounters = natsorted(self.encounters, lambda x: x.file)

    @property
    def name(self):
        return os.path.relpath(self.dir, self.campaign.dir)

    def appearance_frequencies(self) \
            -> Dict[Hashable, Dict[str, Tuple[Creature, int]]]:
        freq = {}
        for encounter in self.encounters:
            self.add_encounter_frequencies(encounter, freq)
        return freq

    @staticmethod
    def add_encounter_frequencies(encounter, freq):
        for creature in encounter.creatures:
            images = list({creature.image_path, creature.player_image_path})
            for image in images:
                if image not in freq:
                    freq.setdefault(image, {})[creature.size] = \
                        (creature, 0)
                if creature.size not in freq[image]:
                    freq[image][creature.size] = (creature, 0)
                freq[image][creature.size] = \
                    (creature, freq[image][creature.size][1] + 1)

    def __hash__(self):
        return self.dir

    def __eq__(self, other):
        return isinstance(other, Chapter) \
               and self.dir == other.dir

    def __lt__(self, other):
        return isinstance(other, Chapter) \
               and self.dir < other.dir

    def __str__(self):
        return self.dir
