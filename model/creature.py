import builtins
import copy
import os
import random
import re
from dataclasses import dataclass, field
from typing import Dict, List, Any, Optional, ClassVar, Union

from flask import request

from utils import aidedd_text, roll
from .spell import SpellLevel, Spell
from .trait import Trait

hpmax_max_roll_regex = re.compile(r"^(\d+)d(\d+) ?([+-] ?\d+)?$")


@dataclass
class Creature:
    """Class representing any NPC or PC"""

    # Generic variables

    name: str
    alias: Optional[str] = None

    image_key: Optional[str] = None
    image_path: Optional[str] = None
    appearance: Optional[str] = None
    player_image_path: Optional[str] = None
    reference_image_path: Optional[str] = None  # Given by the database, not the adventure

    source: List[str] = field(default_factory=list)  # [5e.tools creature name, 5e.tools source book]

    note: str = ""

    # DnD5 specific variables

    ac: int = 0
    armor_desc: str = ""
    hpmax: int = 1
    hpmax_roll: Optional[str] = None
    hp: Optional[int] = None
    xp: int = 0
    other_xp: Dict[str, int] = field(default_factory=dict)

    size: str = "M"
    type: str = "Humanoid (all races)"
    alignment: str = "any alignment"
    speed: str = "9 m."

    strength: int = 0
    dexterity: int = 0
    constitution: int = 0
    intelligence: int = 0
    wisdom: int = 0
    charisma: int = 0

    saving_throws: List[str] = field(default_factory=list)
    abilities: List[str] = field(default_factory=list)
    damage_weaknesses: List[str] = field(default_factory=list)
    damage_resistances: List[str] = field(default_factory=list)
    damage_immunities: List[str] = field(default_factory=list)
    condition_immunities: List[str] = field(default_factory=list)
    senses: List[str] = field(default_factory=list)
    languages: List[str] = field(default_factory=list)

    traits: List[Union[Trait, Dict]] = field(default_factory=list)
    spells: List[Union[SpellLevel, Dict]] = field(default_factory=list)
    actions: List[Union[Trait, Dict]] = field(default_factory=list)
    reactions: List[Union[Trait, Dict]] = field(default_factory=list)
    variant: List[Union[Trait, Dict]] = field(default_factory=list)
    legendary_actions_desc: Optional[str] = None
    legendary_actions_limit: int = 0
    legendary_actions_expanded: int = 0
    legendary_actions: List[Union[Trait, Dict]] = field(default_factory=list)
    mythic_actions_desc: Optional[str] = None
    mythic_actions: List[Union[Trait, Dict]] = field(default_factory=list)
    lair_actions_desc: Optional[str] = None
    lair_actions: List[Union[Trait, Dict]] = field(default_factory=list)
    regional_effects: List[Union[Trait, Dict]] = field(default_factory=list)
    has_spellbook: bool = False
    known_spells: List[Union[Spell, str]] = field(default_factory=list)

    # Useful when saving a fight

    initiative: int = 0
    conditions: List[str] = field(default_factory=list)
    visible: bool = False
    active: bool = False

    # Class variables

    challenge_rating_xp: ClassVar[Dict[int, str]] = {
        25: "1/8", 50: "1/4", 100: "1/2", 200: "1", 450: "2",
        700: "3", 1100: "4", 1800: "5", 2300: "6", 2900: "7",
        3900: "8", 5000: "9", 5900: "10", 7200: "11", 8400: "12",
        10000: "13", 11500: "14", 13000: "15", 15000: "16", 18000: "17",
        20000: "18", 22000: "19", 25000: "20", 33000: "21", 41000: "22",
        50000: "23", 62000: "24", 75000: "25", 90000: "26", 105000: "27",
        120000: "28", 135000: "29", 155000: "30"
    }
    xp_challenge_rating: ClassVar[Dict[str, int]] = {
        y: x for x, y in challenge_rating_xp.items()
    }

    ability_regex: ClassVar[re.Pattern] = re.compile(r"(.+) ([+-]\d+)")
    abilities_mapping: ClassVar[Dict[str, str]] = {
        "acrobatics": "dex",
        "animal handling": "wis",
        "arcana": "int",
        "athletics": "str",
        "deception": "dex",
        "history": "int",
        "insight": "wis",
        "intimidation": "cha",
        "investigation": "int",
        "medecine": "wis",
        "nature": "int",
        "perception": "wis",
        "performance": "cha",
        "persuasion": "cha",
        "religion": "int",
        "sleight of hand": "dex",
        "stealth": "dex",
        "survival": "wis",
    }
    proficiency_bonus: ClassVar[Dict[str, int]] = {
        "0": +2, "1/8": +2, "1/4": +2, "1/2": +2, "1": +2, "2": +2, "3": +2, "4": +2,
        "5": +3, "6": +3, "7": +3,
        "8": +4, "9": +4, "10": +4, "11": +4, "12": +4,
        "13": +5, "14": +5, "15": +5, "16": +5,
        "17": +6, "18": +6, "19": +6, "20": +6,
        "21": +7, "22": +7, "23": +7, "24": +7,
        "25": +8, "26": +8, "27": +8, "28": +8,
        "29": +9, "30": +9
    }

    width_by_size: ClassVar[Dict[str, int]] = {
        "T": 20,
        "S": 20,
        "M": 20,
        "L": 20 * 2,
        "H": 20 * 3,
        "G": 20 * 4,
        "TP": 20,
        "P": 20,
        "TG": 20 * 3,
        "Gig": 20 * 4
    }
    height_by_size: ClassVar[Dict[str, int]] = {
        "T": 20,
        "S": 27,
        "M": 35,
        "L": 35,
        "H": 35,
        "G": 35,
        "TP": 20,
        "P": 27,
        "TG": 35,
        "Gig": 35
    }
    sizes_str: ClassVar[Dict[str, str]] = {
        "T": "Tiny",
        "S": "Small",
        "M": "Medium",
        "L": "Large",
        "H": "Huge",
        "G": "Gargantuesque",
        "TP": "Tiny",
        "P": "Small",
        "TG": "Huge",
        "Gig": "Gargantuesque"
    }

    def __post_init__(self):
        if self.alias is None:
            self.alias = self.name

        old_hpmax = self.hpmax
        if self.hpmax_roll is not None:
            # By default, we give the maximum rolled hp
            self.hpmax = self.hpmax_max_roll

        if self.hp is None or self.hp == old_hpmax and self.hp != self.hpmax:
            self.hp = self.hpmax

        # We give two saving throw proficiency if it has fewer than two
        # We add the best best statistics first
        if len(self.saving_throws) < 2:
            modifiers = []
            for characteristic in ["str", "dex", "con", "int", "wis", "cha"]:
                modifiers.append((characteristic.capitalize(), getattr(self, characteristic)))
            modifiers = sorted(modifiers, key=lambda x: x[1], reverse=True)
            for characteristic, modifier in modifiers:
                if len(self.saving_throws) == 2:
                    break
                modifier = modifier + self.proficiency_bonus_from_challenge(self.challenge)
                saving_throw = f"{characteristic} {modifier:+g}"
                if saving_throw not in self.saving_throws:
                    self.saving_throws.append(saving_throw)

        self.spells = [SpellLevel(**s) for s in self.spells]
        self.known_spells = [Spell(s) for s in self.known_spells]

        self.traits = [Trait(**t) for t in self.traits]
        self.actions = [Trait(**a) for a in self.actions]
        self.reactions = [Trait(**a) for a in self.reactions]
        self.variant = [Trait(**a) for a in self.variant]
        self.legendary_actions = [Trait(**a) for a in self.legendary_actions]
        self.legendary_actions_expanded = min(self.legendary_actions_expanded, self.legendary_actions_limit)
        self.mythic_actions = [Trait(**a) for a in self.mythic_actions]
        self.lair_actions = [Trait(**a) for a in self.lair_actions]
        self.regional_effects = [Trait(**a) for a in self.regional_effects]

    @property
    def has_simple_ac(self):
        return not self.armor_desc or not re.search(r"\d+", self.armor_desc)

    @property
    def has_regular_hpmax_max(self):
        return self.hpmax_roll is None or hpmax_max_roll_regex.match(self.hpmax_roll or "") is not None \
               or re.match(r"^\d+$", self.hpmax_roll or "") is not None

    @property
    def hpmax_max_roll(self) -> int:
        match = hpmax_max_roll_regex.match(self.hpmax_roll or "")
        if match is not None:
            modifier = int(match.group(3).replace(" ", "") if match.group(3) is not None else 0)
            return int(match.group(1)) * int(match.group(2)) + modifier
        return self.hpmax

    @property
    def link(self) -> Optional[builtins.str]:
        """Link to 5e.tools page for the current creature"""
        if len(self.source) < 2:
            return None
        return f"https://{request.host + '/5e' if request else '5e.tools'}/bestiary.html" \
               f"#{self.source[0].lower()}_{self.source[1].lower()}"

    @property
    def width(self) -> builtins.int:
        return self.width_by_size[self.size if self.size else "M"]

    @property
    def length(self) -> builtins.int:
        return self.width

    @property
    def height(self) -> builtins.int:
        return self.height_by_size[self.size if self.size else "M"]

    @property
    def size_full(self) -> builtins.str:
        return self.sizes_str[self.size]

    @property
    def str(self) -> builtins.int:
        return (self.strength - 10) // 2 if self.strength is not None else 0

    @property
    def dex(self) -> builtins.int:
        return (self.dexterity - 10) // 2 if self.dexterity is not None else 0

    @property
    def con(self) -> builtins.int:
        return (self.constitution - 10) // 2 \
            if self.constitution is not None else 0

    @property
    def int(self) -> builtins.int:
        return (self.intelligence - 10) // 2 \
            if self.intelligence is not None else 0

    @property
    def wis(self) -> builtins.int:
        return (self.wisdom - 10) // 2 if self.wisdom is not None else 0

    @property
    def cha(self) -> builtins.int:
        return (self.charisma - 10) // 2 if self.charisma is not None else 0

    @property
    def str_save(self) -> builtins.int:
        return self.saving_throw_modifier("Str")

    @property
    def dex_save(self) -> builtins.int:
        return self.saving_throw_modifier("Dex")

    @property
    def con_save(self) -> builtins.int:
        return self.saving_throw_modifier("Con")

    @property
    def int_save(self) -> builtins.int:
        return self.saving_throw_modifier("Int")

    @property
    def wis_save(self) -> builtins.int:
        return self.saving_throw_modifier("Wis")

    @property
    def cha_save(self) -> builtins.int:
        return self.saving_throw_modifier("Cha")

    @property
    def passive_insight(self) -> builtins.int:
        return 10 + self.ability_modifier("Insight")

    @property
    def spellbook(self) -> List[Spell]:
        return [spell for spell_level in self.spells
                for spell in spell_level.spells] + self.known_spells \
            if self.has_spellbook else []

    def generate_random_spellbook(self):
        """Generate random spells in the spellbook: max 20% of the total number of spells
         and max one level below the maximum"""
        max_level = max([int(spell_level.name.split(" ")[-1])
                         for spell_level in self.spells if "Level" in spell_level.name] or [0])
        nbr_spells = int(len([spell for spell_level in self.spells for spell in spell_level.spells]) * 0.2)
        if max_level < 1:
            return

        current_spellbook = self.spellbook
        choices = [spell for spell in Spell.filter(max_level=max_level - 1)
                   if spell not in current_spellbook]  # Do not duplicate spells
        self.known_spells = random.sample(choices, nbr_spells)

    def compute_challenge(self, xp_value):
        if int(xp_value) < min(self.challenge_rating_xp.keys()):
            return "0"
        if int(xp_value) > max(self.challenge_rating_xp.keys()):
            return "31"
        return self.challenge_rating_xp[int(xp_value)]

    @property
    def challenge(self) -> builtins.str:
        return self.compute_challenge(self.xp)

    @property
    def other_xp_desc(self) -> builtins.str:
        if len(self.other_xp) == 0:
            return ""
        return "or " + " or ".join([f"{key}: {self.compute_challenge(value)} ({value} XP)"
                                    for key, value in self.other_xp.items()])

    @property
    def search_term(self) -> builtins.str:
        if self.name not in self.alias:
            return self.alias
        return self.name

    @staticmethod
    def get_image_path(candidate_image_paths: Dict[builtins.str, builtins.str],
                       candidate_keys: List[builtins.str]):
        for possible_name in candidate_keys:
            if candidate_image_paths.get(possible_name) is not None:
                return candidate_image_paths[possible_name]

    def set_image_path(self, candidate_image_paths: Dict[builtins.str,
                                                         builtins.str]):
        self.image_path = self.get_image_path(
            candidate_image_paths,
            [aidedd_text(self.image_key), aidedd_text(self.alias), aidedd_text(self.name)])

        # Set the reference image only if no other image was found in the campaign folder
        if self.image_path is None and self.reference_image_path is not None and "IMAGE_DIR" in os.environ:
            self.image_path = os.path.join(os.path.abspath(os.environ["IMAGE_DIR"]), self.reference_image_path)

        if self.appearance is not None:
            self.player_image_path = self.get_image_path(
                candidate_image_paths, [aidedd_text(self.appearance)])
        else:
            self.player_image_path = self.image_path

    @classmethod
    def xp_from_challenge(cls, challenge_rating: builtins.str) -> builtins.int:
        if challenge_rating == "0":
            return 0
        try:
            if int(challenge_rating) >= 30:
                return 155000
        except ValueError:
            pass
        return cls.xp_challenge_rating[challenge_rating]

    @classmethod
    def proficiency_bonus_from_challenge(cls, challenge_rating: builtins.str) -> builtins.int:
        if challenge_rating == "0":
            return +2
        try:
            if int(challenge_rating) > 30:
                return +9
        except ValueError:
            pass
        return cls.proficiency_bonus[challenge_rating]

    def ability_modifier(self, ability: builtins.str) -> builtins.int:
        """Find the ability modifier (with or without proficiency bonus)"""

        # Check if ability is present in ability proficiency list
        for ability_text in self.abilities:
            match = self.ability_regex.match(ability_text)
            if match is not None and match.group(1) == ability:
                return int(match.group(2))

        # Otherwise, take nearest characteristic to roll it
        return getattr(self, self.abilities_mapping[ability.lower()])

    def saving_throw_modifier(self, characteristic):
        for save in self.saving_throws:
            if characteristic in save:  # e.g., "Str +3"
                return int(save.split(" ")[-1])
        return getattr(self, characteristic.lower())

    def roll(self, ability: builtins.str) -> builtins.int:
        """Roll ability check"""

        return roll(modifier=self.ability_modifier(ability))

    def __str__(self):
        return f"Creature<name={self.name},alias={self.alias}>"

    def serialize(self) -> Dict[builtins.str, Any]:
        data = copy.deepcopy(vars(self))
        for key in ["traits", "actions", "reactions", "variant", "legendary_actions", "mythic_actions",
                    "lair_actions", "regional_effects", "spells", "known_spells"]:
            data[key] = [trait.serialize() for trait in data[key]]
        return data

    def __hash__(self):
        return hash(self.name + self.alias)

    def __lt__(self, c):
        return self.alias < c.alias

    def __eq__(self, other):
        return self.name == other.name and self.alias == other.alias
