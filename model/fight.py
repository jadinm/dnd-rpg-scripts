import copy
import os
from typing import Callable

import yaml

from db.query import DB
from model import Encounter, Creature
from utils import YAML_ARGS

CURRENT_FIGHT = os.path.join(DB, "current_fight.yaml")


class Fight(Encounter):
    def __init__(self, hash_path: Callable[[str], str],
                 characters: Encounter, encounter_file: str = CURRENT_FIGHT):
        self.hash_path = hash_path
        self.image_encrypted = {}
        super().__init__(encounter_file)
        self._load_from_file()
        self.update_encrypted_paths()
        self.characters = characters

    def _load_from_file(self):
        if os.path.exists(self.file):
            with open(self.file) as file_obj:
                self._data = yaml.load(file_obj,
                                       Loader=yaml.CSafeLoader)
        else:
            self._data = {}

    def write(self):
        with open(self.file, "w") as file_obj:
            yaml.dump(self._data, file_obj, **YAML_ARGS)

    def reset(self):
        if os.path.exists(self.file):
            with open(self.file, "w") as file_obj:
                yaml.dump({}, file_obj, **YAML_ARGS)
        self._load_from_file()
        self._creatures = None

    @property
    def data(self):
        return copy.deepcopy(self._data.get("creatures", []))

    @property
    def round(self):
        return self._data.get("round", 0)

    def update_encrypted_paths(self):
        for c in self.creatures:
            if c.image_path not in self.image_encrypted:
                self.image_encrypted[c.image_path] = \
                    self.hash_path(c.image_path)

    def update(self, data):
        self._data = data
        self.update_encrypted_paths()
        self.write()

    def update_initiative(self, c: Creature, initiative: int):
        for creature in self._data["creatures"]:
            if creature["alias"] == c.alias:
                creature["initiative"] = initiative
                self.write()
                break

    def toggle_condition(self, c: Creature, condition: str, add: bool = True):
        for creature in self._data["creatures"]:
            if creature["alias"] == c.alias:
                if not add:
                    creature["conditions"].remove(condition)
                elif condition not in creature["conditions"]:
                    creature["conditions"].append(condition)
                self.write()

    def player_filter(self, creature):
        """
            Returns a dictionary with properties that are visible by players
        """
        hashed_image = self.hash_path(creature.player_image_path)
        return {
            "image_hash": hash(hashed_image),
            "key": creature.__hash__(),
            "image": hashed_image,
            "conditions": creature.conditions,
            "initiative": creature.initiative,
            "active": creature.active,
            "alias": (creature.alias
                      if creature.player_image_path is None
                         or creature in self.characters.creatures
                      else "")
        }

    @property
    def visible_creatures(self):
        """
            Returns the visible creatures. If an invisible creature is
            playing, the player will see that it is the turn of the previous
            creature. This does not work on the start of a new round.
        """
        visible_creatures = []
        for creature in self.creatures:
            if not creature.visible:
                if creature.active and len(visible_creatures) > 0:
                    visible_creatures[-1]["active"] = True
            else:
                visible_creatures.append(self.player_filter(creature))
        return visible_creatures
