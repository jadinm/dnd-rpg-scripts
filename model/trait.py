import copy
from dataclasses import dataclass, field
from typing import Dict, Any
from typing import List, Union

from .spell import SpellLevel


@dataclass
class Trait:
    name: str
    description: str
    spells: List[Union[SpellLevel, Dict[str, Any]]] = field(default_factory=list)
    limit: int = 0
    expanded: int = 0

    def __post_init__(self):
        self.expanded = min(self.expanded, self.limit)
        self.spells = [SpellLevel(**s) for s in self.spells]

    def serialize(self) -> Dict[str, Any]:
        data = copy.deepcopy(vars(self))
        data["spells"] = [level.serialize() for level in data["spells"]]
        if len(data["spells"]) == 0:
            del data["spells"]
        return data
