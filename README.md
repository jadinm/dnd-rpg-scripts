# DnD Fight Tracker

This DnD fight tracker is a flask application
that can, among other things, track initiative,
show NPC stats, roll dices, present a player-side view
of the combat,...

## Installing dependencies

You need Python 3.7+ and additional dependencies
that you can install them with the following command
when in the repository root folder:

```console
$ pip install -r requirements.txt 
```

For javascript libraries, you need
[yarn](https://classic.yarnpkg.com/en/docs/install)
as well as node but its installation process is described
in yarn's documentation.

Then, the libraries are installed with the following command
when in the repository root folder:

```console
$ yarn install
```

### Music streaming setup

We use an icecast2 server under /stream path on port 8000. These options are not yet configurable in the application.
To setup the icecast2 server, refer to https://www.icecast.org/docs/icecast-trunk/basic_setup/#setting-up-icecast

While Mixxx is one of the best options to stream music to an icecast server,
it is anoying because it does not work with Pulseaudio (default on many Linux distributions)
You can use it directly on ASLA (the layer below pulseaudio) but you will not be able to listen to anything else but mixxx while doing so
The alternative is to install jackd, qjackctl and pulseaudio-module-jack.
And then configure qjackctl to run the following command after startup. Copy it into "Setup..." > "Options" > "Execute script after Startup":

```
pacmd set-default-sink jack_out
```

(credits: https://askubuntu.com/questions/572120/how-to-use-jack-and-pulseaudio-alsa-at-the-same-time-on-the-same-audio-device)

Finally you can configure the "Live Broadcast" options on Mixxx to match your icecast2 configurations

## Running the website

You need to define two environment variables: ``CAMPAIGN`` and ``IMAGE_DIR``.

The first one defines the path to the campaign folder
in order to load the encounter files that are stored there.
By default, ``campaign/example`` is selected.

The second one defines the path to the image folder of https://5e.tools [images](https://get.5e.tools/).
This variable is optional but helpful to get most images directly.

### Development mode

```bash
FLASK_APP = fight_tracker
FLASK_ENV = development
FLASK_DEBUG = 1
CAMPAIGN = /path/to/campaign/directory
IMAGE_DIR = /path/to/image/5e.tools/directory
python -m flask run
```

### Production mode

To deploy the server, we use gunicorn (behind a nginx) because it has support for websockets.
You can check the [script](server/server_provision.sh) to see which command line we are using to start the server.

We also have a [Python script](server/server.py) to update the server program and campaign folders before starting a new instance again.
These scripts are not generic, so you might need to modify them to match your setup.
It also starts the music streaming client: Mixxx with qjackctl

## Campaign format

The fight tracker reads a directory to retrieve all the encounters, the
images,...
This directory has to respect a minimal format:

```
.
├── characters.yaml
├── chapter
│   ├── encounter.yaml
|   └── other_encounter.yaml
├── other_chapter
│   ├── encounter.yaml
|   └── other_encounter.yaml
├── images
│   ├── creature_name.png
│   ├── other_creature_name.jpg
│   ├── pc
│   |   ├── pc_name.png
│   |   └── other_pc_name.jpg
│   └── npc
│       ├── npc_name.png
│       └── other_npc_name.jpg
└── figurines
    ├── pc
    |   └── .gitkeep
    └── npc
        └── .gitkeep
```

We explain the role of each file and folder in the following sections.
An example of campaign with a single chapter and two encounters can be found
in the folder [campaign/example](campaign/example).

### Encounters

Encounters are grouped by chapters.
Each directory in the campaign is considered as a chapter.
Encounters consist of list of creatures encoded as yaml files.

You can specify a creature or group of creatures with the same characteristics
with four notations.

```yaml
- Creature name
- Creature name
```

This creates two creature with alias and name being "Creature name". If the
creature exists in database, its stats are extracted from that. Otherwise,
default values are applied.

```yaml
- - 2
  - Creature name
```

This creates the same creatures as above. They have the same characteristics.

```yaml
- - Creature name
  - Creature alias
```

This creates one creature with name "Creature name" and alias "Creature alias".
This is useful to named NPC that have standardized stats. For instance, a
common bandit that needs to interact a bit longer with PC and hence need a name.


```yaml
- name: Creature name
  alias: Creature alias
  number: 1
  hp: 3
```

This is the most precise way of defining one or more creatures. You can
change every default value of a creature in database and specify the number
of such creatures that you want. This can also help to create creatures from
scratch. You can specify the following elements:

```yaml
- name: Creature name
  alias: <name>
  translation: null
  image_path: null
  appearance: null
  player_image_path: null
  note: ''
  ac: 0
  armor_desc: ''
  hpmax: 1
  hpmax_roll: None
  hp: <hpmax>
  xp: 0
  size: M
  type: Humanoïde (toute race)
  alignment: tout alignement
  speed: 9m
  strength: 0
  dexterity: 0
  constitution: 0
  intelligence: 0
  wisdom: 0
  charisma: 0
  saving_throws: []
  abilities: []
  damage_weaknesses: []
  damage_resistances: []
  damage_immunities: []
  condition_immunities: []
  senses: []
  languages: []
  traits: []
  spells: []
  actions: []
  reactions: []
  legendary_actions_desc: null
  legendary_actions: []
  lair_actions_desc: null
  lair_actions: []
  has_spellbook: false
  known_spells: []
```

The only required argument is the name. The other are optional and their
default values are given above.
You can look in the [creature database](db/creatures) for concrete examples. 

### Player characters

The ``characters.yaml`` file follows the same rules as a regular encounter file.
The only difference is that the creatures in this file, your player
characters are included in every fight that you start.

### Images and figurines

Each creature (npc or pc) has to have an image.
These images are stored in the ``images`` folder.
Named NPC can have their own illustration that you can add in ``images/npc``
while PC images are stored in ``images/pc``.

The image must have the same name as te creature alias (or name if no image
with the alias can be found). All image formats are accepted.

Figurines can be built from these images with the script and they are stored
in the ``figurines`` folder. More details can be found in
[a following section](#build-paper-figurines).

## Other scripts

There are numerous scripts in the repository to help you building your campaign
or to keep the fight tracker database up-to-date. Every script has a ``-h``
options that document its arguments. 

### Check your campaign files for mistakes

Because it is easy to make an encoding mistake, we provide a script to check
the validity of your campaign. This also checks the database of the fight
tracker. This script is [data_checker.py](data_checker.py).

### Build paper figurines

We provide a script to build your own figurines with only paper, scissors and
a stapler. The script [build_figurine.py](campaign/build_figurine.py) creates
for each creature a svg that contains copies of the figurines that you can
then print, cut, fold and finally staple to have your own figurine.

Each figurine has a different size depending on the size of the creature.
The number of figurines in a svg is the maximum number of times that a creature
is appearing in a given chapter.

These svg files are added to the ``figurines`` folder of your campaign.

### Evaluate the difficulty level of the encounters

The script [build_encounter_levels.py](campaign/build_encounter_levels.py)
produces yaml files that gives the challenge rating of the encounters
according to the guidelines in the dungeon master guide. The produced yaml
file can be found next to each tested encounter.

### Download stats of new monsters

To add the newest creatures the database, you can use a script that updates
stats from data that you can download from https://get.5e.tools/.
This script is [extract_5e_tools_creatures.py](db/extract_5e_tools_creatures.py).
You have to give it the path to the bestiary folder containing all the JSON files,
and the path to the image folder that can be downloaded also at https://get.5e.tools/.

### Download all spells

To be able to check that all spells are valid, we use a script to populate
the list of all spells from the data of https://5e.tools
with the script [extract_5e_tools_spells.py](db/extract_5e_tools_spells.py).
