import argparse
from typing import List, Dict

import yaml

from model import Campaign, Creature
from utils import YAML_ARGS

rating_str = [
    "too easy",
    "easy",
    "medium",
    "hard",
    "deadly"
]

thresholds_by_level = [
    [25, 50, 75, 100],
    [50, 100, 150, 200],
    [75, 150, 225, 400],
    [125, 250, 375, 500],
    [250, 500, 750, 1100],
    [300, 600, 900, 1400],
    [350, 750, 1100, 1700],
    [450, 900, 1400, 2100],
    [550, 1100, 1600, 2400],
    [600, 1200, 1900, 2800],
    [800, 1600, 2400, 3600],
    [1000, 2000, 3000, 4500],
    [1100, 2200, 3400, 5100],
    [1250, 2500, 3800, 5700],
    [1400, 2800, 4300, 6400],
    [1600, 3200, 4800, 7200],
    [2000, 3900, 5900, 8800],
    [2100, 4200, 6300, 9500],
    [2400, 4900, 7300, 10900],
    [2800, 5700, 8500, 12700]
]

multiplier_number_monsters = [
    (0, 0.5),  # There for large parties
    (1, 1),
    (2, 1.5),
    (3, 2),
    (7, 2.5),
    (11, 3),
    (15, 4),
    (1000, 5)  # There for small parties
]


def parse_args():
    parser = argparse.ArgumentParser(description="Computes the Challenge "
                                                 "Rating of encounters")
    parser.add_argument("campaign_dir", help="The folder of the campaign "
                                             "directory")
    parser.add_argument("players", help="The number of players", type=int)
    parser.add_argument("level_min", help="The minimum level of players",
                        type=int)
    parser.add_argument("level_max", help="The maximum level of players",
                        type=int)
    return parser.parse_args()


def compute_challenge_rating(player_nbr: int, player_level_min: int,
                             player_level_max: int,
                             encounter: List[Creature]) -> Dict:
    challenge_rating = {
        "sum_xp": sum([int(creature.xp) for creature in encounter]),
        "player_number": player_nbr
    }
    challenge_rating["xp_by_player"] = challenge_rating["sum_xp"] / player_nbr

    # Get multipliers depending on the number of monsters and players
    i = 0
    for thresh_num_monsters, _ in multiplier_number_monsters:
        if len(encounter) < thresh_num_monsters:
            break
        i += 1
    if player_nbr < 3:
        i += 1
    elif player_nbr > 5:
        i -= 1
    multiplier = multiplier_number_monsters[i - 1][1]

    # Get rating by comparing with thresholds
    challenge_rating["rating"] = {}
    for player_level in range(player_level_min, player_level_max + 1):
        rating = None
        for i, threshold in enumerate(thresholds_by_level[player_level]):
            if multiplier * challenge_rating["sum_xp"] < threshold * player_nbr:
                rating = rating_str[i]
                break
        if rating is None:
            rating = rating_str[-1]  # Deadly
        challenge_rating["rating"][player_level] = rating

    return challenge_rating


if __name__ == "__main__":
    args = parse_args()
    encounters = list(Campaign(args.campaign_dir).encounters)
    idx = 0
    for encounter in encounters:
        idx += 1
        print(f"[{idx}/{len(encounters)}] Processing encounter: "
              f"{encounter.file}")
        result = compute_challenge_rating(args.players, args.level_min,
                                          args.level_max, encounter.creatures)
        file_path = encounter.swap_extension("xp.yaml")
        with open(file_path, "w") as file_obj:
            yaml.dump(result, file_obj, **YAML_ARGS)
