import argparse
import base64
import imghdr
import os
from typing import Tuple, List

import svgwrite.image
import svgwrite.shapes
import svgwrite.text
from PIL import Image as PilImage
from natsort import natsorted
from wand.image import Image

from db.query import get_db_data_by_id
from model import Creature, Campaign

OUTER_MARGIN_MM = 5
INNER_MARGIN_MM = 10
A4_PAPER_LENGTH = 297
A4_PAPER_WIDTH = 210


def rect_x_mm(c: Creature) -> int:
    return c.width


def rect_y_mm(c: Creature) -> int:
    return min(rect_x_mm(c), Creature.width_by_size["TG"])


def rect_2_y_mm(c: Creature) -> int:
    return rect_x_mm(c) // 2


rect_2_x_mm = rect_x_mm


def size_sub_drawing(c: Creature) -> Tuple[int, int]:
    return c.width, (2 * creature.height + INNER_MARGIN_MM
                     + 2 * rect_y_mm(c) + 2 * rect_2_y_mm(c))


def max_columns(c: Creature) -> int:
    return (A4_PAPER_WIDTH - 2 * OUTER_MARGIN_MM) // size_sub_drawing(c)[0]


def max_rows(c: Creature) -> int:
    return (A4_PAPER_LENGTH - 2 * OUTER_MARGIN_MM) // size_sub_drawing(c)[1]


def parse_args():
    parser = argparse.ArgumentParser(description="Produces svg of "
                                                 "miniatures from monster "
                                                 "images")
    parser.add_argument("campaign_dir", help="The campaign folder")
    return parser.parse_args()


def mm_to_px(mm) -> float:
    return 793.70079 / 210 * mm


def draw_image(d: svgwrite.Drawing, x: float, y: float, img_data: str,
               img_gray_data: str, c: Creature):
    # Image to rotate => we set its center to origin first
    img = svgwrite.image.Image(href=img_data,
                               insert=(mm_to_px(-1 * c.width / 2),
                                       mm_to_px(-1 * c.height / 2)),
                               size=(mm_to_px(c.width),
                                     mm_to_px(c.height)))
    img.rotate(180)
    img.translate(-1 * mm_to_px(x + c.width / 2),
                  -1 * mm_to_px(y + c.height / 2))
    d.add(img)

    d.add(svgwrite.shapes.Rect(
        insert=(mm_to_px(x), mm_to_px(y - rect_y_mm(c) - rect_2_y_mm(c))),
        size=(mm_to_px(rect_x_mm(c)), mm_to_px(rect_y_mm(c))),
        fill='black'
    ))
    d.add(svgwrite.shapes.Rect(
        insert=(mm_to_px(x), mm_to_px(y - rect_2_y_mm(c))),
        size=(mm_to_px(rect_2_x_mm(c)), mm_to_px(rect_2_y_mm(c))),
        fill='gray'
    ))

    img = svgwrite.image.Image(href=img_gray_data,
                               size=(mm_to_px(c.width),
                                     mm_to_px(c.height)))
    img.translate(mm_to_px(x), mm_to_px(y + c.height + INNER_MARGIN_MM))
    d.add(img)

    d.add(svgwrite.shapes.Rect(
        insert=(mm_to_px(x), mm_to_px(y + 2 * c.height + INNER_MARGIN_MM)),
        size=(mm_to_px(rect_2_x_mm(c)), mm_to_px(rect_2_y_mm(c))),
        fill='gray'
    ))
    d.add(svgwrite.shapes.Rect(
        insert=(mm_to_px(x), mm_to_px(y + 2 * c.height + INNER_MARGIN_MM
                                      + rect_2_y_mm(c))),
        size=(mm_to_px(rect_x_mm(c)), mm_to_px(rect_y_mm(c))),
        fill='black'
    ))

    # Vertical lines for scissors
    d.add(svgwrite.shapes.Line(start=(mm_to_px(x), 0),
                               end=(mm_to_px(x), mm_to_px(A4_PAPER_LENGTH)),
                               stroke_width=1, stroke='lightgray'))
    d.add(svgwrite.shapes.Line(start=(mm_to_px(x + c.width), 0),
                               end=(mm_to_px(x + c.width),
                                    mm_to_px(A4_PAPER_LENGTH)),
                               stroke_width=1, stroke='lightgray'))

    # Horizontal lines for scissors
    start_line_y = mm_to_px(y - rect_y_mm(c) - rect_2_y_mm(c))
    end_line_y = mm_to_px(y + 2 * c.height + INNER_MARGIN_MM + rect_2_y_mm(c)
                          + rect_y_mm(c))
    d.add(svgwrite.shapes.Line(start=(0, start_line_y),
                               end=(mm_to_px(A4_PAPER_WIDTH), start_line_y),
                               stroke_width=1, stroke='lightgray'))
    d.add(svgwrite.shapes.Line(start=(0, end_line_y),
                               end=(mm_to_px(A4_PAPER_WIDTH), end_line_y),
                               stroke_width=1, stroke='lightgray'))


def get_grayscale(img_in_path: str) -> Tuple[str, str]:
    # Then get raw image data to encode into the SVG file.
    img = Image(filename=str(img_in_path))
    img_format = imghdr.what(img_in_path)
    img_blob = img.make_blob(format=img_format)
    img_64 = base64.b64encode(img_blob).decode()
    img_data = f'data:image/{img_format};base64,{img_64}'

    idx = str(img_in_path).rfind(".")
    grayscale_path = str(img_in_path)[:idx] + ".grayscale.png"
    if not os.path.exists(grayscale_path):
        # Transform image to shadow (grayscale + normalize color + flip
        # left right)
        img = PilImage.open(str(img_in_path)).convert('LA')
        datas = img.getdata()
        new_data = []
        for item in datas:
            if item[1] != 255:  # transparent
                new_data.append((item[0], 0))
                continue
            if item[0] >= 245 and item[1] == 255:  # almost white -> transparent
                new_data.append((255, 0))
            else:
                new_data.append((0, 255))
        img.putdata(new_data)
        img = img.transpose(PilImage.FLIP_LEFT_RIGHT)

        # Get the grayscale image for the back
        img.save(grayscale_path)

    gray_img = Image(filename=grayscale_path)
    gray_data = gray_img.make_blob(format="png")
    encoded = base64.b64encode(gray_data).decode()
    gray_data = f'data:image/png;base64,{encoded}'

    return img_data, gray_data


def create_svg(image_data: List[Tuple[str, str]], svg_out_path: str,
               c: Creature):
    dwg = svgwrite.Drawing(filename=svg_out_path,
                           size=(mm_to_px(210), mm_to_px(297)),
                           style="font-family:Arial;font-weight:bold")
    dwg.viewbox(width=mm_to_px(210), height=mm_to_px(297))

    idx = 0
    for i in range(max_columns(c)):
        for j in range(max_rows(c)):
            if idx >= len(image_data):  # No more images to add
                continue
            draw_image(dwg, OUTER_MARGIN_MM + size_sub_drawing(c)[0] * i,
                       OUTER_MARGIN_MM + rect_y_mm(c)
                       + rect_2_y_mm(c)
                       + size_sub_drawing(c)[1] * j,
                       image_data[idx][0], image_data[idx][1], c)
            idx += 1

    dwg.save(pretty=True, indent=4)


args = parse_args()

campaign = Campaign(args.campaign_dir)
images = campaign.retrieve_existing_images(include_creatures=True,
                                           include_npc=False, include_pc=False)
npc_images = campaign.retrieve_existing_images(include_creatures=False,
                                               include_npc=True,
                                               include_pc=False)
pc_images = campaign.retrieve_existing_images(include_creatures=False,
                                              include_npc=False,
                                              include_pc=True)

# Set creature image paths
for encounter in list(campaign.encounters):
    for creature in encounter.creatures:
        # Try first PNJ images, then generic ones
        creature.set_image_path(npc_images)
        if creature.image_path is None:
            creature.set_image_path(images)
        if creature.image_path is None:
            creature.set_image_path(images)
for creature in campaign.characters.creatures:
    if creature.image_path is None:
        creature.set_image_path(pc_images)

# Recover the maximum frequency of image path for a chapter
creature_frequencies = campaign.max_appearance_frequencies()

for image_in_path in natsorted(images.values()):
    print(f"Processing '{image_in_path}'")
    image_id = "".join(os.path.basename(image_in_path).split(".")[:-1])
    frequencies = creature_frequencies.get(image_in_path)
    if frequencies is None:
        print("\tWarning: Cannot find a creature for the image")
        continue

    # Build grayscale image
    base_data = get_grayscale(image_in_path)
    image_in_path = os.path.basename(image_in_path)

    # One set of figurines for each different size
    for size, size_freq in frequencies.items():
        creature, freq = size_freq
        # Copy the necessary amount of time
        data = [base_data for _ in range(freq)]

        # Write it out to the svg
        new_extension = f".{size}.svg" if len(frequencies) > 1 else ".svg"
        idx = str(image_in_path).rfind(".")
        image_out_path = str(image_in_path)[:idx] + new_extension
        image_out_path = os.path.join(campaign.figurine_dir, image_out_path)

        create_svg(data, image_out_path, creature)

for image_list in [natsorted(pc_images.values()),
                   natsorted(npc_images.values())]:
    svg_idx = {size: 0 for size in Creature.width_by_size.keys()}
    data = {size: [] for size in Creature.width_by_size.keys()}

    # Build one svg file by size of PC/NPC
    for img_idx, image_in_path in enumerate(image_list):
        print(f"Processing '{image_in_path}'")
        folder_id = os.path.basename(os.path.dirname(image_list[0]))
        frequencies = creature_frequencies.get(image_in_path)
        if frequencies is None:
            # Recover corresponding creature from database
            cid = ".".join(os.path.basename(image_in_path).split(".")[:-1])
            creature_data = get_db_data_by_id(cid)
            if creature_data is None:
                print("\tWarning: Cannot find a creature for the image")
            else:
                # Build grayscale image
                base_data = get_grayscale(image_in_path)
                # Append to correct svg
                data[creature_data["size"]].append(base_data)
        else:
            # Build grayscale image
            base_data = get_grayscale(image_in_path)

            # Recover all the sizes of the PC/NPC
            for size, size_freq in frequencies.items():
                creature, freq = size_freq
                data[size].extend([base_data for _ in range(freq)])

        # If we have enough data for a full svg
        # or if this is the last image, write data to the svg
        for size in data.keys():
            creature = Creature(size, size=size)
            if len(data[size]) == max_columns(creature) * max_rows(creature) \
                    or (img_idx == len(image_list) - 1 and len(data[size]) > 0):
                image_out_path = \
                    os.path.join(campaign.figurine_dir, folder_id,
                                 f"{folder_id}_{size}_{svg_idx[size]}.svg")
                image_id = "".join(os.path.basename(image_in_path)
                                   .split(".")[:-1])
                create_svg(data[size], image_out_path, creature)
                data[size] = []
                svg_idx[size] += 1
