import os
import re
import sys

import yaml
from natsort import natsorted

from db import iterate_db_creatures
from db.query import translations, CREATURES_DB
from model import Creature, Campaign, Encounter, Spell, Trait, SpellLevel
from utils import YAML_ARGS, DEFAULT_CAMPAIGN

regex_5e_tools_tag = re.compile(r"{@(\w+)(?: [|\w\d \"':.,;()/+-=!\[\]&]+)?}")
known_tags = ["spell", "item", "dc", "hit", "atk", "h", "url", "dice", "damage", "condition", "skill", "creature",
              "recharge", "chance", "i", "book", "adventure", "table", "action", "sense", "feat", "hitYourSpellAttack",
              "disease", "hazard", "b", "note", "hitYourSpellAttack", "filter"]


def assert_only_known_tag(text: str):
    if isinstance(text, list):
        print(f"\tW: '{text}' is a list and not a string")
        return
    for match in regex_5e_tools_tag.finditer(text):
        assert match.group(1) in known_tags, f"Found the invalid tag {match.group(0)}"


def check_creature(creature: Creature, source: Encounter):
    # Needed variables
    for var in ["name", "alias", "ac", "hp", "hpmax", "strength",
                "dexterity", "constitution", "intelligence", "wisdom",
                "charisma", "str", "dex", "con", "int", "wis", "cha", "type",
                "alignment", "size", "speed"]:
        try:
            assert getattr(creature, var) is not None, \
                f"\t[{source}] The creature {creature} has None as {var}"
            assert getattr(creature, var) != "", \
                f"\t[{source}] The creature {creature} has '' as {var}"
        except Exception as e:
            print(f"\tE: [{source}] Cannot query variable {var} of {creature}")
            raise e

    # Check that characteristics are sound
    characteristics = [creature.strength, creature.dexterity,
                       creature.constitution, creature.intelligence,
                       creature.wisdom, creature.charisma]
    assert any([c != 0 for c in characteristics]), \
        f"\t[{source}] The creature {creature} has 0 to all" \
        f" of its characteristics"

    # Check that all iterables are lists (and not strings)
    for var in ["saving_throws", "abilities", "damage_weaknesses",
                "damage_resistances", "damage_immunities",
                "condition_immunities", "senses", "languages", "traits",
                "spells", "actions", "reactions", "variant",
                "legendary_actions", "mythic_actions", "lair_actions", "regional_effects",
                "known_spells"]:
        value = getattr(creature, var)
        assert isinstance(value, list), \
            f"\t[{source}] The creature {creature} has a {type(value)} as {var}"

    # Check that all variables are integers (and not strings)
    for var in ["ac", "hpmax", "xp", "strength", "dexterity", "constitution", "intelligence", "wisdom", "charisma"]:
        value = getattr(creature, var)
        assert isinstance(value, int), \
            f"\t[{source}] The creature {creature} has a {type(value)} as {var}"

    # Check that senses have a passive perception
    senses = list(filter(lambda s: "Perception passive" in s or "Passive perception" in s, creature.senses))
    assert len(senses) >= 1, f"\t[{source}] The creature {creature} has" \
                             f" {len(senses)} entries on passive perception"

    # Check spell links
    spells = [spell for spell_level in creature.spells
              for spell in spell_level.spells] + creature.known_spells
    for spell in spells:
        assert spell.link is not None, \
            f"\t[{source}] The creature {creature} has an invalid spell:" \
            f" '{spell}'"

    # Check that only magicians have not empty known_spells
    assert len(creature.known_spells) == 0 or creature.has_spellbook, \
        f"\t[{source}] The creature {creature} is not a magician but has " \
        f"known spells"

    # Check for unknown tags {@<5e.tool-tag> <value>}
    for var in vars(creature):
        value = getattr(creature, var)
        if isinstance(value, str):
            assert_only_known_tag(value)
        elif isinstance(value, list):
            for item in value:
                if isinstance(item, str):
                    assert_only_known_tag(item)
                elif isinstance(item, Spell):
                    assert_only_known_tag(item.original_name)
                elif isinstance(item, SpellLevel):
                    assert_only_known_tag(item.name)
                    for spell in item.spells:
                        assert_only_known_tag(spell.original_name)
                elif isinstance(item, Trait):
                    assert_only_known_tag(item.name)
                    assert_only_known_tag(item.description)
                else:
                    raise ValueError(f"{var} of {creature} contains lists of {type(item)}")
        elif isinstance(value, dict):
            for key, value in value.items():
                if isinstance(key, str):
                    assert_only_known_tag(key)
                else:
                    raise ValueError(f"{var} of {creature} contains dictionary with key of type {type(key)}")
                if isinstance(value, int):
                    pass
                else:
                    raise ValueError(f"{var} of {creature} contains dictionary with values of type {type(key)}")
        elif isinstance(value, int) or value is None:
            pass
        else:
            raise ValueError(f"{var} of {creature} contains {type(value)}")


def check_all_encounters(campaign_dir):
    campaign = Campaign(campaign_dir, os.path.abspath(os.environ.get("IMAGE_DIR")))
    encounters = {}
    images = campaign.retrieve_existing_images()
    pc_images = campaign.retrieve_existing_images(include_pc=True)
    creature_images = []
    for encounter in list(campaign.encounters) + [campaign.characters]:
        try:
            creatures = list(encounter.creatures)
        except Exception as e:
            print(f"\tE: Cannot create the creatures of file "
                  f"{encounter}:\n\t\t{e}")
            continue

        for c in creatures:
            check_creature(creature=c, source=encounter)

    # Check that an image exists for each creature
    for encounter in list(campaign.encounters) + [campaign.characters]:
        for c in encounter.creatures:
            if encounter == campaign.characters:
                c.set_image_path(pc_images)
            else:
                c.set_image_path(images)
            assert c.image_path is not None, \
                f"\t[{encounter}] {c} has no image associated in " \
                f"campaign '{campaign}'"
            assert c.player_image_path is not None, \
                f"\t[{encounter}] {c} has no player image associated in " \
                f"campaign '{campaign}'"
            if c.image_path not in creature_images:
                creature_images.append(c.image_path)
            if c.player_image_path not in creature_images:
                creature_images.append(c.player_image_path)

        encounter.reformat()

    # Check that all images were used
    for image in images.values():
        if image not in creature_images:
            print(f"\tW: [{campaign}] image {image} is not used by any"
                  f" creature")
    return encounters


def check_database():
    for db_file in natsorted(iterate_db_creatures()):
        with open(db_file) as file_obj:
            if ".yaml" != db_file[-5:]:
                continue
            try:
                origin_yaml = file_obj.read()
                d = yaml.load(origin_yaml, Loader=yaml.CSafeLoader)
            except yaml.YAMLError as e:
                print(f"\tE: Cannot decode file {db_file}:\n{e}")
                continue

        try:
            c = Creature(**d)
        except TypeError as e:
            print(f"\tE: Cannot create creature from file {db_file}:\n{e}")
            continue
        except ValueError as e:
            print(f"\tE: Cannot create creature from file {db_file}:\n{e}")
            continue

        check_creature(creature=c, source=db_file)
        # Reformat correctly the file
        new_yaml = yaml.dump(c.serialize(), **YAML_ARGS)
        if origin_yaml != new_yaml:
            with open(db_file, "w") as file_obj:
                file_obj.write(new_yaml)

    # Check that translations are pointing to an actual creature file
    for french, english in translations.items():
        assert os.path.exists(os.path.join(CREATURES_DB, english + ".yaml")), \
            f"Translation '{french}'->'{english}' is invalid"

    # Check that every spell can be parsed
    Spell.filter(min_level=0, max_level=9, filter_out_ua=False, dnd_class=None)


print("Checking database")
check_database()

for directory in [DEFAULT_CAMPAIGN] + sys.argv[1:]:
    print(f"Checking campaign {directory}")
    check_all_encounters(directory)
