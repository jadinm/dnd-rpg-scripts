import http
import os
import random
import re
import shutil
from datetime import datetime, timedelta
from threading import Lock
from typing import List, Callable
from urllib.parse import urlparse

import yaml
from flask import Flask, render_template, request, url_for, send_file, redirect, session
from flask_socketio import SocketIO, emit
from flask_user import UserManager, login_required, current_user
from natsort import natsorted

from db import get_db_data, get_all_creature_names
from model import Creature, Campaign, Encounter, Spell
from model.conditions import conditions_tooltips
from model.fight import Fight
from model.user import DM, db
from utils import aidedd_text, DEFAULT_CAMPAIGN, SOURCE_DIR, roll, YAML_ARGS

app = Flask(__name__)
app.secret_key = os.environ.get("SECRET_KEY", False) or os.urandom(24).hex()
app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///db/fight_tracker.sqlite"
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
app.config["USER_APP_NAME"] = "Fight Tracker"
app.config["USER_ENABLE_REGISTER"] = False
app.config["USER_ENABLE_CHANGE_PASSWORD"] = False
app.config["USER_ENABLE_EMAIL"] = False
app.config["USER_ENABLE_USERNAME"] = True
app.config["USER_REQUIRE_RETYPE_PASSWORD"] = False
app.config["USER_ENABLE_REMEMBER_ME"] = False
app.config["USER_LOGIN_TEMPLATE"] = "login.html"

socketio = SocketIO(app)

db.init_app(app)
with app.app_context():
    db.create_all()
user_manager = UserManager(app, db, DM)

src_dir = os.path.dirname(os.path.abspath(__file__))
campaign_dir = os.path.abspath(os.environ.get("CAMPAIGN", DEFAULT_CAMPAIGN))
reference_dir = os.path.abspath(os.environ.get("IMAGE_DIR"))
campaign = Campaign(campaign_dir, reference_dir)
all_images = campaign.retrieve_existing_images(include_pc=True)
db_creature_names = natsorted(get_all_creature_names())
current_fight_lock = Lock()

ability_dice_pattern = re.compile(r"\(?(?<!\d)([-+]\d+)\)?")
damage_dice_pattern = re.compile(r"\(?(\d+)d(\d+)( ?[-+] ?\d+)?\)?")
recharge_dice_pattern = re.compile(r"\([Rr]echarge (\d+(?:-\d+)?)\)")


@app.before_request
def make_session_permanent():
    """Make connections hold for a full day to avoid problems during a session"""
    session.permanent = True
    app.permanent_session_lifetime = timedelta(days=1)


@app.template_filter()
def escape_slash(text):
    return text.replace("\\n", "\\\\n")


@app.template_filter()
def domain_name(text):
    return urlparse(text).hostname


@app.template_filter()
def natsort(items):
    return natsorted(items)


@app.template_filter()
def aidedd(text):
    return aidedd_text(text)


@app.template_filter()
def abilitymodifier(text):
    return f"+{int(text)}" if int(text) >= 0 else text


@app.template_filter()
def visual_spellbook(spellbook: List[Spell]) -> str:
    return yaml.dump([[s.name, s.link] for s in spellbook], **YAML_ARGS)


dice_font = {6: "fa-dice", 20: "fa-dice-d20"}
dice_format = "<i class=\"fas {dice_font} dice fa-lg\" " \
              "number={number} data-toggle=\"tooltip\" data-html=\"true\" title=\"Roll\" type={dice_type} " \
              "modifier={modifier}></i>"


@app.template_filter()
def diceroller(text, size="4%", enable_dice=True):
    if not enable_dice:
        return text
    text = str(text)

    def ability_insert(match):
        modifier = int(match.group(1))
        width = size
        return match.group(0) + " " \
               + dice_format.format(number=1, dice_type=20, size=width, modifier=modifier, dice_font=dice_font[20])

    def damage_insert(match):
        number = int(match.group(1))
        dice_type = int(match.group(2))
        if match.group(3) is None:
            modifier = 0
        else:
            modifier = int(match.group(3).replace(" ", ""))
        font = dice_font[dice_type] if dice_type in dice_font else dice_font[20]
        return match.group(0) + " " \
               + dice_format.format(number=number, dice_type=dice_type, size=size, modifier=modifier, dice_font=font)

    def recharge_insert(match):
        number = 1
        dice_type = 6
        modifier = 0
        font = dice_font[6]
        return match.group(0) + " " \
               + dice_format.format(number=number, dice_type=dice_type, size=size, modifier=modifier, dice_font=font)

    text = ability_dice_pattern.sub(ability_insert, text)
    text = damage_dice_pattern.sub(damage_insert, text)
    text = recharge_dice_pattern.sub(recharge_insert, text)
    return text


# Process 5e.tools tags
item_regex = re.compile(r"{@item ([\w '/+]+)(?:\|(\w+))?(?:\|([\w ]+))?}")
dc_regex = re.compile(r"{@dc (\d+)}")
hit_regex = re.compile(r"{@hit (-?\d+)}")
attack_regex = re.compile(r"{@atk (mw)?,?(rw)?(ms)?(rs)?}")
hit_text_regex = re.compile(r"{@h}")
hit_your_spell_attack_regex = re.compile(r"{@hitYourSpellAttack}")
url_regex = re.compile(r"{@url ([\w '/.]+)(?:\|(\w+))?\|([\w '/]+)}")
dice_regex = re.compile(r"{@(?:dice|damage) (\d+)?d(\d+)(?: ([+-]) (\d+))?}")
condition_regex = re.compile(r"{@condition ([\w '/.]+)(?:\|(\w+))?}")
skill_regex = re.compile(r"{@skill ([\w '/.]+)}")
creature_regex = re.compile(r"{@creature ([\w '/-]+)(?:\|(\w+)?)?(?:\|(\w+))?}")
recharge_regex = re.compile(r"{@recharge (\d(?:-\d)?)}")
percentage_regex = re.compile(r"{@chance [\w '/-]+\|([ \w]+)\|[ \w%]+}")
italic_regex = re.compile(r"{@i ([\w '/.+-]+)}")
bold_regex = re.compile(r"{@b ([\w '/.+-]+)}")
note_regex = re.compile(r"{@note (.+)}")
book_regex = re.compile(r"{@book ([\w '/+-]+)\|([|\w '/+-]+)}")
adventure_regex = re.compile(r"{@adventure ([\w '/+-]+)\|([|\w '/+-]+)}")
table_regex = re.compile(r"{@table ([\w '/+-]+)\|(\w+)}")
action_regex = re.compile(r"{@action ([\w '/+-]+)(?:\|(\w+))?}")
senses_regex = re.compile(r"{@sense ([\w '/+-]+)(?:\|(\w+))?}")
feat_regex = re.compile(r"{@feat ([\w '/+-]+)(?:\|(\w+))?}")
disease_regex = re.compile(r"{@disease ([\w '/+-]+)(?:\|(\w+))?}")
hazard_regex = re.compile(r"{@hazard ([\w '/+-]+)(?:\|(\w+))?}")
filter_regex = re.compile(r"{@filter ([\w ()'/-]+)\|(\w+)\|((?:(?:\w| )+=[!0-9; \w&\[\]]+\|?)+)}")
filter_range_regex = re.compile(r"\[&(\d+)(?:;&(\d+))?]")


def add_generic_link(subpath: str, default_suffix: str) -> Callable[[re.Match], str]:
    def link(match: re.Match) -> str:
        suffix = "_" + (match.group(2).lower() if match.group(2) else default_suffix)
        alias = match.group(3) if len(match.groups()) > 2 and match.group(3) else match.group(1)
        return f'<a class="btn btn-link m-0 p-0 align-baseline" target="_blank"' \
               f' href="https://{request.host}/5e/{subpath}#{match.group(1).lower().replace("+", "%2b")}{suffix}">{alias}</a>'

    return link


def add_attack_text(match: re.Match) -> str:
    if match.group(1) and match.group(2):
        return "<i>Melee or Ranged Weapon Attack:</i>"
    elif match.group(1):
        return "<i>Melee Weapon Attack:</i>"
    elif match.group(2):
        return "<i>Ranged Weapon Attack:</i>"
    elif match.group(3):
        return "<i>Melee Spell Attack:</i>"
    else:
        return "<i>Ranged Spell Attack:</i>"


def add_link(match: re.Match) -> str:
    suffix = "#" + match.group(2).lower() if match.group(2) else ""
    return f'<a class="btn btn-link m-0 p-0 align-baseline" target="_blank"' \
           f' href="https://{request.host}/5e/{match.group(1).lower()}{suffix}">{match.group(3)}</a>'


def add_dice(match: re.Match) -> str:
    modifier = int(match.group(4)) if match.group(4) else 0
    modifier = -1 * modifier if match.group(3) and match.group(3) == "-" else modifier
    number = int(match.group(1)) if match.group(1) else 1
    dice_type = int(match.group(2))

    suffix = f"{modifier:+d}" if modifier != 0 else ""
    font = dice_font[dice_type] if dice_type in dice_font else dice_font[20]
    return f"{number}d{dice_type}{suffix} " + \
           dice_format.format(number=number, dice_type=dice_type, size="4%", modifier=modifier, dice_font=font)


def add_d20_dice(match: re.Match) -> str:
    modifier = int(match.group(1))
    return f"{modifier:+d} " + \
           dice_format.format(number=1, dice_type=20, size="4%", modifier=modifier, dice_font=dice_font[20])


def add_spell_link(match: re.Match) -> str:
    name, link = Spell.to_spell_link(match)
    return f'<a class="btn btn-link m-0 p-0 align-baseline" target="_blank" href="{link}">{name}</a>'


def add_recharge_dice(match: re.Match) -> str:
    return f"Recharge {match.group(1)} " \
           + dice_format.format(number=1, dice_type=6, size="4%", modifier=0, dice_font=dice_font[6])


def add_percentage_dice(match: re.Match) -> str:
    return f"{match.group(1)} " \
           + dice_format.format(number=1, dice_type=100, size="4%", modifier=0, dice_font=dice_font[20])


def add_book_link(match: re.Match) -> str:
    qualifiers = ",".join(match.group(2).lower().split("|"))
    return f'<a class="btn btn-link m-0 p-0 align-baseline" target="_blank"' \
           f' href="https://{request.host}/5e/book.html#{qualifiers}">{match.group(1)}</a>'


def add_adventure_link(match: re.Match) -> str:
    qualifiers = ",".join(match.group(2).lower().split("|"))
    return f'<a class="btn btn-link m-0 p-0 align-baseline" target="_blank"' \
           f' href="https://{request.host}/5e/adventure.html#{qualifiers}">{match.group(1)}</a>'


def add_filter_regex(match: re.Match) -> str:
    args = []
    for arg_desc in match.group(3).split("|"):
        arg_name, arg_value = arg_desc.split("=")
        convert_value = 2 if "!" in arg_value else 1
        arg_value = arg_value.replace("!", "")
        sub_match = filter_range_regex.search(arg_value)
        if sub_match:
            min_arg_value = sub_match.group(1)
            max_arg_value = sub_match.group(2) if sub_match.group(2) else min_arg_value
            arg_value = f"min=&{min_arg_value}~max=&{max_arg_value}"
        else:
            arg_value = "~".join(map(lambda x: f"{x}={convert_value}", arg_value.split(";")))
        args.append(f"flst{arg_name}:{arg_value}")

    url = f'https://{request.host}/5e/{match.group(2)}.html#blankhash,{",".join(args)}'
    print(url)
    return f'<a class="btn btn-link m-0 p-0 align-baseline" target="_blank"' \
           f' href="{url}">{match.group(1)}</a>'


@app.template_filter()
def parse_tags(text: str) -> str:
    text = item_regex.sub(add_generic_link("items.html", "dmg"), text)
    text = dc_regex.sub(r"DC \1", text)
    text = hit_regex.sub(add_d20_dice, text)
    text = attack_regex.sub(add_attack_text, text)
    text = hit_text_regex.sub("<i>Hit:&nbsp;</i>", text)
    text = hit_your_spell_attack_regex.sub("<i>your spell attack modifier</i>", text)
    text = url_regex.sub(add_link, text)
    text = dice_regex.sub(add_dice, text)
    text = Spell.SPELL_LINK_REGEX.sub(add_spell_link, text)
    text = condition_regex.sub(add_generic_link("conditionsdiseases.html", "phb"), text)
    text = skill_regex.sub(r"\1", text)
    text = creature_regex.sub(add_generic_link("bestiary.html", "mm"), text)
    text = recharge_regex.sub(add_recharge_dice, text)
    text = percentage_regex.sub(add_percentage_dice, text)
    text = italic_regex.sub(r"<i>\1</i>", text)
    text = bold_regex.sub(r"<b>\1</b>", text)
    text = book_regex.sub(add_book_link, text)
    text = adventure_regex.sub(add_adventure_link, text)
    text = table_regex.sub(add_generic_link("tables.html", "phb"), text)
    text = action_regex.sub(add_generic_link("actions.html", "phb"), text)
    text = senses_regex.sub(r"\1", text)
    text = text.replace("{@hitYourSpellAttack}", "your spell attack modifier")
    text = feat_regex.sub(add_generic_link("feats.html", "phb"), text)
    text = disease_regex.sub(add_generic_link("conditionsdiseases.html", "phb"), text)
    text = hazard_regex.sub(add_generic_link("trapshazards.html", "dmg"), text)
    text = filter_regex.sub(add_filter_regex, text)

    # Keep last because notes contain other elements
    text = note_regex.sub(r'<i style="color:#808080">\1</i>', text)

    return text


@app.template_filter()
def condition_style(text, conditions=None):
    if not isinstance(conditions, list):
        return f"\"{text}\""

    if text in conditions:
        style = "opacity: 1;"
    else:
        style = ""

    return f"title=\"{text}{conditions_tooltips[text]}\" style=\"{style}\" data-toggle=\"tooltip\"" \
           f" data-placement=\"bottom\" data-html=\"true\" data-condition=\"{text}\" data-container=\"body\""


@app.template_filter()
def use_initial_style(use_index, expanded_use):
    if use_index <= expanded_use:
        # jinja2 indices start at 1, already used => hide
        return 'style="opacity: 0.2"'
    return ""


@app.template_filter()
def condition_list(conditions):
    if not isinstance(conditions, list):
        return f"\"{conditions}\""

    s = []
    # Replace condition by an image with a tooltip if such image is available
    for condition in conditions:
        if os.path.exists(os.path.join(src_dir, "static/img", condition + ".svg")):
            s.append(f'<img src="{url_for("static", filename="img/" + condition + ".svg")}" alt="" class="condition"'
                     f' title="{condition}" data-toggle="tooltip" data-placement="bottom" data-html="true"'
                     f' style="opacity: 1; height: 1em"/>')
        else:
            s.append(condition)
    return ", ".join(s)


@app.template_filter()
def damage_type_list(damage_types):
    if not isinstance(damage_types, list):
        return f"\"{damage_types}\""

    same_suffix = {}  # e.g., " from nonmagical attacks"
    for damage_type in damage_types:
        split_text = damage_type.split(" ")
        same_suffix.setdefault(" ".join(split_text[1:]), []).append(split_text[0])
    # e.g., "poison, fire; bludgeoning, slashing from nonmagical attacks"
    return "; ".join([", ".join(group) + " " + suffix for suffix, group in same_suffix.items()])


@app.template_filter()
def serialize(creature_list: List[Creature]):
    return [c.serialize() for c in creature_list]


def hash_path(text: str) -> str:
    if text is None:
        return ""
    return url_for("download_image", path=campaign.image_hashes[text],
                   anonymous=1)


def dehash_path(text: str) -> str:
    return campaign.reverse_image_hash[text]


def get_sorted_encounters_by_chapter():
    chapters = {}
    for chapter in campaign.chapters:
        for encounter in chapter.encounters:
            chapters.setdefault(chapter.name, []) \
                .append((encounter.file, encounter.name))
    return chapters


@app.route("/image", methods=["GET"])
def download_image():
    path = request.args.get("path")
    if path is None:
        return '', http.HTTPStatus.NOT_FOUND
    if request.args.get("anonymous", False):
        try:
            path = dehash_path(path)
        except Exception:
            path = ""
    path = os.path.abspath(path)
    if (campaign.image_dir in path or os.path.abspath(os.environ.get("IMAGE_DIR", "")) in path) \
            and os.path.exists(path):
        return send_file(path)
    return '', http.HTTPStatus.NOT_FOUND


@app.route("/", methods=['GET'])
def home():
    with current_fight_lock:
        fight = Fight(hash_path, campaign.characters)
        return render_template('home.html', creatures=fight.visible_creatures,
                               round=fight.round, conditions_tooltips=conditions_tooltips)


@socketio.on('connect', namespace="/dm")
def connect_handler():
    # Reject non connected users to dm-space
    if not current_user.is_authenticated:
        return False


@app.route("/fight", methods=['POST', 'GET'])
@login_required
def fight_tracker():
    # Build creature list
    creatures: List[Creature] = []

    if request.method == "POST":
        # Add new encounters to the current fight
        encounters = request.form.getlist("encounter")
        for encounter_file in encounters:
            creatures.extend(Encounter(encounter_file).creatures)

        # Add new creature to the current fight
        creature_name = request.form.get("creature-name")
        if creature_name is not None:  # Search database
            db_data = get_db_data(creature_name)
            if db_data is not None:
                creatures.append(Creature(**db_data))

    # Retrieve current fight
    with current_fight_lock:
        fight = Fight(hash_path, campaign.characters)

        # Build player list
        players = list(campaign.characters.creatures)
        for i in range(len(players)):
            for c in fight.creatures:
                if c.alias == players[i].alias:
                    players[i] = c

        # Extract creatures from current fight
        current_creatures = [c for c in fight.creatures if c not in players]

    # Find images
    for c in creatures + players:
        c.set_image_path(all_images)
        if c.image_path is None:
            print(f"Creature '{c}' has no associated image")

    # Prepare duplicates
    duplicates = {}
    existing_aliases = set()
    for c in current_creatures + creatures:
        try:
            split = c.alias.split(" ")
            int(split[-1])
            # The creature alias is in the form "Bandit 1"
            # so the group of common creatures share "Bandit"
            alias = " ".join(split[:-1])
        except ValueError:
            # The alias does not contain a number
            alias = c.alias
        duplicates.setdefault(alias, []).append(c)
        existing_aliases.add(c.alias)
        existing_aliases.add(alias)

    # Add numbers if creatures have the same aliases
    for c in creatures:
        alias = c.alias
        if c.alias in duplicates and len(duplicates[c.alias]) > 1:  # A duplicate
            i = 1
            while c.alias in existing_aliases:
                c.alias = f"{alias} {i}"
                i += 1
            existing_aliases.add(c.alias)
        duplicates.setdefault(alias, []).append(c)

        # Add random spells in spellbook if it has one
        if c.has_spellbook and len(c.known_spells) == 0:
            c.generate_random_spellbook()

    # Roll initiative (either by group or individual)
    initiatives = {}
    group_initiative = len(creatures) > len(players)
    for alias, creature_dups in duplicates.items():
        for c in creature_dups:
            if c.initiative != 0:
                continue
            if group_initiative:
                if alias not in initiatives:
                    initiatives[alias] = roll(modifier=c.dex)
                initiatives[hash(c)] = initiatives[alias]
            else:
                initiatives[hash(c)] = roll(modifier=c.dex)
            c.initiative = initiatives[hash(c)]

    creatures = sorted([c for c in creatures + current_creatures],
                       key=lambda x: x.initiative, reverse=True)
    return render_template('fight_tracker.html', creatures=creatures,
                           players=players, enable_dice=True,
                           all_encounters=get_sorted_encounters_by_chapter(),
                           creature_names=db_creature_names)


@app.route("/update-fight", methods=['POST'])
@login_required
def update_fight():
    with current_fight_lock:
        fight = Fight(hash_path, campaign.characters)
        fight.update(request.get_json())

        # Notify clients from the update
        fight = Fight(hash_path, campaign.characters)
        emit("update-fight", {"creatures": fight.visible_creatures, "round": fight.round}, broadcast=True,
             namespace="/")
        # Update PC initiative (otherwise reactjs won't update text input that can be modified by users)
        for c in fight.visible_creatures:
            if len(c["alias"]) > 0:
                emit("initiative-updated", {"pc": c["alias"], "init": c["initiative"]}, namespace="/", broadcast=True)
    return '', http.HTTPStatus.NO_CONTENT


@app.route("/save-fight", methods=["GET"])
@login_required
def save_fight():
    with current_fight_lock:
        fight = Fight(hash_path, campaign.characters)
        filename = datetime.now().strftime("db/.old/fight_%m-%d-%Y_%H-%M-%S.yaml")
        shutil.copy(fight.file, os.path.join(SOURCE_DIR, filename))
    return redirect(url_for("fight_tracker"))


@app.route("/reset-fight", methods=["GET"])
@login_required
def reset_fight():
    with current_fight_lock:
        fight = Fight(hash_path, campaign.characters)
        fight.reset()
    return redirect(url_for("fight_tracker"))


@socketio.on("update-initiative", "/")
def player_update_initiative(json_data):
    with current_fight_lock:
        # If player exists, update
        fight = Fight(hash_path, campaign.characters)
        try:
            player = next(filter(lambda x: x.alias == json_data["pc"], fight.characters.creatures))
            fight.update_initiative(player, int(json_data["init"]))
            emit("update-initiative", json_data, namespace="/dm", broadcast=True)
            emit("initiative-updated", json_data, namespace="/", broadcast=True, include_self=False)
        except ValueError:
            pass
        except StopIteration:
            pass
        except KeyError:
            pass


@socketio.on("condition-toggle", "/")
def player_toggle_condition(json_data):
    with current_fight_lock:
        # If player exists, update
        fight = Fight(hash_path, campaign.characters)
        try:
            player = next(filter(lambda x: x.alias == json_data["pc"], fight.characters.creatures))
            condition = next(
                filter(lambda x: x == json_data["condition"],
                       ["Opportunity attack used", "Reaction used", "Readied action", "Concentrating on a spell",
                        "Hasted", "Invisible", "On fire", "Transformed"]))
            fight.toggle_condition(player, condition, add=json_data["up"])
            emit("condition-toggle", json_data, namespace="/dm", broadcast=True)
            emit("condition-toggled", json_data, namespace="/", broadcast=True, include_self=True)
        except ValueError:
            pass
        except StopIteration:
            pass
        except KeyError:
            pass


@socketio.on("roll-insight", "/")
def player_roll_insight(json_data):
    # If player exists, roll insight and show result only to the DM
    try:
        player = next(filter(lambda x: x.alias == json_data["pc"], campaign.characters.creatures))
        player.set_image_path(all_images)
        emit("player-message", {"message": {"header": "Insight",
                                            "player": url_for("download_image", path=player.image_path),
                                            "message": player.roll("Insight")}},
             namespace="/dm", broadcast=True)
    except ValueError:
        pass
    except StopIteration:
        pass
    except KeyError:
        pass


@app.route("/creatures", methods=["GET"])
@login_required
def npc_list():
    with current_fight_lock:
        # Retrieve current fight creatures
        fight = Fight(hash_path, campaign.characters)
        creatures = [c for c in fight.creatures
                     if c not in campaign.characters.creatures]

    # Remove duplicates (creatures that have the same name and alias if you remove the index)
    unique_creatures = set()
    for c in creatures:
        try:
            split = c.alias.split(" ")
            int(split[-1])
            # The creature alias is in the form "Bandit 1"
            # so the group of common creatures share "Bandit"
            alias = " ".join(split[:-1])
        except ValueError:
            # The alias does not contain a number
            alias = c.alias
        c.alias = alias
        if c not in unique_creatures:
            unique_creatures.add(c)

    return render_template('npc_list.html',
                           creatures=natsorted(list(unique_creatures),
                                               key=lambda x: x.alias),
                           theme=request.args.get("theme"),
                           enable_dice=request.args.get("remove_dice") is None)


@app.route("/search", methods=["GET", "POST"])
@login_required
def search_creature():
    creature = None

    encounter_creatures = []
    for encounter in campaign.encounters:
        encounter_creatures.extend(encounter.creatures)

    creature_names = {c.alias for c in encounter_creatures}
    creature_names.update(db_creature_names)
    creature_names = natsorted(list(creature_names))

    if request.method == "POST":  # Get creature name
        creature_name = request.form.get("creature-name")

        # Search all encounters
        for c in encounter_creatures:
            if c.alias == creature_name:
                creature = c
                break

        if creature is None:  # Search database
            db_data = get_db_data(creature_name)
            if db_data is not None:
                creature = Creature(**db_data)

        if creature is not None:
            creature.set_image_path(all_images)

    return render_template("search_creature.html", creature=creature,
                           creature_names=creature_names, enable_dice=True)


@app.route("/treasure", methods=["GET"])
@login_required
def generate_treasure():
    return render_template("generate_treasure.html")


@app.route("/spells", methods=["GET", "POST"])
def wizard_spells():
    spells = []
    if request.method == "POST":  # Show spell names
        try:
            level = int(request.form.get("spell-level"))
            nbr_spells = int(request.form.get("spell-number"))
            nbr_spellbooks = int(request.form.get("spellbook-number"))
            if 0 <= level <= 9 and nbr_spells > 0 and nbr_spellbooks > 0:
                for _ in range(nbr_spellbooks):
                    spells += random.sample(Spell.filter(min_level=level, max_level=level), nbr_spells)
                spells = sorted(list(set(spells)))
        except ValueError:
            pass

    return render_template("spells.html", spells=spells)
