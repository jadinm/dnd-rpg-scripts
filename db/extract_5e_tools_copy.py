import copy
import json
import math
import os
import re
from typing import Any, Dict, List, Union, Callable

from model import Creature

generic_replace_trait_regex = re.compile(r"<\$.+\$>")
dc_regex = re.compile(r"{@dc (\d+)}")
hit_regex = re.compile(r"{@hit (-?\d+)}")


def trait_list(bestiary_folder):
    with open(os.path.join(bestiary_folder, "traits.json")) as file_obj:
        return json.load(file_obj)["trait"]


def replace_text(mod_value: Dict[str, Any], candidate: Dict[str, Any], key_candidate: str = None,
                 ignore_name: bool = False):
    """Replace all element values and it children containing a give text described by mod_value"""

    insensitive_replace = re.compile(re.escape(mod_value["replace"]), re.IGNORECASE)

    for key, value in candidate.items():
        if key_candidate and key != key_candidate or key == "name" and ignore_name:
            continue

        if isinstance(value, str):
            candidate[key] = insensitive_replace.sub(mod_value["with"], value)
        elif isinstance(value, dict):
            replace_text(mod_value, value)
        elif isinstance(value, list):

            for i, item in enumerate(value):
                if isinstance(item, str):
                    value[i] = insensitive_replace.sub(mod_value["with"], item)
                elif isinstance(item, dict):
                    replace_text(mod_value, item)
                elif isinstance(item, list):
                    replace_text(mod_value, {"list": item})
                else:  # booleans and integers
                    pass
        else:
            pass  # booleans and integers


def replace_array(mod_value: Dict[str, Any], candidate: Dict[str, Any], key_candidate: str):
    """Replace elements in array"""

    insert_index = -1
    for i, item in enumerate(candidate[key_candidate]):
        if item["name"] == mod_value["replace"]:
            insert_index = i
            replacing_element = mod_value["items"]
            if isinstance(replacing_element, list):
                replacing_element = replacing_element[0]
            for key, value in replacing_element.items():
                item[key] = value

    # Insert additional elements after the index (not really replacement)
    replacing_elements = mod_value["items"]
    if isinstance(replacing_elements, list) and insert_index >= 0:
        for replacing_element in replacing_elements[1:]:
            insert_index += 1
            candidate[key_candidate].insert(insert_index, replacing_element)


def append_array(mod_value: Dict[str, Any], candidate: Dict[str, Any], key_candidate: str, check_exist: bool = False):
    """Append elements to array"""

    new_elements = mod_value["items"]
    if not isinstance(new_elements, list):
        new_elements = [new_elements]
    candidate.setdefault(key_candidate, [])
    for new_element in new_elements:
        if not check_exist or new_element not in candidate[key_candidate]:
            candidate[key_candidate].append(new_element)


def insert_array(mod_value: Dict[str, Any], candidate: Dict[str, Any], key_candidate: str, insertion_index: int = 0):
    """Prepend elements in an array"""

    new_elements = mod_value["items"]
    if not isinstance(new_elements, list):
        new_elements = [new_elements]
    for new_element in new_elements:
        candidate.setdefault(key_candidate, []).insert(insertion_index, new_element)
        insertion_index += 1


def remove_array(mod_value: Dict[str, Any], candidate: Dict[str, Any], key_candidate: str):
    """Remove elements in an array"""

    to_remove_elements = mod_value.get("names")
    if to_remove_elements:
        if not isinstance(to_remove_elements, list):
            to_remove_elements = [to_remove_elements]
        indices = [i for i, elem in enumerate(candidate[key_candidate]) if elem["name"] in to_remove_elements]
    else:
        to_remove_elements = mod_value["items"]
        if not isinstance(to_remove_elements, list):
            to_remove_elements = [to_remove_elements]
        indices = [i for i, elem in enumerate(candidate[key_candidate]) if elem in to_remove_elements]

    indices.reverse()
    for index in indices:
        del candidate[key_candidate][index]


def replace_spell(replace_list: List[Dict[str, Any]], candidate: List[str]):
    for replace in replace_list:
        for i in range(len(candidate)):
            if candidate[i] == replace["replace"]:
                candidate[i] = replace["with"]
                break


def add_spell(mod_value: Dict[str, Any], candidate: Dict[str, Any], replace: bool = False):
    """Add spells to the candidate"""

    for key, value in mod_value.items():
        if key == "mode":
            continue
        elif key == "spells":  # Regular spell slots
            for level, spell_list in value.items():
                applied = False
                for spell_casting in candidate["spellcasting"]:
                    if "spells" in spell_casting:
                        applied = True
                        if replace:
                            replace_spell(spell_list, spell_casting["spells"][level]["spells"])
                        else:
                            spell_casting["spells"].setdefault(level, [])
                            append_array({"items": spell_list["spells"]}, spell_casting["spells"][level], "spells")
                if not applied:
                    raise ValueError(f"Cannot apply spell level {key} in {mod_value}")

        elif key == "will":
            applied = False
            for spell_casting in candidate["spellcasting"]:
                if "will" in spell_casting or "daily" in spell_casting:
                    applied = True
                    if replace:
                        replace_spell(value, spell_casting["will"])
                        print(mod_value)
                        print(candidate)
                        raise ValueError("untested")
                    else:
                        spell_casting.setdefault("will", [])
                        append_array({"items": value}, spell_casting, "will")
            if not applied:
                raise ValueError(f"Cannot apply spell level {key} in {mod_value}")

        elif key == "daily":
            applied = False
            for daily_level, spell_list in value.items():
                for spell_casting in candidate["spellcasting"]:
                    if "will" in spell_casting or "daily" in spell_casting:
                        applied = True
                        if replace:
                            replace_spell(spell_list, spell_casting["daily"][daily_level])
                        else:
                            spell_casting["daily"].setdefault(daily_level, [])
                            append_array({"items": spell_list}, spell_casting["daily"], daily_level)
            if not applied:
                raise ValueError(f"Cannot apply spell level {key} in {mod_value}")
        else:
            raise ValueError(f"Invalid spell level {key} in {mod_value}")


def modifier(candidate: Dict[str, Any], characteristic: str, proficiency: bool = False) -> int:
    cr = candidate.get("cr", "0")
    if isinstance(cr, dict):
        cr = cr["cr"]
    proficiency_bonus = Creature.proficiency_bonus_from_challenge(cr) if proficiency else 0
    return (candidate[characteristic] - 10) // 2 + proficiency_bonus


def spell_dc(candidate: Dict[str, Any], characteristic: str) -> int:
    return 8 + modifier(candidate, characteristic, True)


def average_damage(candidate: Dict[str, Any], characteristic: str, means: float) -> int:
    return int(means + modifier(candidate, characteristic, False))


def add_skills(skills: List[Dict[str, int]], candidate: Dict[str, Any]):
    for skill_desc in skills:
        for skill, factor_bonus in skill_desc.items():
            cr = candidate["cr"]
            if isinstance(cr, dict):
                cr = cr["cr"]
            proficiency_bonus = Creature.proficiency_bonus_from_challenge(cr)
            characteristic = modifier(candidate, Creature.abilities_mapping[skill])
            skill_bonus = characteristic + factor_bonus * proficiency_bonus

            current_value = int(candidate.setdefault("skill", {}).get(skill, -30))
            if current_value < skill_bonus:  # Do not downgrade creatures
                candidate.setdefault("skill", {})[skill] = f"{skill_bonus:+d}"


def cap_size(max_size: str, candidate: Dict[str, Any]):
    sizes = ["T", "S", "M", "L", "H", "G"]
    assert candidate["size"][-1] in sizes and max_size in sizes, f"Invalid sizes {candidate['size']} or {max_size}"
    if sizes.index(candidate["size"][-1]) > sizes.index(max_size):
        candidate["size"] = max_size


def multiply_xp(initial_cr: str, factor: float, floor: bool) -> str:
    new_xp = Creature.xp_from_challenge(initial_cr) * factor
    new_xp = math.floor(new_xp) if floor else math.ceil(new_xp)
    for xp in sorted(list(Creature.challenge_rating_xp.keys())):
        if new_xp <= xp:  # We take the closest CR that gives at least this amount of XP
            return Creature.challenge_rating_xp[xp]
    return "30"


def change_cr(factor: float, floor: bool, candidate: Dict[str, Any]):
    if isinstance(candidate["cr"], str):
        candidate["cr"] = multiply_xp(candidate["cr"], factor, floor)
    else:
        for k in candidate["cr"].keys():
            candidate["cr"][k] = multiply_xp(candidate["cr"][k], factor, floor)


def add_to_skills(key: str, term: int, candidate: Dict[str, Any]):
    for save in candidate.get(key, {}).keys():
        new_val = int(candidate[key][save]) + term
        candidate[key][save] = f"{'+' if new_val >= 0 else '-'}{new_val}"


def mult_prop(key: str, prop: str, factor: float, floor: bool, candidate: Dict[str, Any]):
    assert key in candidate and (prop in candidate[key] or prop == "*"), \
        f"Cannot find candidate[{key}][{prop}] to multiply in {candidate['name']}"

    props = candidate[key].keys() if prop == "*" else [prop]

    for prop in props:
        old_val = candidate[key][prop]
        candidate[key][prop] = math.floor(old_val * factor) if floor else math.ceil(old_val * factor)
        if key == "hp":
            candidate[key]["formula"] += " (reduced HP)"


def add_prop(key: str, prop: str, term: int, candidate: Dict[str, Any]):
    if key not in candidate:
        return

    assert prop in candidate[key] or prop == "*", f"Cannot find candidate[{key}][{prop}] to add in {candidate['name']}"

    props = candidate[key].keys() if prop == "*" else [prop]

    for prop in props:
        sign_prefixed = isinstance(candidate[key][prop], str) and candidate[key][prop][0] in "+-"
        candidate[key][prop] = int(candidate[key][prop]) + term
        if sign_prefixed:
            candidate[key][prop] = f"{'+' if candidate[key][prop] >= 0 else '-'}{candidate[key][prop]}"


def add_on_match(search_and_replace: Callable, candidate: Union[str, bool, int, List, Dict]) \
        -> Union[str, bool, int, List, Dict]:
    if isinstance(candidate, bool) or isinstance(candidate, int):
        return candidate
    elif isinstance(candidate, list):
        return [add_on_match(search_and_replace, x) for x in candidate]
    elif isinstance(candidate, dict):
        return {k: add_on_match(search_and_replace, v) for k, v in candidate.items()}
    elif isinstance(candidate, str):
        return search_and_replace(candidate)
    raise ValueError(f"Cannot parse type {type(candidate)} of {candidate}")


def add_hit(key: str, term: int, candidate: Dict[str, Any]):
    def update_hit(match: re.Match):
        return f"{{@hit {int(match.group(1)) + term}}}"

    if key not in candidate:
        return
    candidate[key] = add_on_match(lambda x: hit_regex.sub(update_hit, x), candidate[key])


def add_dc(key: str, term: int, candidate: Dict[str, Any]):
    def update_dc(match: re.Match):
        return f"{{@dc {int(match.group(1)) + term}}}"

    if key not in candidate:
        return
    candidate[key] = add_on_match(lambda x: dc_regex.sub(update_dc, x), candidate[key])


def merge_copy_with_source(new: Dict[str, Any], source: Dict[str, Any], traits: List[Dict[str, Any]]):
    """Create a new 5e.tools dictionary for the monster based on its source"""
    operations = copy.deepcopy(new["_copy"])
    del operations["name"]
    del operations["source"]
    source["hasFluff"] = False
    source_image_path = source.get("image_path")

    candidate = copy.deepcopy(source)
    candidate.update(new)
    del candidate["_copy"]

    # Handle "_trait" first because additional operations may have to be run
    creature_trait = operations.get("_trait", [])
    if not isinstance(creature_trait, list):
        creature_trait = [creature_trait]
    for to_apply_trait in creature_trait:
        for existing_trait in traits:
            if existing_trait["name"] == to_apply_trait["name"] and \
                    existing_trait["source"] == to_apply_trait["source"]:
                existing_trait = copy.deepcopy(existing_trait)

                # Directly replaceable values
                candidate.update(existing_trait["apply"].get("_root", {}))

                # Add modifications to apply to the existing list
                for modification, mod_values in existing_trait["apply"].get("_mod", {}).items():
                    if not isinstance(mod_values, list):
                        mod_values = [mod_values]

                    if modification not in operations.setdefault("_mod", {}):
                        operations["_mod"][modification] = mod_values
                    elif isinstance(operations["_mod"][modification], list):
                        operations["_mod"][modification].extend(mod_values)
                    else:
                        operations["_mod"][modification] = [operations["_mod"][modification]] + mod_values
                break
    if "_trait" in operations:
        del operations["_trait"]

        # Replace placeholders introduced by generic traits
        if not candidate.get("isNpc", False):
            prefix = "The "
            short_name = candidate["name"]
        else:
            prefix = ""
            short_name = candidate["name"].split(" ")[0]

        replace_text({"replace": "<$title_short_name$>", "with": prefix + short_name}, operations)
        replace_text({"replace": "<$short_name$>", "with": short_name}, operations)
        replace_text({"replace": "<$spell_dc__int$>", "with": str(spell_dc(candidate, "int"))}, operations)
        replace_text({"replace": "<$spell_dc__wis$>", "with": str(spell_dc(candidate, "wis"))}, operations)
        replace_text({"replace": "<$spell_dc__cha$>", "with": str(spell_dc(candidate, "cha"))}, operations)
        replace_text({"replace": "<$damage_mod__str$>", "with": str(modifier(candidate, "str"))}, operations)
        replace_text({"replace": "<$damage_avg__2.5+str$>",
                      "with": str(average_damage(candidate, "str", 2.5))}, operations)
        replace_text({"replace": "<$to_hit__str$>",
                      "with": str(modifier(candidate, "str", proficiency=True))}, operations)
        match = generic_replace_trait_regex.search(json.dumps(operations))
        if match:
            raise ValueError(f"Cannot parse {match.group(0)} of {operations}")

    # Run all the operations
    for operation, op_value in operations.items():
        if operation == "_mod":
            for modification, mod_values in op_value.items():
                if not isinstance(mod_values, list):
                    mod_values = [mod_values]

                for mod_value in mod_values:
                    if modification == "*":
                        if mod_value["mode"] == "replaceTxt":
                            replace_text(mod_value, candidate, ignore_name=True)
                        else:
                            raise ValueError(f"{new['name']}: Cannot parse the mode of {modification} with {mod_value}")
                    elif isinstance(modification, str) and mod_value == "remove":
                        del candidate[modification]
                    elif isinstance(modification, str):
                        if mod_value["mode"] == "replaceTxt":
                            replace_text(mod_value, candidate, key_candidate=modification, ignore_name=True)
                        elif mod_value["mode"] == "replaceArr":
                            replace_array(mod_value, candidate, key_candidate=modification)
                        elif mod_value["mode"] == "appendArr":
                            append_array(mod_value, candidate, key_candidate=modification)
                        elif mod_value["mode"] == "removeArr":
                            remove_array(mod_value, candidate, key_candidate=modification)
                        elif mod_value["mode"] == "prependArr":
                            insert_array(mod_value, candidate, key_candidate=modification)
                        elif mod_value["mode"] == "insertArr":
                            insert_array(mod_value, candidate, key_candidate=modification,
                                         insertion_index=mod_value["index"])
                        elif mod_value["mode"] == "appendIfNotExistsArr":
                            append_array(mod_value, candidate, key_candidate=modification, check_exist=True)
                        elif mod_value["mode"] == "addSpells":
                            add_spell(mod_value, candidate)
                        elif mod_value["mode"] == "replaceSpells":
                            add_spell(mod_value, candidate, replace=True)
                        elif mod_value["mode"] == "addSenses":
                            senses = mod_value["senses"]
                            if not isinstance(senses, list):
                                senses = [senses]
                            append_array({"items": [f"{s['type']} {s['range']} ft" for s in senses]}, candidate,
                                         key_candidate="senses", check_exist=True)
                        elif mod_value["mode"] == "addSkills":
                            skills = mod_value["skills"]
                            if not isinstance(skills, list):
                                skills = [skills]
                            add_skills(skills, candidate)
                        elif mod_value["mode"] == "maxSize":
                            cap_size(mod_value["max"], candidate)
                        elif mod_value["mode"] == "scalarMultXp":
                            change_cr(mod_value["scalar"], mod_value["floor"], candidate)
                        elif mod_value["mode"] == "addAllSaves":
                            add_to_skills("save", mod_value["saves"], candidate)
                        elif mod_value["mode"] == "addAllSkills":
                            add_to_skills("skill", mod_value["skills"], candidate)
                        elif mod_value["mode"] == "scalarMultProp":
                            mult_prop(modification, mod_value["prop"], mod_value["scalar"], mod_value["floor"],
                                      candidate)
                        elif mod_value["mode"] == "scalarAddProp":
                            add_prop(modification, mod_value["prop"], mod_value["scalar"], candidate)
                        elif mod_value["mode"] == "scalarAddHit":
                            add_hit(modification, mod_value["scalar"], candidate)
                        elif mod_value["mode"] == "scalarAddDc":
                            add_dc(modification, mod_value["scalar"], candidate)
                        else:
                            raise ValueError(f"{new['name']}: Cannot parse the mode of {modification} with {mod_value}")
                    else:
                        raise ValueError(f"{new['name']}: Cannot parse the mode of {modification} with {mod_value}")
        elif operation == "_preserve":
            # Ignore because {'legendaryGroup': True}, {'variant': True} and images are made by design
            if op_value != {'legendaryGroup': True} and op_value != {'variant': True} \
                    and op_value != {'page': True, 'hasFluff': True, 'hasFluffImages': True}:
                raise ValueError(f"{new['name']}: Cannot parse the operation of {operation} with {op_value}")
        else:
            raise ValueError(f"{new['name']}: Cannot parse the operation of {operation} with {op_value}")

    candidate["image_path"] = source_image_path  # so it is not modified during the copy

    return candidate
