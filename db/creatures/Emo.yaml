name: Emo
alias: Emo
image_key: null
image_path: null
appearance: null
player_image_path: null
reference_image_path: bestiary/MM/Beholder.jpg
source:
- Emo
- NRH-AT
note: ''
ac: 18
armor_desc: from natural armor
hpmax: 266
hpmax_roll: 19d10 + 76
hp: 266
xp: 10000
other_xp:
  lair: 11500
size: L
type: aberration
alignment: lawful evil
speed: 0 m., fly 6 m. (hover), canHover 0.5 m.
strength: 10
dexterity: 14
constitution: 18
intelligence: 17
wisdom: 15
charisma: 17
saving_throws:
- Int +8
- Wis +7
- Cha +8
abilities:
- Perception +12
damage_weaknesses: []
damage_resistances: []
damage_immunities: []
condition_immunities:
- prone
senses:
- darkvision 36 m.
- Passive perception 22
languages:
- Deep Speech
- Undercommon
traits:
- name: Antimagic Cone
  description: Emo's central eye creates an area of antimagic, as in the {@spell antimagic
    field} spell, in a 45-meter cone. At the start of each of its turns, Emo decides
    which way the cone faces and whether the cone is active. The area works against
    Emo's own eye rays.
  limit: 0
  expanded: 0
spells: []
actions:
- name: Bite
  description: '{@atk mw} {@hit 5} to hit, reach 1.5 m., one target. {@h}14 ({@damage
    4d6}) piercing damage.'
  limit: 0
  expanded: 0
- name: Eye Rays
  description: Emo shoots three of the following magical eye rays at random (reroll
    duplicates), choosing one to three targets it can see within 36 meters of it:<br/><ul><li><b>1.
    Happiness Ray</b> The targeted creature must succeed on a {@dc 14} Wisdom saving
    throw or fall {@condition prone} with laughter, becoming {@condition incapacitated}
    and unable to stand up for 1 minute or until Emo harms the creature. The target
    can repeat the saving throw at the end of each of its turns, ending the effect
    on itself on a success.</li><li><b>2. Sadness Ray</b> The targeted creature must
    succeed on a {@dc 15} Intelligence saving throw. On a failed save, the target
    is overcome by sorrow and takes {@damage 3d6} psychic damage, and it can't take
    a reaction until the end of its next turn. On its next turn, it can use either
    an action or a bonus action, not both. On a successful save, the target takes
    half as much damage and suffers none of the other effects.</li><li><b>3. Anger
    Ray</b> The targeted creature must succeed on a {@dc 14} Wisdom saving throw or
    be compelled to duel with Emo for 1 minute. The target has disadvantage on attack
    rolls against creatures other than Emo and must make a Wisdom saving throw each
    time it attempts to move more than 9 meters away from Emo.</li><li><b>4. Anxiety
    Ray</b> The targeted creature must succeed on a {@dc 15} Intelligence saving throw
    or lose the ability to distinguish friend from foe, regarding all creatures it
    can see as enemies for 1 minute. Each time the target takes damage, it can repeat
    the saving throw, ending the effect on itself on a success.<br/>Whenever the affected
    creature targets another creature with an attack, spell, or other ability, it
    must choose the target at random from among the creatures it can see within range
    of the attack, spell, or other ability it's using. If an enemy provokes an opportunity
    attack from the affected creature, the creature must make that attack if it is
    able to.</li><li><b>5. Fear Ray</b> The targeted creature must succeed on a {@dc
    14} Wisdom saving throw or be {@condition frightened} for 1 minute.<br/>The target
    can repeat the saving throw at the end of each of its turns, ending the effect
    on itself on a success.</li><li><b>6. Desolation Ray</b> The targeted creature
    must succeed on a {@dc 14} Intelligence saving throw. On a successful save, the
    target takes {@damage 5d10} psychic damage, and the spell ends. On a failed save,
    the target takes {@damage 5d10} psychic damage and perceives itself to be confined
    within an illusory stone cell for 1 minute. The target can't see or hear anything
    beyond the cell and is {@condition restrained}.</li><li><b>7. Jealousy Ray</b>
    The targeted creature must succeed on a {@dc 14} Wisdom saving throw or become
    {@condition charmed} for 1 minute. While the target is {@condition charmed} in
    this way, it must use its action before moving on each of its turns to make a
    melee attack against a creature that Emo chooses. The target can repeat the saving
    throw at the end of each of its turns, ending the effect on itself on a success.</li><li><b>8.
    Disgusting Ray</b> The targeted creature must succeed on a {@dc 15} Constitution
    saving throw against poison. On a failed save, the target is enveloped in sphere
    of yellow, nauseating gas and spends its action on its next turn retching and
    reeling.</li><li><b>9. Surprise Ray</b> The targeted creature must succeed on
    a<br/>{@dc 15} Dexterity saving throw or be {@condition blinded} by a spray of
    color until the end of its next turn.</li><li><b>10. Trust Ray</b> The targeted
    creature must succeed on a<br/>{@dc 16} Wisdom saving throw or be {@condition
    charmed} by Emo for 1 hour or until Emo harms the creature.</li></ul>
  limit: 0
  expanded: 0
reactions: []
variant:
- name: Variant Abilities
  description: When a beholder's dream-imagination runs wild, the result can be an
    offspring that has an unusual or unique set of abilities. Rather than the standard
    powers of a beholder's central eye and eyestalks, the creature has one or more
    variant abilities—guaranteed to surprise any enemies who thought they knew what
    they were getting themselves into.<br/>This section provides several alternative
    spell effects for a beholder's eye. Each of these effects is designed to be of
    the same power level as the one it replaces, enabling you to create a custom beholder
    without altering the monster's challenge rating. As another option, you can switch
    any of the damaging eye rays in the {@book Monster Manual|MM} to an effect with
    a different damage type, such as replacing the enervation ray with a combustion
    ray that deals fire damage instead of necrotic damage.<br/>Unless otherwise indicated,
    an alternative ability has the same range as the eye ray it is replacing, and
    it affects only one creature per use (even if the ability is based on a spell
    that normally affects an area or multiple targets). The saving throw for an alternative
    ability uses the same DC and the same ability score as the spell the eye ray is
    based on.<br/><ul><li><b>Antimagic Cone:</b> {@spell mirage arcane}, {@spell power
    word stun} (affecting the weakest non-{@condition stunned} target in the cone
    each round)</li><li><b>Charm Ray:</b> {@spell banishment} (1 minute), {@spell
    confusion} (1 minute)</li><li><b>Death Ray:</b> {@spell circle of death} (3-meter-radius
    sphere; 4d6 necrotic damage to all creatures in the area), {@spell feeblemind}</li><li><b>Disintegration
    Ray:</b> {@spell chain lightning} (primary target takes 6d8 lightning damage;
    two secondary targets within 9 meters of the primary target take 3d8 lightning
    damage each), {@spell eyebite} (sickened effect; 1 minute)</li><li><b>Enervation
    Ray:</b> {@spell create undead} (usable regardless of the time of day), {@spell
    polymorph} (1 minute)</li><li><b>Fear Ray:</b> {@spell gaseous form} (self or
    willing creature only), {@spell moonbeam}</li><li><b>Paralyzing Ray:</b> {@spell
    modify memory}, {@spell silence} (1 minute)</li><li><b>Petrification Ray:</b>
    {@spell Otto's irresistible dance} (1 minute), {@spell wall of ice} (1 minute;
    one 3-meter-square panel)</li><li><b>Sleep Ray:</b> {@spell blindness/deafness},
    {@spell misty step} (self or willing creature only)</li><li><b>Slowing Ray:</b>
    {@spell bestow curse} (1 minute), {@spell sleet storm} (one 3-meter-cube)</li><li><b>Telekinesis
    Ray:</b> {@spell geas} (1 hour), {@spell wall of force} (1 minute; one 3-meter-square
    panel)</li></ul>
  limit: 0
  expanded: 0
legendary_actions_desc: Emo can take 3 legendary actions, using the Eye Ray option
  below. It can take only one legendary action at a time and only at the end of another
  creature's turn. Emo regains spent legendary actions at the start of its turn.
legendary_actions_limit: 3
legendary_actions_expanded: 0
legendary_actions:
- name: Eye Ray
  description: Emo uses one random eye ray.
  limit: 0
  expanded: 0
mythic_actions_desc: null
mythic_actions: []
lair_actions_desc: null
lair_actions: []
regional_effects: []
has_spellbook: false
known_spells: []
initiative: 0
conditions: []
visible: false
active: false
