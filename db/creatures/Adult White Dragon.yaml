name: Adult White Dragon
alias: Adult White Dragon
image_key: null
image_path: null
appearance: null
player_image_path: null
reference_image_path: bestiary/MM/White Dragon.jpg
source:
- Adult White Dragon
- MM
note: ''
ac: 18
armor_desc: from natural armor
hpmax: 288
hpmax_roll: 16d12 + 96
hp: 288
xp: 10000
other_xp: {}
size: H
type: dragon
alignment: chaotic evil
speed: 12 m., burrow 9 m., fly 24 m., swim 12 m.
strength: 22
dexterity: 10
constitution: 22
intelligence: 8
wisdom: 12
charisma: 12
saving_throws:
- Dex +5
- Con +11
- Wis +6
- Cha +6
abilities:
- Perception +11
- Stealth +5
damage_weaknesses: []
damage_resistances: []
damage_immunities:
- cold
condition_immunities: []
senses:
- blindsight 18 m.
- darkvision 36 m.
- Passive perception 21
languages:
- Common
- Draconic
traits:
- name: Ice Walk
  description: The dragon can move across and climb icy surfaces without needing to
    make an ability check. Additionally, difficult terrain composed of ice or snow
    doesn't cost it extra movement.
  limit: 0
  expanded: 0
- name: Legendary Resistance (3/Day)
  description: If the dragon fails a saving throw, it can choose to succeed instead.
  limit: 3
  expanded: 0
spells: []
actions:
- name: Multiattack
  description: 'The dragon can use its Frightful Presence. It then makes three attacks:
    one with its bite and two with its claws.'
  limit: 0
  expanded: 0
- name: Bite
  description: '{@atk mw} {@hit 11} to hit, reach 3 m., one target. {@h}17 ({@damage
    2d10 + 6}) piercing damage plus 4 ({@damage 1d8}) cold damage.'
  limit: 0
  expanded: 0
- name: Claw
  description: '{@atk mw} {@hit 11} to hit, reach 1.5 m., one target. {@h}13 ({@damage
    2d6 + 6}) slashing damage.'
  limit: 0
  expanded: 0
- name: Tail
  description: '{@atk mw} {@hit 11} to hit, reach 4.5 m., one target. {@h}15 ({@damage
    2d8 + 6}) bludgeoning damage.'
  limit: 0
  expanded: 0
- name: Frightful Presence
  description: Each creature of the dragon's choice that is within 36 meters of the
    dragon and aware of it must succeed on a {@dc 14} Wisdom saving throw or become
    {@condition frightened} for 1 minute. A creature can repeat the saving throw at
    the end of each of its turns, ending the effect on itself on a success. If a creature's
    saving throw is successful or the effect ends for it, the creature is immune to
    the dragon's Frightful Presence for the next 24 hours.
  limit: 0
  expanded: 0
- name: Cold Breath ({@recharge 5-6})
  description: The dragon exhales an icy blast in a 18-meter cone. Each creature in
    that area must make a {@dc 19} Constitution saving throw, taking 54 ({@damage
    12d8}) cold damage on a failed save, or half as much damage on a successful one.
  limit: 1
  expanded: 0
reactions: []
variant:
- name: Customizing Dragons
  description: 'You can customize any dragon''s stat block to reflect the dragon''s
    unique character. Minor changes such as those below are easy to make and have
    no impact on a dragon''s challenge rating.<br/><b>Languages</b> Most dragons prefer
    to speak Draconic but learn Common for dealing with allies and minions. But given
    their high Intelligence and long life span, dragons can easily learn additional
    languages. You can add languages to a dragon''s stat block.<br/><b>Skills</b>
    Most dragons are proficient in the {@skill Perception} and {@skill Stealth} skills,
    and many dragons have additional skill proficiencies. As with languages, you can
    customize a dragon''s skill list (even doubling their proficiency bonus with certain
    skills) to reflect particular interests and activities. You can also give a dragon
    tool proficiencies, particularly if the dragon spends time in Humanoid form.<br/><b>Spells</b>
    {@note See the ''Variant: Dragons as Innate Spellcasters'' inset(s), below.}<br/><b>Other
    Traits and Actions</b> You can borrow traits and actions from other monsters to
    add unique flavor to a dragon. Consider these examples:<br/><ul><li><b>Flyby</b>
    The dragon is an agile flier, quick to fly out of enemies'' reach.<br/>The dragon
    doesn''t provoke an opportunity attack when it flies out of an enemy''s reach.</li><li><b>Mimicry</b>
    Impersonating characters or their allies could be a fun trick for a crafty dragon.<br/>The
    dragon can mimic any sounds it has heard, including voices. A creature that hears
    the sounds can tell they are imitations with a successful {@dc 9} Wisdom ({@skill
    Insight}) check.</li><li><b>Rejuvenation</b> You might decide that dragons in
    your campaign, being an essential part of the Material Plane, are nearly impossible
    to destroy. A dragon''s life essence might be preserved in the egg from which
    it first emerged, in its hoard, or in a cavernous hall at the center of the world,
    just as a lich''s essence is hidden in a phylactery.<br/>If it has an essence-preserving
    object, a destroyed dragon gains a new body in {@dice 1d10} days, regaining all
    its hit points and becoming active again. The new body appears within 1.5 meters
    of the object.</li><li><b>Special Senses</b> Most dragons have {@sense blindsight}
    and {@sense darkvision}. You might upgrade {@sense blindsight} to {@sense truesight},
    or you could give a dragon with a burrowing speed {@sense tremorsense}.</li><li><b>Tunneler</b>
    The dragon can burrow through solid rock at half its burrowing speed and leaves
    a tunnel in its wake.</li></ul>'
  limit: 0
  expanded: 0
legendary_actions_desc: The Adult White Dragon can take 3 legendary actions, choosing
  from the options below. Only one legendary action can be used at a time and only
  at the end of another creature's turn. The Adult White Dragon regains spent legendary
  actions at the start of its turn.
legendary_actions_limit: 3
legendary_actions_expanded: 0
legendary_actions:
- name: Detect
  description: The dragon makes a Wisdom ({@skill Perception}) check.
  limit: 0
  expanded: 0
- name: Tail Attack
  description: The dragon makes a tail attack.
  limit: 0
  expanded: 0
- name: Wing Attack (Costs 2 Actions)
  description: The dragon beats its wings. Each creature within 3 meters of the dragon
    must succeed on a {@dc 19} Dexterity saving throw or take 13 ({@damage 2d6 + 6})
    bludgeoning damage and be knocked {@condition prone}. The dragon can then fly
    up to half its flying speed.
  limit: 0
  expanded: 0
mythic_actions_desc: null
mythic_actions: []
lair_actions_desc: 'On initiative count 20 (losing initiative ties), the dragon takes
  a lair action to cause one of the following effects; the dragon can''t use the same
  effect two rounds in a row:'
lair_actions:
- name: ''
  description: <li>Freezing fog fills a 6-meter-radius sphere centered on a point
    the dragon can see within 36 meters of it. The fog spreads around corners, and
    its area is heavily obscured. Each creature in the fog when it appears must make
    a {@dc 10} Constitution saving throw, taking 10 ({@damage 3d6}) cold damage on
    a failed save, or half as much damage on a successful one. A creature that ends
    its turn in the fog takes 10 ({@damage 3d6}) cold damage. A wind of at least 32
    kilometers per hour disperses the fog. The fog otherwise lasts until the dragon
    uses this lair action again or until the dragon dies.</li>
  limit: 0
  expanded: 0
- name: ''
  description: <li>Jagged ice shards fall from the ceiling, striking up to three creatures
    underneath that the dragon can see within 36 meters of it. The dragon makes one
    ranged attack roll ({@hit 7} to hit) against each target. On a hit, the target
    takes 10 ({@damage 3d6}) piercing damage.</li>
  limit: 0
  expanded: 0
- name: ''
  description: <li>The dragon creates an opaque wall of ice on a solid surface it
    can see within 36 meters of it. The wall can be up to 9 meters long, 9 meters
    high, and 1 foot thick. When the wall appears, each creature within its area is
    pushed 1.5 meters out of the wall's space; appearing on whichever side of the
    wall it wants. Each 3-meter section of the wall has AC 5, 30 hit points, vulnerability
    to fire damage, and immunity to acid, cold, necrotic, poison, and psychic damage.
    The wall disappears when the dragon uses this lair action again or when the dragon
    dies.</li>
  limit: 0
  expanded: 0
- name: Additional Lair Actions
  description: 'At your discretion, a legendary ({@creature Adult White Dragon||adult}
    or {@creature Ancient White Dragon||ancient}) white dragon can use one or both
    of the following additional lair actions while in its lair:'
  limit: 0
  expanded: 0
- name: Blinding Sleet
  description: Driving sleet falls in a 12-meter-high, 6-meter-radius cylinder centered
    on a point the dragon can see within 36 meters of it. Each creature in that area
    must succeed on a DC 15 Constitution saving throw or be {@condition blinded} until
    initiative count 20 on the next round.
  limit: 0
  expanded: 0
- name: Whirling Wind
  description: A strong wind blows in a 9-meter-radius sphere centered on the dragon
    (see the {@book Dungeon Master's Guide|DMG} for rules on {@book strong wind|DMG|5|Strong
    Wind}). The dragon's flying is not affected by this wind, which lasts until the
    next time the dragon uses a lair action or until the dragon dies.
  limit: 0
  expanded: 0
regional_effects:
- name: ''
  description: <li>Chilly fog lightly obscures the land within 9.5 kilometers of the
    dragon's lair.</li>
  limit: 0
  expanded: 0
- name: ''
  description: <li>Freezing precipitation falls within 9.5 kilometers of the dragon's
    lair, sometimes forming blizzard conditions when the dragon is at rest.</li>
  limit: 0
  expanded: 0
- name: ''
  description: <li>Icy walls block off areas in the dragon's lair. Each wall is 6
    inches thick, and a 3-meter section has AC 5, 15 hit points, vulnerability to
    fire damage, and immunity to acid, cold, necrotic, poison, and psychic damage.</li>
  limit: 0
  expanded: 0
- name: ''
  description: <li>If the dragon wishes to move through a wall, it can do so without
    slowing down. The portion of the wall the dragon moves through is destroyed, however.</li>
  limit: 0
  expanded: 0
- name: ''
  description: If the dragon dies, the fog and precipitation fade within 1 day. The
    ice walls melt over the course of {@dice 1d10} days.
  limit: 0
  expanded: 0
- name: Additional Regional Effects
  description: 'Any of these effects might appear in the area around a white dragon''s
    lair, in addition to or instead of the effects described in the {@i Monster Manual}:'
  limit: 0
  expanded: 0
- name: Biting Chill
  description: Extreme cold envelops the land within 9.5 kilometers of the dragon's
    lair (see the {@book Dungeon Master's Guide|DMG} for rules on {@book extreme cold|DMG|5|Extreme
    Cold}). If the climate in the area already features extreme cold, the cold is
    numbing, giving creatures in the area without immunity or resistance to cold damage
    disadvantage on Strength and Dexterity checks.
  limit: 0
  expanded: 0
- name: Mirror Ice
  description: The icy surfaces in the dragon's lair reflect light like mirrors, giving
    creatures in the lair other than the dragon disadvantage on Dexterity ({@skill
    Stealth}) checks made to hide. In addition, at each intersection or branching
    passage, any creature other than the dragon has a {@chance 50|50 percent} chance
    of going in a different direction from the direction it intends.
  limit: 0
  expanded: 0
has_spellbook: false
known_spells: []
initiative: 0
conditions: []
visible: false
active: false
