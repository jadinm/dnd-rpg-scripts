name: Sapphire Greatwyrm
alias: Sapphire Greatwyrm
image_key: null
image_path: null
appearance: null
player_image_path: null
reference_image_path: bestiary/FTD/Sapphire Greatwyrm.webp
source:
- Sapphire Greatwyrm
- FTD
note: ''
ac: 21
armor_desc: from natural armor
hpmax: 754
hpmax_roll: 26d20 + 234
hp: 754
xp: 90000
other_xp: {}
size: G
type: dragon(gem)
alignment: neutral
speed: 18 m., burrow 18 m., fly 36 m. (hover), swim 18 m., canHover 0.5 m.
strength: 28
dexterity: 14
constitution: 29
intelligence: 30
wisdom: 24
charisma: 25
saving_throws:
- Dex +10
- Con +17
- Wis +15
- Cha +15
abilities:
- Arcana +26
- History +18
- Perception +15
damage_weaknesses: []
damage_resistances: []
damage_immunities:
- thunder
condition_immunities:
- charmed
- frightened
- poisoned
- prone
senses:
- truesight 36 m.
- Passive perception 25
languages:
- Common
- Draconic
traits:
- name: Gem Awakening (Recharges after a Short or Long Rest)
  description: If the greatwyrm would be reduced to 0 hit points, its current hit
    point total instead resets to 400 hit points, it recharges its Breath Weapon,
    and it regains any expended uses of Legendary Resistance. Additionally, the greatwyrm
    can now use its Mass Telekinesis action during the next hour. Award a party an
    additional 90,000 XP (180,000 XP total) for defeating the greatwyrm after its
    Gem Awakening activates.
  limit: 1
  expanded: 0
- name: Legendary Resistance (4/Day)
  description: If the greatwyrm fails a saving throw, it can choose to succeed instead.
  limit: 4
  expanded: 0
- name: Unusual Nature
  description: The greatwyrm doesn't require food or drink.
  limit: 0
  expanded: 0
- name: Spellcasting (Psionics)
  description: 'The greatwyrm casts one of the following spells, requiring no spell
    components and using Intelligence as the spellcasting ability (spell save {@dc
    26}, {@hit 18} to hit with spell attack):'
  limit: 0
  expanded: 0
spells:
- name: 1/day each
  spells:
  - '{@spell dispel magic}'
  - '{@spell forcecage}'
  - '{@spell plane shift}'
  - '{@spell reverse gravity}'
  - '{@spell time stop}'
  limit: 5
  expanded: 0
actions:
- name: Multiattack
  description: The greatwyrm makes one Bite attack and two Claw attacks.
  limit: 0
  expanded: 0
- name: Bite
  description: '{@atk mw} {@hit 17} to hit, reach 4.5 m., one target. {@h}20 ({@damage
    2d10 + 9}) piercing damage plus 16 ({@damage 3d10}) force damage.'
  limit: 0
  expanded: 0
- name: Claw
  description: '{@atk mw} {@hit 17} to hit, reach 3 m., one target. {@h}18 ({@damage
    2d8 + 9}) slashing damage. If the target is a Huge or smaller creature, it is
    {@condition grappled} (escape {@dc 19}) and is {@condition restrained} until this
    grapple ends. The greatwyrm can have only one creature {@condition grappled} in
    this way at a time.'
  limit: 0
  expanded: 0
- name: Breath Weapon ({@recharge 5-6})
  description: The greatwyrm exhales crushing force in a 90-meter cone. Each creature
    in that area must make a {@dc 25} Dexterity saving throw. On a failed save, the
    creature takes 71 ({@damage 11d12}) force damage and is knocked {@condition prone}.
    On a successful save, it takes half as much damage and isn't knocked {@condition
    prone}. On a success or failure, the creature's speed becomes 0 until the end
    of its next turn.
  limit: 1
  expanded: 0
- name: Mass Telekinesis (Gem Awakening Only; Recharges after a Short or Long Rest)
  description: The greatwyrm targets any number of creatures and objects it can see
    within 36 meters of it. No one target can weigh more than 4,000 pounds, and objects
    can't be targeted if they're being worn or carried. Each targeted creature must
    succeed on a {@dc 26} Strength saving throw or be {@condition restrained} in the
    greatwyrm's telekinetic grip. At the end of a creature's turn, it can repeat the
    saving throw, ending the effect on itself on a success.<br/>At the end of the
    greatwyrm's turn, it can move each creature or object it has in its telekinetic
    grip up to 18 meters in any direction, but not beyond 36 meters of itself. In
    addition, it can choose any number of creatures {@condition restrained} in this
    way and deal 45 ({@damage 7d12}) force damage to each of them.
  limit: 1
  expanded: 0
reactions: []
variant:
- name: Customizing Dragons
  description: 'You can customize any dragon''s stat block to reflect the dragon''s
    unique character. Minor changes such as those below are easy to make and have
    no impact on a dragon''s challenge rating.<br/><b>Languages</b> Most dragons prefer
    to speak Draconic but learn Common for dealing with allies and minions. But given
    their high Intelligence and long life span, dragons can easily learn additional
    languages. You can add languages to a dragon''s stat block.<br/><b>Skills</b>
    Most dragons are proficient in the {@skill Perception} and {@skill Stealth} skills,
    and many dragons have additional skill proficiencies. As with languages, you can
    customize a dragon''s skill list (even doubling their proficiency bonus with certain
    skills) to reflect particular interests and activities. You can also give a dragon
    tool proficiencies, particularly if the dragon spends time in Humanoid form.<br/><b>Spells</b>
    {@note See the ''Variant: Dragons as Innate Spellcasters'' inset(s), below.}<br/><b>Other
    Traits and Actions</b> You can borrow traits and actions from other monsters to
    add unique flavor to a dragon. Consider these examples:<br/><ul><li><b>Change
    Shape</b> You can decide that a dragon acquires this action at a younger age than
    usual, particularly if you want to feature a dragon in Humanoid form in your campaign:<br/>The
    dragon magically polymorphs into a humanoid or beast that has a challenge rating
    no higher than its own, or back into its true form. It reverts to its true form
    if it dies. Any equipment it is wearing or carrying is absorbed or borne by the
    new form (the dragon''s choice).<br/>In a new form, the dragon retains its alignment,
    hit points, Hit Dice, ability to speak, proficiencies, Legendary Resistance, lair
    actions, and Intelligence, Wisdom, and Charisma scores, as well as this action.
    Its statistics and capabilities are otherwise replaced by those of the new form,
    except any class features or legendary actions of that form.</li><li><b>Flyby</b>
    The dragon is an agile flier, quick to fly out of enemies'' reach.<br/>The dragon
    doesn''t provoke an opportunity attack when it flies out of an enemy''s reach.</li><li><b>Mimicry</b>
    Impersonating characters or their allies could be a fun trick for a crafty dragon.<br/>The
    dragon can mimic any sounds it has heard, including voices. A creature that hears
    the sounds can tell they are imitations with a successful {@dc 15} Wisdom ({@skill
    Insight}) check.</li><li><b>Rejuvenation</b> You might decide that dragons in
    your campaign, being an essential part of the Material Plane, are nearly impossible
    to destroy. A dragon''s life essence might be preserved in the egg from which
    it first emerged, in its hoard, or in a cavernous hall at the center of the world,
    just as a lich''s essence is hidden in a phylactery.<br/>If it has an essence-preserving
    object, a destroyed dragon gains a new body in {@dice 1d10} days, regaining all
    its hit points and becoming active again. The new body appears within 1.5 meters
    of the object.</li><li><b>Special Senses</b> Most dragons have {@sense blindsight}
    and {@sense darkvision}. You might upgrade {@sense blindsight} to {@sense truesight},
    or you could give a dragon with a burrowing speed {@sense tremorsense}.</li><li><b>Tunneler</b>
    The dragon can burrow through solid rock at half its burrowing speed and leaves
    a tunnel in its wake.</li></ul>'
  limit: 0
  expanded: 0
legendary_actions_desc: The Sapphire Greatwyrm can take 3 legendary actions, choosing
  from the options below. Only one legendary action can be used at a time and only
  at the end of another creature's turn. The Sapphire Greatwyrm regains spent legendary
  actions at the start of its turn.
legendary_actions_limit: 3
legendary_actions_expanded: 0
legendary_actions:
- name: Claw
  description: The greatwyrm makes one Claw attack.
  limit: 0
  expanded: 0
- name: Psionics (Costs 2 Actions)
  description: The greatwyrm uses Psychic Step or Spellcasting.
  limit: 0
  expanded: 0
- name: Psychic Beam (Costs 3 Actions)
  description: The greatwyrm emits a beam of psychic energy in a 27-meter line that
    is 3 meters wide. Each creature in that area must make a {@dc 26} Intelligence
    saving throw, taking 27 ({@damage 5d10}) psychic damage on a failed save, or half
    as much damage on a successful one.
  limit: 0
  expanded: 0
mythic_actions_desc: null
mythic_actions: []
lair_actions_desc: null
lair_actions: []
regional_effects: []
has_spellbook: true
known_spells: []
initiative: 0
conditions: []
visible: false
active: false
