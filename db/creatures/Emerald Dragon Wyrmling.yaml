name: Emerald Dragon Wyrmling
alias: Emerald Dragon Wyrmling
image_key: null
image_path: null
appearance: null
player_image_path: null
reference_image_path: bestiary/FTD/Emerald Dragon Wyrmling.webp
source:
- Emerald Dragon Wyrmling
- FTD
note: ''
ac: 16
armor_desc: from natural armor
hpmax: 60
hpmax_roll: 6d8 + 12
hp: 60
xp: 450
other_xp: {}
size: M
type: dragon(gem)
alignment: lawful neutral
speed: 9 m., burrow 6 m., fly 18 m.
strength: 15
dexterity: 12
constitution: 15
intelligence: 14
wisdom: 12
charisma: 14
saving_throws:
- Dex +3
- Con +4
- Wis +3
- Cha +4
abilities:
- Arcana +4
- Deception +4
- Perception +5
- Stealth +3
damage_weaknesses: []
damage_resistances:
- fire
- psychic
damage_immunities: []
condition_immunities: []
senses:
- blindsight 3 m.
- darkvision 18 m.
- Passive perception 15
languages:
- Draconic
- telepathy 120 ft.
traits:
- name: Tunneler
  description: The dragon can burrow through solid rock at half its burrowing speed
    and can leave a 3-meter-diameter tunnel in its wake.
  limit: 0
  expanded: 0
- name: Spellcasting (Psionics)
  description: 'The dragon casts one of the following spells, requiring no spell components
    and using Intelligence as the spellcasting ability (spell save {@dc 12}):'
  limit: 0
  expanded: 0
spells:
- name: At will
  spells:
  - '{@spell mage hand} (the hand is invisible)'
  - '{@spell minor illusion}'
  limit: 0
  expanded: 0
actions:
- name: Bite
  description: '{@atk mw} {@hit 4} to hit, reach 3 m., one target. {@h}7 ({@damage
    1d10 + 2}) piercing damage plus 3 ({@damage 1d6}) psychic damage.'
  limit: 0
  expanded: 0
- name: Disorienting Breath ({@recharge 5-6})
  description: The dragon exhales a wave of psychic dissonance in a 4.5-meter cone.
    Each creature in that area must make a {@dc 12} Intelligence saving throw. On
    a failed save, the creature takes 17 ({@damage 5d6}) psychic damage, and until
    the end of its next turn, when the creature makes an attack roll or an ability
    check, it must roll a {@dice d4} and reduce the total by the number rolled. On
    a successful save, the creature takes half as much damage with no additional effects.
  limit: 1
  expanded: 0
reactions: []
variant:
- name: Customizing Dragons
  description: 'You can customize any dragon''s stat block to reflect the dragon''s
    unique character. Minor changes such as those below are easy to make and have
    no impact on a dragon''s challenge rating.<br/><b>Languages</b> Most dragons prefer
    to speak Draconic but learn Common for dealing with allies and minions. But given
    their high Intelligence and long life span, dragons can easily learn additional
    languages. You can add languages to a dragon''s stat block.<br/><b>Skills</b>
    Most dragons are proficient in the {@skill Perception} and {@skill Stealth} skills,
    and many dragons have additional skill proficiencies. As with languages, you can
    customize a dragon''s skill list (even doubling their proficiency bonus with certain
    skills) to reflect particular interests and activities. You can also give a dragon
    tool proficiencies, particularly if the dragon spends time in Humanoid form.<br/><b>Spells</b>
    {@note See the ''Variant: Dragons as Innate Spellcasters'' inset(s), below.}<br/><b>Other
    Traits and Actions</b> You can borrow traits and actions from other monsters to
    add unique flavor to a dragon. Consider these examples:<br/><ul><li><b>Change
    Shape</b> You can decide that a dragon acquires this action at a younger age than
    usual, particularly if you want to feature a dragon in Humanoid form in your campaign:<br/>The
    dragon magically polymorphs into a humanoid or beast that has a challenge rating
    no higher than its own, or back into its true form. It reverts to its true form
    if it dies. Any equipment it is wearing or carrying is absorbed or borne by the
    new form (the dragon''s choice).<br/>In a new form, the dragon retains its alignment,
    hit points, Hit Dice, ability to speak, proficiencies, Legendary Resistance, lair
    actions, and Intelligence, Wisdom, and Charisma scores, as well as this action.
    Its statistics and capabilities are otherwise replaced by those of the new form,
    except any class features or legendary actions of that form.</li><li><b>Flyby</b>
    The dragon is an agile flier, quick to fly out of enemies'' reach.<br/>The dragon
    doesn''t provoke an opportunity attack when it flies out of an enemy''s reach.</li><li><b>Mimicry</b>
    Impersonating characters or their allies could be a fun trick for a crafty dragon.<br/>The
    dragon can mimic any sounds it has heard, including voices. A creature that hears
    the sounds can tell they are imitations with a successful {@dc 12} Wisdom ({@skill
    Insight}) check.</li><li><b>Rejuvenation</b> You might decide that dragons in
    your campaign, being an essential part of the Material Plane, are nearly impossible
    to destroy. A dragon''s life essence might be preserved in the egg from which
    it first emerged, in its hoard, or in a cavernous hall at the center of the world,
    just as a lich''s essence is hidden in a phylactery.<br/>If it has an essence-preserving
    object, a destroyed dragon gains a new body in {@dice 1d10} days, regaining all
    its hit points and becoming active again. The new body appears within 1.5 meters
    of the object.</li><li><b>Special Senses</b> Most dragons have {@sense blindsight}
    and {@sense darkvision}. You might upgrade {@sense blindsight} to {@sense truesight},
    or you could give a dragon with a burrowing speed {@sense tremorsense}.</li><li><b>Tunneler</b>
    The dragon can burrow through solid rock at half its burrowing speed and leaves
    a tunnel in its wake.</li></ul>'
  limit: 0
  expanded: 0
legendary_actions_desc: null
legendary_actions_limit: 3
legendary_actions_expanded: 0
legendary_actions: []
mythic_actions_desc: null
mythic_actions: []
lair_actions_desc: null
lair_actions: []
regional_effects: []
has_spellbook: true
known_spells: []
initiative: 0
conditions: []
visible: false
active: false
