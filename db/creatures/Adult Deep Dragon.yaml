name: Adult Deep Dragon
alias: Adult Deep Dragon
image_key: null
image_path: null
appearance: null
player_image_path: null
reference_image_path: bestiary/FTD/Adult Deep Dragon.webp
source:
- Adult Deep Dragon
- FTD
note: ''
ac: 17
armor_desc: from natural armor
hpmax: 224
hpmax_roll: 14d12 + 56
hp: 224
xp: 7200
other_xp:
  lair: 8400
size: H
type: dragon
alignment: neutral evil
speed: 12 m., burrow 9 m., fly 24 m., swim 12 m.
strength: 20
dexterity: 14
constitution: 18
intelligence: 16
wisdom: 16
charisma: 18
saving_throws:
- Dex +6
- Con +8
- Wis +7
- Cha +8
abilities:
- Perception +7
- Persuasion +12
- Stealth +10
damage_weaknesses: []
damage_resistances:
- poison
- psychic
damage_immunities: []
condition_immunities:
- charmed
- frightened
- poisoned
senses:
- blindsight 18 m.
- darkvision 45 m.
- Passive perception 17
languages:
- Common
- Draconic
- Undercommon
traits:
- name: Legendary Resistance (3/Day)
  description: If the dragon fails a saving throw, it can choose to succeed instead.
  limit: 3
  expanded: 0
spells: []
actions:
- name: Multiattack
  description: The dragon makes one Bite attack and two Claw attacks.
  limit: 0
  expanded: 0
- name: Bite
  description: '{@atk mw} {@hit 9} to hit, reach 3 m., one target. {@h}16 ({@damage
    2d10 + 5}) piercing damage plus 5 ({@damage 1d10}) poison damage.'
  limit: 0
  expanded: 0
- name: Claw
  description: '{@atk mw} {@hit 9} to hit, reach 1.5 m., one target. {@h}8 ({@damage
    1d6 + 5}) slashing damage.'
  limit: 0
  expanded: 0
- name: Tail
  description: '{@atk mw} {@hit 9} to hit, reach 4.5 m., one target. {@h}9 ({@damage
    1d8 + 5}) bludgeoning damage. If the target is a creature, it must succeed on
    a {@dc 17} Strength saving throw or be knocked {@condition prone}.'
  limit: 0
  expanded: 0
- name: Change Shape
  description: The dragon magically transforms into any creature that is Medium or
    Small, while retaining its game statistics (other than its size). This transformation
    ends if the dragon is reduced to 0 hit points or uses its action to end it.
  limit: 0
  expanded: 0
- name: Nightmare Breath ({@recharge 5-6})
  description: The dragon exhales a cloud of spores in a 18-meter cone. Each creature
    in that area must make a {@dc 16} Wisdom saving throw. On a failed save, the creature
    takes 33 ({@damage 6d10}) psychic damage, and it is {@condition frightened} of
    the dragon for 1 minute. On a successful save, the creature takes half as much
    damage with no additional effects. A {@condition frightened} creature can repeat
    the saving throw at the end of each of its turns, ending the effect on itself
    on a success.
  limit: 1
  expanded: 0
reactions: []
variant:
- name: Customizing Dragons
  description: 'You can customize any dragon''s stat block to reflect the dragon''s
    unique character. Minor changes such as those below are easy to make and have
    no impact on a dragon''s challenge rating.<br/><b>Languages</b> Most dragons prefer
    to speak Draconic but learn Common for dealing with allies and minions. But given
    their high Intelligence and long life span, dragons can easily learn additional
    languages. You can add languages to a dragon''s stat block.<br/><b>Skills</b>
    Most dragons are proficient in the {@skill Perception} and {@skill Stealth} skills,
    and many dragons have additional skill proficiencies. As with languages, you can
    customize a dragon''s skill list (even doubling their proficiency bonus with certain
    skills) to reflect particular interests and activities. You can also give a dragon
    tool proficiencies, particularly if the dragon spends time in Humanoid form.<br/><b>Spells</b>
    {@note See the ''Variant: Dragons as Innate Spellcasters'' inset(s), below.}<br/><b>Other
    Traits and Actions</b> You can borrow traits and actions from other monsters to
    add unique flavor to a dragon. Consider these examples:<br/><ul><li><b>Change
    Shape</b> You can decide that a dragon acquires this action at a younger age than
    usual, particularly if you want to feature a dragon in Humanoid form in your campaign:<br/>The
    dragon magically polymorphs into a humanoid or beast that has a challenge rating
    no higher than its own, or back into its true form. It reverts to its true form
    if it dies. Any equipment it is wearing or carrying is absorbed or borne by the
    new form (the dragon''s choice).<br/>In a new form, the dragon retains its alignment,
    hit points, Hit Dice, ability to speak, proficiencies, Legendary Resistance, lair
    actions, and Intelligence, Wisdom, and Charisma scores, as well as this action.
    Its statistics and capabilities are otherwise replaced by those of the new form,
    except any class features or legendary actions of that form.</li><li><b>Flyby</b>
    The dragon is an agile flier, quick to fly out of enemies'' reach.<br/>The dragon
    doesn''t provoke an opportunity attack when it flies out of an enemy''s reach.</li><li><b>Mimicry</b>
    Impersonating characters or their allies could be a fun trick for a crafty dragon.<br/>The
    dragon can mimic any sounds it has heard, including voices. A creature that hears
    the sounds can tell they are imitations with a successful {@dc 12} Wisdom ({@skill
    Insight}) check.</li><li><b>Rejuvenation</b> You might decide that dragons in
    your campaign, being an essential part of the Material Plane, are nearly impossible
    to destroy. A dragon''s life essence might be preserved in the egg from which
    it first emerged, in its hoard, or in a cavernous hall at the center of the world,
    just as a lich''s essence is hidden in a phylactery.<br/>If it has an essence-preserving
    object, a destroyed dragon gains a new body in {@dice 1d10} days, regaining all
    its hit points and becoming active again. The new body appears within 1.5 meters
    of the object.</li><li><b>Special Senses</b> Most dragons have {@sense blindsight}
    and {@sense darkvision}. You might upgrade {@sense blindsight} to {@sense truesight},
    or you could give a dragon with a burrowing speed {@sense tremorsense}.</li><li><b>Tunneler</b>
    The dragon can burrow through solid rock at half its burrowing speed and leaves
    a tunnel in its wake.</li></ul>'
  limit: 0
  expanded: 0
legendary_actions_desc: The Adult Deep Dragon can take 3 legendary actions, choosing
  from the options below. Only one legendary action can be used at a time and only
  at the end of another creature's turn. The Adult Deep Dragon regains spent legendary
  actions at the start of its turn.
legendary_actions_limit: 3
legendary_actions_expanded: 0
legendary_actions:
- name: Commanding Spores
  description: The dragon releases spores around a creature within 9 meters of it
    that it can see. The target must succeed on a {@dc 16} Wisdom saving throw or
    use its reaction to make a melee weapon attack against a random creature within
    reach. If no creatures are within reach, or the target can't take a reaction,
    it takes 5 ({@damage 1d10}) psychic damage.
  limit: 0
  expanded: 0
- name: Tail
  description: The dragon makes one Tail attack.
  limit: 0
  expanded: 0
- name: Spore Salvo (Costs 2 Actions)
  description: The dragon releases poisonous spores around a creature within 9 meters
    of it that it can see. The target must succeed on a {@dc 16} Constitution saving
    throw or take 17 ({@damage 5d6}) poison damage and become {@condition poisoned}
    for 1 minute. The {@condition poisoned} creature can repeat the saving throw at
    the end of each of its turns, ending the effect on itself on a success.
  limit: 0
  expanded: 0
mythic_actions_desc: null
mythic_actions: []
lair_actions_desc: 'On initiative count 20 (losing initiative ties), the dragon can
  take one of the following lair actions; the dragon can''t take the same lair action
  two rounds in a row:'
lair_actions:
- name: Deep Torpor
  description: The dragon casts the {@spell slow} spell, requiring no spell components
    and using Charisma as the spellcasting ability (spell save DC 16). The spell ends
    early if the dragon uses this lair action again or if the dragon dies.
  limit: 0
  expanded: 0
- name: Mossy Sludge
  description: The dragon conjures sludge-like moss that briefly covers surfaces in
    the lair. The ceiling, floor, and walls of the lair become difficult terrain until
    initiative count 20 on the next round.
  limit: 0
  expanded: 0
- name: Toxic Spores
  description: The dragon fills a 6-meter cube it can see within 36 meters of itself
    with toxic spores. Each creature in that area must succeed on a DC 15 Constitution
    saving throw or take 14 ({@damage 4d6}) poison damage and be {@condition poisoned}
    until the end of its next turn.
  limit: 0
  expanded: 0
regional_effects:
- name: Preservation of Knowledge
  description: Books, letters, and any other physical forms of writing within 9.5
    kilometers of the dragon's lair become magically charged and can't be damaged
    by nonmagical means.
  limit: 0
  expanded: 0
- name: Restless Sleep
  description: When a creature finishes a long rest within 9.5 kilometers of the lair,
    the creature must first succeed on a DC 10 Constitution saving throw or be unable
    to reduce its level of {@condition exhaustion}. Creatures immune to the {@condition
    poisoned} condition are immune to this effect.
  limit: 0
  expanded: 0
- name: Verdant Growth
  description: Vegetation and fungi within 9.5 kilometers of the dragon's lair grow
    faster and cover a greater area than they normally would. Foraging in this area
    yields twice the usual amount of food.
  limit: 0
  expanded: 0
- name: ''
  description: If the dragon dies, these effects fade over the course of {@dice 1d10}
    days.
  limit: 0
  expanded: 0
has_spellbook: false
known_spells: []
initiative: 0
conditions: []
visible: false
active: false
