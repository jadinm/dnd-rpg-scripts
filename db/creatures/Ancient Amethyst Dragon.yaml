name: Ancient Amethyst Dragon
alias: Ancient Amethyst Dragon
image_key: null
image_path: null
appearance: null
player_image_path: null
reference_image_path: bestiary/FTD/Ancient Amethyst Dragon.webp
source:
- Ancient Amethyst Dragon
- FTD
note: ''
ac: 20
armor_desc: from natural armor
hpmax: 672
hpmax_roll: 24d20 + 192
hp: 672
xp: 50000
other_xp:
  lair: 62000
size: G
type: dragon(gem)
alignment: neutral
speed: 12 m., fly 24 m. (hover), swim 12 m., canHover 0.5 m.
strength: 26
dexterity: 14
constitution: 27
intelligence: 26
wisdom: 19
charisma: 23
saving_throws:
- Dex +9
- Con +15
- Wis +11
- Cha +13
abilities:
- Arcana +22
- Perception +18
- Persuasion +13
- Stealth +9
damage_weaknesses: []
damage_resistances:
- force
- psychic
damage_immunities: []
condition_immunities:
- frightened
- prone
senses:
- blindsight 18 m.
- darkvision 36 m.
- Passive perception 28
languages:
- Common
- Draconic
- telepathy 120 ft.
traits:
- name: Amphibious
  description: The dragon can breathe both air and water.
  limit: 0
  expanded: 0
- name: Legendary Resistance (3/Day)
  description: If the dragon fails a saving throw, it can choose to succeed instead.
  limit: 3
  expanded: 0
- name: Spellcasting (Psionics)
  description: 'The dragon casts one of the following spells, requiring no spell components
    and using Intelligence as the spellcasting ability (spell save {@dc 23}, {@hit
    15} to hit with spell attacks):'
  limit: 0
  expanded: 0
spells:
- name: 1/day each
  spells:
  - '{@spell blink}'
  - '{@spell control water}'
  - '{@spell dispel magic}'
  - '{@spell freedom of movement}'
  - '{@spell globe of invulnerability}'
  - '{@spell plane shift}'
  - '{@spell protection from evil and good}'
  - '{@spell sending}'
  limit: 8
  expanded: 0
actions:
- name: Multiattack
  description: The dragon makes one Bite attack and two Claw attacks.
  limit: 0
  expanded: 0
- name: Bite
  description: '{@atk mw} {@hit 15} to hit, reach 4.5 m., one target. {@h}19 ({@damage
    2d10 + 8}) piercing damage plus 13 ({@damage 3d8}) force damage.'
  limit: 0
  expanded: 0
- name: Claw
  description: '{@atk mw} {@hit 15} to hit, reach 3 m., one target. {@h}15 ({@damage
    2d6 + 8}) slashing damage.'
  limit: 0
  expanded: 0
- name: Singularity Breath ({@recharge 5-6})
  description: The dragon creates a shining bead of gravitational force in its mouth,
    then releases the energy in a 27-meter cone. Each creature in that area must make
    a {@dc 23} Strength saving throw. On a failed save, the creature takes 63 ({@damage
    14d8}) force damage, and its speed becomes 0 until the start of the dragon's next
    turn. On a successful save, the creature takes half as much damage, and its speed
    isn't reduced.
  limit: 1
  expanded: 0
reactions: []
variant:
- name: Customizing Dragons
  description: 'You can customize any dragon''s stat block to reflect the dragon''s
    unique character. Minor changes such as those below are easy to make and have
    no impact on a dragon''s challenge rating.<br/><b>Languages</b> Most dragons prefer
    to speak Draconic but learn Common for dealing with allies and minions. But given
    their high Intelligence and long life span, dragons can easily learn additional
    languages. You can add languages to a dragon''s stat block.<br/><b>Skills</b>
    Most dragons are proficient in the {@skill Perception} and {@skill Stealth} skills,
    and many dragons have additional skill proficiencies. As with languages, you can
    customize a dragon''s skill list (even doubling their proficiency bonus with certain
    skills) to reflect particular interests and activities. You can also give a dragon
    tool proficiencies, particularly if the dragon spends time in Humanoid form.<br/><b>Spells</b>
    {@note See the ''Variant: Dragons as Innate Spellcasters'' inset(s), below.}<br/><b>Other
    Traits and Actions</b> You can borrow traits and actions from other monsters to
    add unique flavor to a dragon. Consider these examples:<br/><ul><li><b>Change
    Shape</b> You can decide that a dragon acquires this action at a younger age than
    usual, particularly if you want to feature a dragon in Humanoid form in your campaign:<br/>The
    dragon magically polymorphs into a humanoid or beast that has a challenge rating
    no higher than its own, or back into its true form. It reverts to its true form
    if it dies. Any equipment it is wearing or carrying is absorbed or borne by the
    new form (the dragon''s choice).<br/>In a new form, the dragon retains its alignment,
    hit points, Hit Dice, ability to speak, proficiencies, Legendary Resistance, lair
    actions, and Intelligence, Wisdom, and Charisma scores, as well as this action.
    Its statistics and capabilities are otherwise replaced by those of the new form,
    except any class features or legendary actions of that form.</li><li><b>Flyby</b>
    The dragon is an agile flier, quick to fly out of enemies'' reach.<br/>The dragon
    doesn''t provoke an opportunity attack when it flies out of an enemy''s reach.</li><li><b>Mimicry</b>
    Impersonating characters or their allies could be a fun trick for a crafty dragon.<br/>The
    dragon can mimic any sounds it has heard, including voices. A creature that hears
    the sounds can tell they are imitations with a successful {@dc 14} Wisdom ({@skill
    Insight}) check.</li><li><b>Rejuvenation</b> You might decide that dragons in
    your campaign, being an essential part of the Material Plane, are nearly impossible
    to destroy. A dragon''s life essence might be preserved in the egg from which
    it first emerged, in its hoard, or in a cavernous hall at the center of the world,
    just as a lich''s essence is hidden in a phylactery.<br/>If it has an essence-preserving
    object, a destroyed dragon gains a new body in {@dice 1d10} days, regaining all
    its hit points and becoming active again. The new body appears within 1.5 meters
    of the object.</li><li><b>Special Senses</b> Most dragons have {@sense blindsight}
    and {@sense darkvision}. You might upgrade {@sense blindsight} to {@sense truesight},
    or you could give a dragon with a burrowing speed {@sense tremorsense}.</li></ul>'
  limit: 0
  expanded: 0
legendary_actions_desc: The Ancient Amethyst Dragon can take 3 legendary actions,
  choosing from the options below. Only one legendary action can be used at a time
  and only at the end of another creature's turn. The Ancient Amethyst Dragon regains
  spent legendary actions at the start of its turn.
legendary_actions_limit: 3
legendary_actions_expanded: 0
legendary_actions:
- name: Claw
  description: The dragon makes one claw attack.
  limit: 0
  expanded: 0
- name: Psionics (Costs 2 Actions)
  description: The dragon uses Psychic Step or Spellcasting.
  limit: 0
  expanded: 0
- name: Explosive Crystal (Costs 3 Actions)
  description: The dragon spits an amethyst that that explodes at a point it can see
    within 18 meters of it. Each creature within a 6-meter-radius sphere centered
    on that point must succeed on a {@dc 23} Dexterity saving throw or take 18 ({@damage
    4d8}) force damage and be knocked {@condition prone}.
  limit: 0
  expanded: 0
mythic_actions_desc: null
mythic_actions: []
lair_actions_desc: 'On initiative count 20 (losing initiative ties), the dragon can
  take one of the following lair actions; the dragon can''t take the same lair action
  two rounds in a row:'
lair_actions:
- name: Beguiling Whisper
  description: The dragon telepathically whispers to one creature within range of
    the dragon's telepathy. The creature must succeed on a DC 15 Wisdom saving throw
    or be {@condition charmed} by the dragon until initiative count 20 on the next
    round. A creature {@condition charmed} in this way obeys to the best of its ability
    any command the dragon issues that isn't directly harmful to the creature.
  limit: 0
  expanded: 0
- name: Imprisoning Force
  description: The dragon casts the {@spell forcecage} spell, using its spell save
    DC and requiring no spell components. The spell ends early if the dragon uses
    this lair action again or dies.
  limit: 0
  expanded: 0
- name: Spatial Projection
  description: The dragon chooses a space it can fit into within the lair. It exists
    in its own space and the chosen space simultaneously until initiative count 20
    on the next round. Whenever it moves or takes an action, it chooses which version
    of itself is moving or acting. If an effect or attack can target both of the dragon's
    spaces at the same time, the dragon is affected only once.
  limit: 0
  expanded: 0
regional_effects:
- name: Background Check
  description: Once per day, the dragon can cast the {@spell legend lore} spell, requiring
    no spell components, naming any person, place, or object within 1.5 kilometers
    of the lair as the spell's subject.
  limit: 0
  expanded: 0
- name: Crystal Profusion
  description: Amethyst crystals and geodes form along muddy shores and lake beds
    within 9.5 kilometers of the lair.
  limit: 0
  expanded: 0
- name: Thriving Wildlife
  description: Fish and other aquatic Beasts reproduce rapidly and thrive in bodies
    of water within 9.5 kilometers of the lair. Foraging in these waters yields twice
    the usual amount of food.
  limit: 0
  expanded: 0
- name: Watery Sight
  description: Water within 9.5 kilometers of the lair is a conduit for the dragon's
    psionic presence. As an action, the dragon can cast the {@spell clairvoyance}
    spell, requiring no spell components and targeting any body of water in that region.
  limit: 0
  expanded: 0
- name: ''
  description: If the dragon dies, the populations of aquatic life near the lair return
    to normal levels over the course of {@dice 1d10} days. The existing abundance
    of amethysts remains, but new crystals and geodes form at a normal rate.
  limit: 0
  expanded: 0
has_spellbook: true
known_spells: []
initiative: 0
conditions: []
visible: false
active: false
