name: Nurvureem, The Dark Lady
alias: Nurvureem, The Dark Lady
image_key: null
image_path: null
appearance: null
player_image_path: null
reference_image_path: PotA/Nurvureem, The Dark Lady.png
source:
- Nurvureem, The Dark Lady
- PotA
note: ''
ac: 19
armor_desc: from natural armor
hpmax: 289
hpmax_roll: 17d12 + 85
hp: 289
xp: 11500
other_xp: {}
size: H
type: dragon
alignment: chaotic evil
speed: 12 m., fly 24 m., swim 12 m.
strength: 23
dexterity: 14
constitution: 21
intelligence: 14
wisdom: 13
charisma: 17
saving_throws:
- Dex +7
- Con +10
- Wis +6
- Cha +8
abilities:
- Perception +11
- Stealth +12
damage_weaknesses: []
damage_resistances:
- necrotic
damage_immunities:
- acid
condition_immunities: []
senses:
- blindsight 18 m.
- darkvision 36 m.
- Passive perception 21
languages:
- Common
- Draconic
traits:
- name: Amphibious
  description: Nurvureem can breathe air and water.
  limit: 0
  expanded: 0
- name: Legendary Resistance (3/Day)
  description: If Nurvureem fails a saving throw, it can choose to succeed instead.
  limit: 3
  expanded: 0
- name: Living Shadow
  description: While in dim light or darkness, the dragon has resistance to damage
    that isn't force, psychic, or radiant.
  limit: 0
  expanded: 0
- name: Shadow Stealth
  description: While in dim light or darkness, the dragon can take the {@action Hide}
    action as a bonus action.
  limit: 0
  expanded: 0
- name: Sunlight Sensitivity
  description: While in sunlight, the dragon has disadvantage on attack rolls, as
    well as on Wisdom ({@skill Perception}) checks that rely on sight.
  limit: 0
  expanded: 0
spells: []
actions:
- name: Multiattack
  description: 'Nurvureem can use its Frightful Presence. It then makes three attacks:
    one with its bite and two with its claws.'
  limit: 0
  expanded: 0
- name: Bite
  description: '{@atk mw} {@hit 11} to hit, reach 3 m., one target. {@h}17 ({@damage
    2d10 + 6}) piercing damage plus 4 ({@damage 1d8}) acid damage.'
  limit: 0
  expanded: 0
- name: Claw
  description: '{@atk mw} {@hit 11} to hit, reach 1.5 m., one target. {@h}13 ({@damage
    2d6 + 6}) slashing damage.'
  limit: 0
  expanded: 0
- name: Tail
  description: '{@atk mw} {@hit 11} to hit, reach 4.5 m., one target. {@h}15 ({@damage
    2d8 + 6}) bludgeoning damage.'
  limit: 0
  expanded: 0
- name: Frightful Presence
  description: Each creature of Nurvureem's choice that is within 36 meters of Nurvureem
    and aware of it must succeed on a {@dc 16} Wisdom saving throw or become {@condition
    frightened} for 1 minute. A creature can repeat the saving throw at the end of
    each of its turns, ending the effect on itself on a success. If a creature's saving
    throw is successful or the effect ends for it, the creature is immune to Nurvureem's
    Frightful Presence for the next 24 hours.
  limit: 0
  expanded: 0
- name: Acid Breath ({@recharge 5-6})
  description: Nurvureem exhales acid in a 18-meter line that is 1.5 meters wide.
    Each creature in that line must make a {@dc 18} Dexterity saving throw, taking
    54 ({@damage 12d8}) acid damage on a failed save, or half as much damage on a
    successful one.
  limit: 1
  expanded: 0
reactions: []
variant:
- name: Customizing Dragons
  description: 'You can customize any dragon''s stat block to reflect Nurvureem''s
    unique character. Minor changes such as those below are easy to make and have
    no impact on a dragon''s challenge rating.<br/><b>Languages</b> Most dragons prefer
    to speak Draconic but learn Common for dealing with allies and minions. But given
    their high Intelligence and long life span, dragons can easily learn additional
    languages. You can add languages to a dragon''s stat block.<br/><b>Skills</b>
    Most dragons are proficient in the {@skill Perception} and {@skill Stealth} skills,
    and many dragons have additional skill proficiencies. As with languages, you can
    customize a dragon''s skill list (even doubling their proficiency bonus with certain
    skills) to reflect particular interests and activities. You can also give a dragon
    tool proficiencies, particularly if Nurvureem spends time in Humanoid form.<br/><b>Spells</b>
    {@note See the ''Variant: Dragons as Innate Spellcasters'' inset(s), below.}<br/><b>Other
    Traits and Actions</b> You can borrow traits and actions from other monsters to
    add unique flavor to a dragon. Consider these examples:<br/><ul><li><b>Flyby</b>
    Nurvureem is an agile flier, quick to fly out of enemies'' reach.<br/>Nurvureem
    doesn''t provoke an opportunity attack when it flies out of an enemy''s reach.</li><li><b>Mimicry</b>
    Impersonating characters or their allies could be a fun trick for a crafty dragon.<br/>Nurvureem
    can mimic any sounds it has heard, including voices. A creature that hears the
    sounds can tell they are imitations with a successful {@dc 11} Wisdom ({@skill
    Insight}) check.</li><li><b>Rejuvenation</b> You might decide that dragons in
    your campaign, being an essential part of the Material Plane, are nearly impossible
    to destroy. A dragon''s life essence might be preserved in the egg from which
    it first emerged, in its hoard, or in a cavernous hall at the center of the world,
    just as a lich''s essence is hidden in a phylactery.<br/>If it has an essence-preserving
    object, a destroyed dragon gains a new body in {@dice 1d10} days, regaining all
    its hit points and becoming active again. The new body appears within 1.5 meters
    of the object.</li><li><b>Special Senses</b> Most dragons have {@sense blindsight}
    and {@sense darkvision}. You might upgrade {@sense blindsight} to {@sense truesight},
    or you could give a dragon with a burrowing speed {@sense tremorsense}.</li></ul>'
  limit: 0
  expanded: 0
legendary_actions_desc: Nurvureem, The Dark Lady can take 3 legendary actions, choosing
  from the options below. Only one legendary action can be used at a time and only
  at the end of another creature's turn. Nurvureem, The Dark Lady regains spent legendary
  actions at the start of its turn.
legendary_actions_limit: 3
legendary_actions_expanded: 0
legendary_actions:
- name: Detect
  description: Nurvureem makes a Wisdom ({@skill Perception}) check.
  limit: 0
  expanded: 0
- name: Tail Attack
  description: Nurvureem makes a tail attack.
  limit: 0
  expanded: 0
- name: Wing Attack (Costs 2 Actions)
  description: Nurvureem beats its wings. Each creature within 3 meters of Nurvureem
    must succeed on a {@dc 19} Dexterity saving throw or take 13 ({@damage 2d6 + 6})
    bludgeoning damage and be knocked {@condition prone}. Nurvureem can then fly up
    to half its flying speed.
  limit: 0
  expanded: 0
mythic_actions_desc: null
mythic_actions: []
lair_actions_desc: null
lair_actions: []
regional_effects: []
has_spellbook: false
known_spells: []
initiative: 0
conditions: []
visible: false
active: false
