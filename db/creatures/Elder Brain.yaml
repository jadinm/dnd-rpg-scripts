name: Elder Brain
alias: Elder Brain
image_key: null
image_path: null
appearance: null
player_image_path: null
reference_image_path: MPMM/Elder Brain.png
source:
- Elder Brain
- MPMM
note: ''
ac: 10
armor_desc: null
hpmax: 300
hpmax_roll: 20d10 + 100
hp: 300
xp: 11500
other_xp: {}
size: L
type: aberration(mind flayer)
alignment: lawful evil
speed: 1.5 m., swim 3 m.
strength: 15
dexterity: 10
constitution: 20
intelligence: 21
wisdom: 19
charisma: 24
saving_throws:
- Int +10
- Wis +9
- Cha +12
abilities:
- Arcana +10
- Deception +12
- Insight +14
- Intimidation +12
- Persuasion +12
damage_weaknesses: []
damage_resistances: []
damage_immunities: []
condition_immunities: []
senses:
- blindsight 36 m.
- Passive perception 14
languages:
- understands Common
- Deep Speech
- and Undercommon but can't speak
- telepathy 5 miles
traits:
- name: Creature Sense
  description: The elder brain is aware of creatures within 8 kilometers of it that
    have an Intelligence score of 4 or higher. It knows the distance and direction
    to each creature, as well as each one's Intelligence score, but can't sense anything
    else about it. A creature protected by a {@spell mind blank} spell, a {@spell
    nondetection} spell, or similar magic can't be perceived in this manner.
  limit: 0
  expanded: 0
- name: Legendary Resistance (3/Day)
  description: If the elder brain fails a saving throw, it can choose to succeed instead.
  limit: 3
  expanded: 0
- name: Magic Resistance
  description: The elder brain has advantage on saving throws against spells and other
    magical effects.
  limit: 0
  expanded: 0
- name: Telepathic Hub
  description: The elder brain can use its telepathy to initiate and maintain telepathic
    conversations with up to ten creatures at a time. The elder brain can let those
    creatures telepathically hear each other while connected in this way.
  limit: 0
  expanded: 0
- name: Spellcasting (Psionics)
  description: 'The elder brain casts one of the following spells, requiring no spell
    components and using Intelligence as the spellcasting ability (spell save {@dc
    18}):'
  limit: 0
  expanded: 0
spells:
- name: 3/day
  spells:
  - '{@spell modify memory}'
  limit: 3
  expanded: 0
- name: 1/day each
  spells:
  - '{@spell dominate monster}'
  - '{@spell plane shift} (self only)'
  limit: 2
  expanded: 0
- name: At will
  spells:
  - '{@spell detect thoughts}'
  - '{@spell levitate}'
  limit: 0
  expanded: 0
actions:
- name: Tentacle
  description: '{@atk mw} {@hit 7} to hit, reach 9 m., one target. {@h}20 ({@damage
    4d8 + 2}) bludgeoning damage. If the target is a Huge or smaller creature, it
    is {@condition grappled} (escape {@dc 15}) and takes 9 ({@damage 1d8 + 5}) psychic
    damage at the start of each of its turns until the grapple ends. The elder brain
    can have up to four targets {@condition grappled} at a time.'
  limit: 0
  expanded: 0
- name: Mind Blast ({@recharge 5-6})
  description: Creatures of the elder brain's choice within 18 meters of it must succeed
    on a {@dc 18} Intelligence saving throw or take 32 ({@damage 5d10 + 5}) psychic
    damage and be {@condition stunned} for 1 minute. A target can repeat the saving
    throw at the end of each of its turns, ending the effect on itself on a success.
  limit: 1
  expanded: 0
reactions: []
variant: []
legendary_actions_desc: The Elder Brain can take 3 legendary actions, choosing from
  the options below. Only one legendary action can be used at a time and only at the
  end of another creature's turn. The Elder Brain regains spent legendary actions
  at the start of its turn.
legendary_actions_limit: 3
legendary_actions_expanded: 0
legendary_actions:
- name: Break Concentration
  description: The elder brain targets one creature within 36 meters of it with which
    it has a psychic link. The elder brain breaks the creature's concentration on
    a spell it has cast. The creature also takes 2 ({@damage 1d4}) psychic damage
    per level of the spell.
  limit: 0
  expanded: 0
- name: Psychic Pulse
  description: The elder brain targets one creature within 36 meters of it with which
    it has a psychic link. The target and enemies of the elder brain within 9 meters
    of target take 10 ({@damage 3d6}) psychic damage.
  limit: 0
  expanded: 0
- name: Sever Psychic Link
  description: The elder brain targets one creature within 36 meters of it with which
    it has a psychic link. The elder brain ends the link, causing the creature to
    have disadvantage on all ability checks, attack rolls, and saving throws until
    the end of the creature's next turn.
  limit: 0
  expanded: 0
- name: Tentacle (Costs 2 Actions)
  description: The elder brain makes one Tentacle attack.
  limit: 0
  expanded: 0
mythic_actions_desc: null
mythic_actions: []
lair_actions_desc: 'On initiative count 20 (losing initiative ties), an elder brain
  can take one of the following lair actions; the elder brain can''t take the same
  lair action two rounds in a row:'
lair_actions:
- name: Force Wall
  description: The elder brain casts {@spell wall of force}.
  limit: 0
  expanded: 0
- name: Psionic Anchor
  description: The elder brain targets one creature it can sense within 36 meters
    of it and anchors it by sheer force of will. The target must make a {@dc 18} Charisma
    saving throw. On a failed save, its speed is reduced to 0, and it can't teleport.
    It can repeat the saving throw at the end of each of its turns, ending the effect
    on itself on a success.
  limit: 0
  expanded: 0
- name: Psychic Inspiration
  description: The elder brain targets one friendly creature it can sense within 36
    meters of it. The target has a flash of inspiration and gains advantage on one
    attack roll, ability check, or saving throw it makes before the end of its next
    turn.
  limit: 0
  expanded: 0
regional_effects:
- name: Paranoia
  description: Creatures within 8 kilometers of an elder brain feel as if they are
    being followed, even when they're not.
  limit: 0
  expanded: 0
- name: Psychic Whispers
  description: Any creature with which the elder brain has formed a psychic link hears
    faint, incomprehensible whispers in the deepest recesses of its mind. This psychic
    detritus consists of the elder brain's stray thoughts commingled with those of
    other creatures to which it is linked.
  limit: 0
  expanded: 0
- name: Telepathic Eavesdropping
  description: The elder brain can overhear any telepathic conversation within 8 kilometers
    of it. The creature that initiated the telepathic conversation makes a {@dc 18}
    Wisdom saving throw when telepathic contact is first established. If the save
    is successful, the creature is aware that something is eavesdropping. The nature
    of the eavesdropper isn't revealed.
  limit: 0
  expanded: 0
- name: ''
  description: If the elder brain dies, these effects immediately end.
  limit: 0
  expanded: 0
has_spellbook: true
known_spells: []
initiative: 0
conditions: []
visible: false
active: false
