name: Morgantha
alias: Morgantha
image_key: null
image_path: null
appearance: null
player_image_path: null
reference_image_path: CoS/Morgantha.png
source:
- Morgantha
- CoS
note: ''
ac: 17
armor_desc: from natural armor
hpmax: 165
hpmax_roll: 15d8 + 45
hp: 165
xp: 1800
other_xp:
  coven: 2900
size: M
type: fiend
alignment: neutral evil
speed: 9 m.
strength: 18
dexterity: 15
constitution: 16
intelligence: 16
wisdom: 14
charisma: 16
saving_throws:
- Str +7
- Con +6
abilities:
- Deception +6
- Insight +5
- Perception +5
- Stealth +5
damage_weaknesses: []
damage_resistances:
- cold
- fire
- bludgeoning from nonmagical attacks that aren't silvered
- piercing from nonmagical attacks that aren't silvered
- slashing from nonmagical attacks that aren't silvered
damage_immunities: []
condition_immunities:
- charmed
senses:
- darkvision 36 m.
- Passive perception 16
languages:
- Abyssal
- Common
- Infernal
- Primordial
traits:
- name: Magic Resistance
  description: Morgantha has advantage on saving throws against spells and other magical
    effects.
  limit: 0
  expanded: 0
- name: Night Hag Items
  description: 'A night hag carries two very rare magic items that she must craft
    for herself. If either object is lost, the night hag will go to great lengths
    to retrieve it, as creating a new tool takes time and effort.<br/>Heartstone:
    This lustrous black gem allows a night hag to become ethereal while it is in her
    possession. The touch of a heartstone also cures any disease. Crafting a heartstone
    takes 30 days.<br/>Soul Bag: When an evil humanoid dies as a result of a night
    hag''s Nightmare Haunting, Morgantha catches the soul in this black sack made
    of stitched flesh. A soul bag can hold only one evil soul at a time, and only
    the night hag who crafted the bag can catch a soul with it. Crafting a soul bag
    takes 7 days and a humanoid sacrifice (whose flesh is used to make the bag).'
  limit: 0
  expanded: 0
- name: Innate Spellcasting
  description: 'Morgantha''s innate spellcasting ability is Charisma (spell save {@dc
    14}, {@hit 6} to hit with spell attacks). She can innately cast the following
    spells, requiring no material components:'
  limit: 0
  expanded: 0
spells:
- name: 2/day each
  spells:
  - '{@spell plane shift} (self only)'
  - '{@spell ray of enfeeblement}'
  - '{@spell sleep}'
  limit: 6
  expanded: 0
- name: At will
  spells:
  - '{@spell detect magic}'
  - '{@spell magic missile}'
  limit: 0
  expanded: 0
actions:
- name: Claws (Hag Form Only)
  description: '{@atk mw} {@hit 7} to hit, reach 1.5 m., one target. {@h}13 ({@damage
    2d8 + 4}) slashing damage.'
  limit: 0
  expanded: 0
- name: Change Shape
  description: Morgantha magically polymorphs into a Small or Medium female humanoid,
    or back into her true form. Her statistics are the same in each form. Any equipment
    she is wearing or carrying isn't transformed. She reverts to her true form if
    she dies.
  limit: 0
  expanded: 0
- name: Etherealness
  description: Morgantha magically enters the Ethereal Plane from the Material Plane,
    or vice versa. To do so, Morgantha must have a heartstone in her possession.
  limit: 0
  expanded: 0
- name: Nightmare Haunting (1/Day)
  description: While on the Ethereal Plane, Morgantha magically touches a sleeping
    humanoid on the Material Plane. A {@spell protection from evil and good} spell
    cast on the target prevents this contact, as does a magic circle. As long as the
    contact persists, the target has dreadful visions. If these visions last for at
    least 1 hour, the target gains no benefit from its rest, and its hit point maximum
    is reduced by 5 ({@dice 1d10}). If this effect reduces the target's hit point
    maximum to 0, the target dies, and if the target was evil, its soul is trapped
    in Morgantha's soul bag. The reduction to the target's hit point maximum lasts
    until removed by the  {@spell greater restoration} spell or similar magic.
  limit: 1
  expanded: 0
reactions: []
variant:
- name: Hag Covens
  description: When hags must work together, they form covens, in spite of their selfish
    natures. A coven is made up of hags of any type, all of whom are equals within
    the group. However, each of Morganthas continues to desire more personal power.<br/>A
    coven consists of three hags so that any arguments between two hags can be settled
    by the third. If more than three hags ever come together, as might happen if two
    covens come into conflict, the result is usually chaos.<br/>While all three members
    of a hag coven are within 9 meters of one another, they can each cast the following
    spells from the wizard's spell list but must share the spell slots among themselves
    (see below (see below)<br/>For casting these spells, each hag is a 12th-level
    spellcaster that uses Intelligence as her spellcasting ability. The spell save
    DC is 12 + Morgantha's Intelligence modifier, and the spell attack bonus is 4
    + Morgantha's Intelligence modifier.<br/><b>Hag Eye</b> A hag coven can craft
    a magic item called a hag eye, which is made from a real eye coated in varnish
    and often fitted to a pendant or other wearable item. Morgantha eye is usually
    entrusted to a minion for safekeeping and transport. A hag in the coven can take
    an action to see what Morgantha eye sees if Morgantha eye is on the same plane
    of existence. A hag eye has AC 10, 1 hit point, and darkvision with a radius of
    18 meters. If it is destroyed, each coven member takes {@dice 3d10} psychic damage
    and is {@condition blinded} for 24 hours.<br/>A hag coven can have only one hag
    eye at a time, and creating a new one requires all three members of the coven
    to perform a ritual. The ritual takes 1 hour, and Morganthas can't perform it
    while {@condition blinded}. During the ritual, if Morganthas take any action other
    than performing the ritual, they must start over.
  spells:
  - name: Level 1
    spells:
    - '{@spell identify}'
    - '{@spell ray of sickness}'
    limit: 4
    expanded: 0
  - name: Level 2
    spells:
    - '{@spell hold person}'
    - '{@spell locate object}'
    limit: 3
    expanded: 0
  - name: Level 3
    spells:
    - '{@spell bestow curse}'
    - '{@spell counterspell}'
    - '{@spell lightning bolt}'
    limit: 3
    expanded: 0
  - name: Level 4
    spells:
    - '{@spell phantasmal killer}'
    - '{@spell polymorph}'
    limit: 3
    expanded: 0
  - name: Level 5
    spells:
    - '{@spell contact other plane}'
    - '{@spell scrying}'
    limit: 2
    expanded: 0
  - name: Level 6
    spells:
    - '{@spell eyebite}'
    limit: 1
    expanded: 0
  limit: 0
  expanded: 0
legendary_actions_desc: null
legendary_actions_limit: 3
legendary_actions_expanded: 0
legendary_actions: []
mythic_actions_desc: null
mythic_actions: []
lair_actions_desc: null
lair_actions: []
regional_effects: []
has_spellbook: true
known_spells: []
initiative: 0
conditions: []
visible: false
active: false
