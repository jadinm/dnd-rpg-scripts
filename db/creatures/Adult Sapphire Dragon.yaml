name: Adult Sapphire Dragon
alias: Adult Sapphire Dragon
image_key: null
image_path: null
appearance: null
player_image_path: null
reference_image_path: bestiary/SADS/Adult Sapphire Dragon.webp
source:
- Adult Sapphire Dragon
- SADS
note: ''
ac: 19
armor_desc: from natural armor
hpmax: 276
hpmax_roll: 18d12 + 60
hp: 276
xp: 13000
other_xp: {}
size: H
type: dragon
alignment: lawful neutral
speed: 12 m., climb 12 m., fly 24 m., burrow 12 m.
strength: 23
dexterity: 16
constitution: 21
intelligence: 18
wisdom: 17
charisma: 18
saving_throws:
- Dex +8
- Con +10
- Wis +8
- Cha +9
abilities:
- History +9
- Perception +13
- Persuasion +14
- Stealth +8
damage_weaknesses: []
damage_resistances: []
damage_immunities:
- thunder
condition_immunities:
- frightened
senses:
- blindsight 18 m.
- darkvision 36 m.
- Passive perception 23
languages:
- telepathy 120 ft.
- Common
- Draconic
traits:
- name: Legendary Resistance (2/Day)
  description: If the dragon fails a saving throw, it can choose to succeed instead.
  limit: 2
  expanded: 0
- name: Spider Climb
  description: The dragon can climb difficult surfaces, including upside down on ceilings,
    without needing to make an ability check.
  limit: 0
  expanded: 0
- name: Tunneler
  description: The dragon can burrow through solid rock at half its burrow speed and
    can choose to leave a 3-meter-diameter tunnel in its wake.
  limit: 0
  expanded: 0
- name: Innate Spellcasting (Psionics)
  description: 'The dragon''s innate spellcasting ability is Intelligence (spell save
    {@dc 17}). It can innately cast the following spells, requiring no components:'
  limit: 0
  expanded: 0
spells:
- name: 1/day each
  spells:
  - '{@spell scrying}'
  - '{@spell telekinesis}'
  - '{@spell teleportation circle}'
  limit: 3
  expanded: 0
actions:
- name: Multiattack
  description: The dragon can use its Frightful Presence. It then makes two melee
    attacks, one with its bite and one with its claws.
  limit: 0
  expanded: 0
- name: Bite
  description: '{@atk mw} {@hit 11} to hit, reach 3 m., one target. {@h}17 ({@damage
    2d10 + 6}) piercing damage plus 3 ({@damage 1d6}) thunder damage.'
  limit: 0
  expanded: 0
- name: Claws
  description: '{@atk mw} {@hit 11} to hit, reach 3 m., one target. {@h}19 ({@damage
    3d8 + 6}) slashing damage.'
  limit: 0
  expanded: 0
- name: Tail
  description: '{@atk mw} {@hit 11} to hit, reach 3 m., one target. {@h}15 ({@damage
    2d8 + 6}) bludgeoning damage.'
  limit: 0
  expanded: 0
- name: Frightful Presence
  description: Each creature of the dragon's choice that is within 36 meters of the
    dragon and aware of it must succeed on a {@dc 17} Wisdom saving throw or become
    {@condition frightened} for 1 minute. A creature can repeat the saving throw at
    the end of each of its turns, ending the effect on itself on a success. If a creature's
    saving throw is successful or the effect ends for it, the creature is immune to
    the dragon's Frightful Presence for the next 24 hours.
  limit: 0
  expanded: 0
- name: Debilitating Breath ({@recharge 5-6})
  description: The dragon exhales a pulse of high-pitched, nearly inaudible sound
    in a 18-meter-cone. Each creature in that cone must make a {@dc 18} Constitution
    saving throw. On a failed save, the creature takes 45 ({@damage 13d6}) thunder
    damage and is {@condition incapacitated} until the end of its next turn. On a
    successful save, the creature takes half as much damage and isn't {@condition
    incapacitated}.
  limit: 1
  expanded: 0
reactions: []
variant:
- name: Customizing Dragons
  description: 'You can customize any dragon''s stat block to reflect the dragon''s
    unique character. Minor changes such as those below are easy to make and have
    no impact on a dragon''s challenge rating.<br/><b>Languages</b> Most dragons prefer
    to speak Draconic but learn Common for dealing with allies and minions. But given
    their high Intelligence and long life span, dragons can easily learn additional
    languages. You can add languages to a dragon''s stat block.<br/><b>Skills</b>
    Most dragons are proficient in the {@skill Perception} and {@skill Stealth} skills,
    and many dragons have additional skill proficiencies. As with languages, you can
    customize a dragon''s skill list (even doubling their proficiency bonus with certain
    skills) to reflect particular interests and activities. You can also give a dragon
    tool proficiencies, particularly if the dragon spends time in Humanoid form.<br/><b>Spells</b>
    {@note See the ''Variant: Dragons as Innate Spellcasters'' inset(s), below.}<br/><b>Other
    Traits and Actions</b> You can borrow traits and actions from other monsters to
    add unique flavor to a dragon. Consider these examples:<br/><ul><li><b>Change
    Shape</b> You can decide that a dragon acquires this action at a younger age than
    usual, particularly if you want to feature a dragon in Humanoid form in your campaign:<br/>The
    dragon magically polymorphs into a humanoid or beast that has a challenge rating
    no higher than its own, or back into its true form. It reverts to its true form
    if it dies. Any equipment it is wearing or carrying is absorbed or borne by the
    new form (the dragon''s choice).<br/>In a new form, the dragon retains its alignment,
    hit points, Hit Dice, ability to speak, proficiencies, Legendary Resistance, lair
    actions, and Intelligence, Wisdom, and Charisma scores, as well as this action.
    Its statistics and capabilities are otherwise replaced by those of the new form,
    except any class features or legendary actions of that form.</li><li><b>Flyby</b>
    The dragon is an agile flier, quick to fly out of enemies'' reach.<br/>The dragon
    doesn''t provoke an opportunity attack when it flies out of an enemy''s reach.</li><li><b>Mimicry</b>
    Impersonating characters or their allies could be a fun trick for a crafty dragon.<br/>The
    dragon can mimic any sounds it has heard, including voices. A creature that hears
    the sounds can tell they are imitations with a successful {@dc 12} Wisdom ({@skill
    Insight}) check.</li><li><b>Rejuvenation</b> You might decide that dragons in
    your campaign, being an essential part of the Material Plane, are nearly impossible
    to destroy. A dragon''s life essence might be preserved in the egg from which
    it first emerged, in its hoard, or in a cavernous hall at the center of the world,
    just as a lich''s essence is hidden in a phylactery.<br/>If it has an essence-preserving
    object, a destroyed dragon gains a new body in {@dice 1d10} days, regaining all
    its hit points and becoming active again. The new body appears within 1.5 meters
    of the object.</li><li><b>Special Senses</b> Most dragons have {@sense blindsight}
    and {@sense darkvision}. You might upgrade {@sense blindsight} to {@sense truesight},
    or you could give a dragon with a burrowing speed {@sense tremorsense}.</li><li><b>Tunneler</b>
    The dragon can burrow through solid rock at half its burrowing speed and leaves
    a tunnel in its wake.</li></ul>'
  limit: 0
  expanded: 0
legendary_actions_desc: The Adult Sapphire Dragon can take 3 legendary actions, choosing
  from the options below. Only one legendary action can be used at a time and only
  at the end of another creature's turn. The Adult Sapphire Dragon regains spent legendary
  actions at the start of its turn.
legendary_actions_limit: 3
legendary_actions_expanded: 0
legendary_actions:
- name: Tail Attack
  description: The dragon makes a tail attack.
  limit: 0
  expanded: 0
- name: Telekinetic Fling
  description: The dragon chooses a Small or smaller object that isn't being worn
    or carried that it can see within 18 meters of it, and magically hurls the object
    at a creature it can see within 18 meters of the object. The target must succeed
    on a {@dc 17} Dexterity saving throw or take 15 ({@damage 4d6}) bludgeoning damage.
  limit: 0
  expanded: 0
- name: Teleport (Costs 2 Actions)
  description: The dragon magically teleports to an unoccupied space it can see within
    9 meters of it.
  limit: 0
  expanded: 0
mythic_actions_desc: null
mythic_actions: []
lair_actions_desc: 'On initiative count 20 (losing initiative ties), the dragon can
  take one of the following lair actions; the dragon can''t take the same lair action
  two rounds in a row:'
lair_actions:
- name: Awesome Thunder
  description: A thunderous detonation of sound that can be heard up to 90 meters
    away surrounds one creature in the lair that the dragon can see. That creature
    must succeed on a DC 15 Constitution saving throw or take 13 ({@damage 3d8}) thunder
    damage and be {@condition stunned} until the end of its next turn.
  limit: 0
  expanded: 0
- name: Beguiling Whisper
  description: The dragon telepathically whispers to one creature within range of
    the dragon's telepathy. The creature must succeed on a DC 15 Wisdom saving throw
    or be {@condition charmed} by the dragon until initiative count 20 on the next
    round. A creature {@condition charmed} in this way obeys to the best of its ability
    any command the dragon issues that isn't directly harmful to the creature.
  limit: 0
  expanded: 0
- name: Stone Passage
  description: The dragon touches a section of stone up to 9 meters in any dimension.
    The dragon can shape the stone to open or close a passage through a wall, as long
    as the wall is less than 3 meters thick.
  limit: 0
  expanded: 0
regional_effects:
- name: Crystal Profusion
  description: Natural stone within 9.5 kilometers of the lair grows plentiful crystal
    formations and veins of sapphire gemstones, particularly underground.
  limit: 0
  expanded: 0
- name: Stony Sight
  description: Natural stone within 9.5 kilometers of the lair is a conduit for the
    dragon's psionic presence. As an action, the dragon can cast the {@spell clairvoyance}
    spell, requiring no spell components and targeting any natural stone formation
    in that region.
  limit: 0
  expanded: 0
- name: Telepathic Enhancement
  description: The dragon's psionic energy enhances the mental powers of other creatures.
    Any creature capable of telepathic communication has its telepathy range doubled
    while within 1.5 kilometers of the lair. This includes creatures with innate telepathy
    and magical telepathy such as the {@spell Rary's telepathic bond} spell.
  limit: 0
  expanded: 0
- name: Thriving Wildlife
  description: '{@creature Giant spider||Giant spiders} (a sapphire dragon''s favorite
    prey) are attracted to the area within 9.5 kilometers of the lair and settle there
    in large numbers.'
  limit: 0
  expanded: 0
- name: ''
  description: If the dragon dies, the population of giant spiders in the region returns
    to normal levels over the course of {@dice 1d10} days. The enhancement of telepathic
    abilities ends immediately. The existing abundance of crystals and sapphires remains,
    but new ones form at a normal rate.
  limit: 0
  expanded: 0
has_spellbook: true
known_spells: []
initiative: 0
conditions: []
visible: false
active: false
