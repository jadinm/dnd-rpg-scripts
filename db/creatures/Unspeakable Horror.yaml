name: Unspeakable Horror
alias: Unspeakable Horror
image_key: null
image_path: null
appearance: null
player_image_path: null
reference_image_path: bestiary/VRGR/Unspeakable Horror.jpg
source:
- Unspeakable Horror
- VRGR
note: ''
ac: 15
armor_desc: from natural armor, 17 from Aberrant Armor Only
hpmax: 140
hpmax_roll: 10d10 + 40
hp: 140
xp: 3900
other_xp: {}
size: H
type: monstrosity
alignment: unaligned
speed: 12 m.
strength: 21
dexterity: 13
constitution: 19
intelligence: 3
wisdom: 14
charisma: 17
saving_throws:
- Con +7
- Wis +5
abilities:
- Perception +5
damage_weaknesses: []
damage_resistances: []
damage_immunities: []
condition_immunities: []
senses:
- darkvision 18 m.
- Passive perception 15
languages: []
traits:
- name: Formed by the Mists
  description: 'When created, the horror''s body composition takes one of four forms:
    Aberrant Armor, Loathsome Limbs, Malleable Mass, or Oozing Organs. This form determines
    certain traits in this stat block.'
  limit: 0
  expanded: 0
- name: Amorphous (Malleable Mass Only)
  description: The horror can move through any opening at least 1 inch wide without
    squeezing.
  limit: 0
  expanded: 0
- name: Bile Body (Oozing Organs Only)
  description: Any creature that touches the horror or hits it with a melee attack
    takes 5 ({@damage 1d10}) acid damage.
  limit: 0
  expanded: 0
- name: Relentless Stride (Loathsome Limbs Only)
  description: The horror can move through the space of another creature. The first
    time on a turn that the horror enters a creature's space during this move, the
    creature must succeed on a {@dc 16} Strength saving throw or be knocked {@condition
    prone}.
  limit: 0
  expanded: 0
spells: []
actions:
- name: Multiattack
  description: The horror makes two Limbs attacks.
  limit: 0
  expanded: 0
- name: Limbs
  description: '{@atk mw} {@hit 8} to hit, reach 1.5 m., one target. {@h}21 ({@damage
    3d10 + 5}) bludgeoning damage.'
  limit: 0
  expanded: 0
- name: Hex Blast ({@recharge 5-6})
  description: The horror expels necrotic energy in a 9-meter cone. Each creature
    in that area must make a {@dc 15} Constitution saving throw, taking 45 ({@damage
    7d12}) necrotic damage on a failed save, or half as much damage on a successful
    one.
  limit: 1
  expanded: 0
reactions: []
variant:
- name: Customizing a Horror
  description: An unspeakable horror has one of four body compositions, determined
    by rolling on the Body Composition table. You can roll on the Limbs to customize
    it further, while results from the Hex Blast table replace that action in the
    stat block. If the results of multiple tables conflict, chose your preferred result.<br/>The
    results of these tables are meant to be broad, so feel free to describe the details
    of an unspeakable horror's form and the interplay between its parts however you
    desire. The more discordant and unexpected a horror's parts, the more unsettling
    it might be.<br/><table class='table table-hover'><caption>Body Composition</caption><thead><tr><th
    scope='col'>d4</th><th scope='col'>Body</th></tr></thead><tbody><tr><td>1</td><td><b>Aberrant
    Armor</b> The horror's body is armored in {@condition petrified} wood, alien crystal,
    rusted mechanisms, sculpted stone, or an exoskeleton.</td></tr><tr><td>2</td><td><b>Loathsome
    Limbs</b> The horror's body boasts spider like legs, many-jointed appendages,
    or thrashing tentacles.</td></tr><tr><td>3</td><td><b>Malleable Mass</b> The horror's
    body is composed of a clot of boneless flesh, shadowy tendrils, or mist.</td></tr><tr><td>4</td><td><b>Oozing
    Organs</b> The horror's body boasts exposed entrails, bloated parasites, or a
    gelatinous shroud, perhaps because it is inside out.</td></tr></table><br/><table
    class='table table-hover'><caption>Hex Blast</caption><thead><tr><th scope='col'>d4</th><th
    scope='col'>Hex</th></tr></thead><tbody><tr><td>1</td><td><b>Beguiling Hex ({@recharge
    5-6})</b> The horror expels a wave of mind-altering magic. Each creature within
    9 meters of the horror must make a DC 15 Wisdom saving throw, taking 33 ({@dice
    6d10}) psychic damage and being {@condition incapacitated} until the end of the
    creature's next turn on a failed save, or taking half as much damage on a successful
    one.</td></tr><tr><td>2</td><td><b>Bile Hex ({@recharge 5-6})</b> The horror expels
    acidic bile in a 18-meter line that is 1.5 meters wide. Each creature in that
    line must succeed on a DC 15 Dexterity saving throw or be covered in bile. A creature
    covered in bile takes 31 ({@dice 7d8}) acid damage at the start of each of its
    turns until it or another creature uses its action to scrape or wash off the bile
    that covers it.</td></tr><tr><td>3</td><td><b>Petrifying Hex ({@recharge 5-6})</b>
    The horror expels petrifying gas in a 9-meter cone. Each creature in that area
    must succeed on a DC 15 Constitution saving throw or take 14 ({@dice 4d6}) necrotic
    damage and be {@condition restrained} as it begins to turn to stone. A {@condition
    restrained} creature must repeat the saving throw at the end of its next turn.
    On a success, the effect ends on the target. On a failure, the target is {@condition
    petrified} until freed by the {@spell greater restoration} spell or other magic.</td></tr><tr><td>4</td><td><b>Reality-Stealing
    Hex ({@recharge 5-6})</b> The horror expels a wave of perception-distorting energy.
    Each creature within 9 meters of the horror must make a DC 15 Wisdom saving throw.
    On a failed save, the target takes 22 ({@dice 5d8}) psychic damage and is {@condition
    deafened} until the end of its next turn. If the saving throw fails by 5 or more,
    the target is also {@condition blinded} until the end of its next turn.</td></tr></table><br/><table
    class='table table-hover'><caption>Limbs</caption><thead><tr><th scope='col'>d4</th><th
    scope='col'>Attack</th></tr></thead><tbody><tr><td>1</td><td><b>Bone Blade</b>
    The horror's limb ends in a blade made of bone, which deals slashing damage instead
    of bludgeoning damage. In addition, it scores a critical hit on a roll of 19 or
    20 and rolls the damage dice of a crit three times, instead of twice.</td></tr><tr><td>2</td><td><b>Corrosive
    Pseudopod</b> The horror's limb attack deals an extra 9 ({@dice 2d8}) acid damage.</td></tr><tr><td>3</td><td><b>Grasping
    Tentacle</b> The horror's limb is a grasping tentacle. When the horror hits a
    creature with this limb, the creature is also {@condition grappled} (escape DC
    16). The limb can have only one creature {@condition grappled} at a time.</td></tr><tr><td>4</td><td><b>Poisonous
    Limb</b> The horror's limb deals piercing damage instead of bludgeoning damage.
    In addition, when the horror hits a creature with this limb, the creature must
    succeed on a DC 15 Constitution saving throw or take 7 ({@dice 2d6}) poison damage
    and be {@condition poisoned} until the end of its next turn.</td></tr></table>
  limit: 0
  expanded: 0
legendary_actions_desc: null
legendary_actions_limit: 3
legendary_actions_expanded: 0
legendary_actions: []
mythic_actions_desc: null
mythic_actions: []
lair_actions_desc: null
lair_actions: []
regional_effects: []
has_spellbook: false
known_spells: []
initiative: 0
conditions: []
visible: false
active: false
