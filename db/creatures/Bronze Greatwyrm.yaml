name: Bronze Greatwyrm
alias: Bronze Greatwyrm
image_key: null
image_path: null
appearance: null
player_image_path: null
reference_image_path: bestiary/MM/Bronze Dragon.jpg
source:
- Bronze Greatwyrm
- FTD
note: ''
ac: 22
armor_desc: from natural armor
hpmax: 841
hpmax_roll: 29d20 + 261
hp: 841
xp: 120000
other_xp: {}
size: G
type: dragon(metallic)
alignment: lawful good
speed: 18 m., burrow 18 m., fly 36 m., swim 18 m.
strength: 30
dexterity: 16
constitution: 29
intelligence: 21
wisdom: 22
charisma: 30
saving_throws:
- Dex +11
- Con +17
- Int +13
- Wis +14
- Cha +18
abilities:
- Insight +14
- Perception +22
- Persuasion +18
damage_weaknesses: []
damage_resistances: []
damage_immunities:
- lightning
condition_immunities:
- charmed
- frightened
- poisoned
senses:
- truesight 36 m.
- Passive perception 32
languages:
- Common
- Draconic
traits:
- name: Legendary Resistance (4/Day)
  description: If the greatwyrm fails a saving throw, it can choose to succeed instead.
  limit: 4
  expanded: 0
- name: Metallic Awakening (Recharges after a Short or Long Rest)
  description: If the greatwyrm would be reduced to 0 hit points, its current hit
    point total instead resets to 450 hit points, it recharges its Breath Weapon,
    and it regains any expended uses of Legendary Resistance. Additionally, the greatwyrm
    can now use the options in the 'Mythic Actions' section for 1 hour. Award a party
    an additional 120,000 XP (240,000 XP total) for defeating the greatwyrm after
    its Metallic Awakening activates.
  limit: 1
  expanded: 0
- name: Unusual Nature
  description: The greatwyrm doesn't require food or drink.
  limit: 0
  expanded: 0
spells: []
actions:
- name: Multiattack
  description: The greatwyrm makes one Bite attack and two Claw attacks.
  limit: 0
  expanded: 0
- name: Bite
  description: '{@atk mw} {@hit 18} to hit, reach 4.5 m., one target. {@h}21 ({@damage
    2d10 + 10}) piercing damage plus 13 ({@damage 2d12}) force damage.'
  limit: 0
  expanded: 0
- name: Claw
  description: '{@atk mw} {@hit 18} to hit, reach 3 m., one target. {@h}19 ({@damage
    2d8 + 10}) slashing damage. If the target is a Huge or smaller creature, it is
    {@condition grappled} (escape {@dc 20}) and is {@condition restrained} until this
    grapple ends. The greatwyrm can have only one creature {@condition grappled} in
    this way at a time.'
  limit: 0
  expanded: 0
- name: Tail
  description: '{@atk mw} {@hit 18} to hit, reach 6 m., one target. {@h}21 ({@damage
    2d10 + 10}) bludgeoning damage. If the target is a creature, it must succeed on
    a {@dc 26} Strength saving throw or be knocked {@condition prone}.'
  limit: 0
  expanded: 0
- name: Breath Weapon ({@recharge 5-6})
  description: The greatwyrm uses one of the following breath weapons:<br/><ul><li><b>Elemental
    Breath</b> The greatwyrm exhales elemental energy in a 90-meter cone. Each creature
    in that area must make a {@dc 25} Dexterity saving throw, taking 84 ({@damage
    13d12}) lightning damage on a failed save, or half as much damage on a successful
    one.</li><li><b>Sapping Breath</b> The greatwyrm exhales gas in a 90-meter cone.
    Each creature in that area must make a {@dc 25} Constitution saving throw. On
    a failed save, the creature falls {@condition unconscious} for 1 minute. On a
    successful save, the creature has disadvantage on attack rolls and saving throws
    until the end of the greatwyrm's next turn. An {@condition unconscious} creature
    can repeat the saving throw at the end of each of its turns, ending the effect
    on itself on a success.</li></ul>
  limit: 1
  expanded: 0
- name: Change Shape
  description: The greatwyrm magically transforms into any creature that is Medium
    or Small, while retaining its game statistics (other than its size). This transformation
    ends if the dragon is reduced to 0 hit points or uses its action to end it.
  limit: 0
  expanded: 0
reactions: []
variant:
- name: Customizing Dragons
  description: 'You can customize any dragon''s stat block to reflect the dragon''s
    unique character. Minor changes such as those below are easy to make and have
    no impact on a dragon''s challenge rating.<br/><b>Languages</b> Most dragons prefer
    to speak Draconic but learn Common for dealing with allies and minions. But given
    their high Intelligence and long life span, dragons can easily learn additional
    languages. You can add languages to a dragon''s stat block.<br/><b>Skills</b>
    Most dragons are proficient in the {@skill Perception} and {@skill Stealth} skills,
    and many dragons have additional skill proficiencies. As with languages, you can
    customize a dragon''s skill list (even doubling their proficiency bonus with certain
    skills) to reflect particular interests and activities. You can also give a dragon
    tool proficiencies, particularly if the dragon spends time in Humanoid form.<br/><b>Spells</b>
    {@note See the ''Variant: Dragons as Innate Spellcasters'' inset(s), below.}<br/><b>Other
    Traits and Actions</b> You can borrow traits and actions from other monsters to
    add unique flavor to a dragon. Consider these examples:<br/><ul><li><b>Change
    Shape</b> You can decide that a dragon acquires this action at a younger age than
    usual, particularly if you want to feature a dragon in Humanoid form in your campaign:<br/>The
    dragon magically polymorphs into a humanoid or beast that has a challenge rating
    no higher than its own, or back into its true form. It reverts to its true form
    if it dies. Any equipment it is wearing or carrying is absorbed or borne by the
    new form (the dragon''s choice).<br/>In a new form, the dragon retains its alignment,
    hit points, Hit Dice, ability to speak, proficiencies, Legendary Resistance, lair
    actions, and Intelligence, Wisdom, and Charisma scores, as well as this action.
    Its statistics and capabilities are otherwise replaced by those of the new form,
    except any class features or legendary actions of that form.</li><li><b>Flyby</b>
    The dragon is an agile flier, quick to fly out of enemies'' reach.<br/>The dragon
    doesn''t provoke an opportunity attack when it flies out of an enemy''s reach.</li><li><b>Mimicry</b>
    Impersonating characters or their allies could be a fun trick for a crafty dragon.<br/>The
    dragon can mimic any sounds it has heard, including voices. A creature that hears
    the sounds can tell they are imitations with a successful {@dc 18} Wisdom ({@skill
    Insight}) check.</li><li><b>Rejuvenation</b> You might decide that dragons in
    your campaign, being an essential part of the Material Plane, are nearly impossible
    to destroy. A dragon''s life essence might be preserved in the egg from which
    it first emerged, in its hoard, or in a cavernous hall at the center of the world,
    just as a lich''s essence is hidden in a phylactery.<br/>If it has an essence-preserving
    object, a destroyed dragon gains a new body in {@dice 1d10} days, regaining all
    its hit points and becoming active again. The new body appears within 1.5 meters
    of the object.</li><li><b>Special Senses</b> Most dragons have {@sense blindsight}
    and {@sense darkvision}. You might upgrade {@sense blindsight} to {@sense truesight},
    or you could give a dragon with a burrowing speed {@sense tremorsense}.</li><li><b>Tunneler</b>
    The dragon can burrow through solid rock at half its burrowing speed and leaves
    a tunnel in its wake.</li></ul>'
  limit: 0
  expanded: 0
legendary_actions_desc: The Bronze Greatwyrm can take 3 legendary actions, choosing
  from the options below. Only one legendary action can be used at a time and only
  at the end of another creature's turn. The Bronze Greatwyrm regains spent legendary
  actions at the start of its turn.
legendary_actions_limit: 3
legendary_actions_expanded: 0
legendary_actions:
- name: Attack
  description: The greatwyrm makes one Claw or Tail attack.
  limit: 0
  expanded: 0
- name: Wing Attack (Costs 2 Actions)
  description: The greatwyrm beats its wings. Each creature within 9 meters of it
    must succeed on a {@dc 26} Dexterity saving throw or take 17 ({@damage 2d6 + 10})
    bludgeoning damage and be knocked {@condition prone}. The greatwyrm can then fly
    up to half its flying speed.
  limit: 0
  expanded: 0
mythic_actions_desc: If the greatwyrm's Metallic Awakening trait has activated in
  the last hour, it can use the options below as legendary actions.
mythic_actions:
- name: Bite
  description: The greatwyrm makes one Bite attack.
  limit: 0
  expanded: 0
- name: Shattering Roar (Costs 2 Actions)
  description: The greatwyrm unleashes a magical roar. Each creature in a 36-meter-radius
    sphere centered on the greatwyrm must succeed on a {@dc 26} Constitution saving
    throw or take 19 ({@damage 3d12}) thunder damage and be {@condition incapacitated}
    until the end of its next turn.
  limit: 0
  expanded: 0
lair_actions_desc: null
lair_actions: []
regional_effects: []
has_spellbook: false
known_spells: []
initiative: 0
conditions: []
visible: false
active: false
