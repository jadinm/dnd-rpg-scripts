name: Burney the Barber
alias: Burney the Barber
image_key: null
image_path: null
appearance: null
player_image_path: null
reference_image_path: BGDIA/Burney the Barber.png
source:
- Burney the Barber
- BGDIA
note: ''
ac: 21
armor_desc: from natural armor
hpmax: 540
hpmax_roll: 20d20 + 140
hp: 540
xp: 33000
other_xp: {}
size: G
type: dragon
alignment: chaotic good
speed: 12 m., climb 12 m., fly 24 m.
strength: 27
dexterity: 12
constitution: 25
intelligence: 20
wisdom: 17
charisma: 19
saving_throws:
- Dex +8
- Con +14
- Wis +10
- Cha +11
abilities:
- Deception +11
- Perception +17
- Stealth +8
damage_weaknesses: []
damage_resistances: []
damage_immunities:
- acid
condition_immunities: []
senses:
- blindsight 18 m.
- darkvision 36 m.
- Passive perception 27
languages:
- Common
- Draconic
traits:
- name: Legendary Resistance (3/Day)
  description: If Burney fails a saving throw, it can choose to succeed instead.
  limit: 3
  expanded: 0
- name: Healer
  description: Burney has the benefits of the {@feat healer} feat as well as proficiency
    with both the {@item healer's kit|phb} and the {@item herbalism kit|phb}.
  limit: 0
  expanded: 0
- name: Bahamut's Blessings
  description: Burney has received several blessings in her service to Bahamut:<br/><ul><li>Unless
    Burney decided otherwise, once any creature less powerful than a deity has taken
    three steps from her, they can no longer remember her or having interacted with
    her specifically.</li><li>Burney is under the effect of a permanent {@spell mind
    blank} spell, and cannot be detected by magical or mundane means unless she wishes
    it. In exchange for this blessing, Burney can take no direct action against the
    denizens of the Nine Hells, though she can certainly enlist the help of those
    who can.</li><li>Burney always knows the location of the Wandering Emporium and
    can transport herself there as though by a {@spell word of recall} spell. This
    explains why Burney simply seems to appear amid the fully deployed marketplace
    each morning it is active to provide service and tell stories.</li><li>Once each
    day, when Burney so desires, she can instantly transport herself to the court
    of Bahamut via a powerful blessing akin to the {@spell plane shift} spell.</li></ul>
  limit: 0
  expanded: 0
spells: []
actions:
- name: Multiattack
  description: 'Burney can use its Frightful Presence. It then makes three attacks:
    one with its bite and two with its claws.'
  limit: 0
  expanded: 0
- name: Bite
  description: '{@atk mw} {@hit 15} to hit, reach 4.5 m., one target. {@h}19 ({@damage
    2d10 + 8}) piercing damage.'
  limit: 0
  expanded: 0
- name: Claw
  description: '{@atk mw} {@hit 15} to hit, reach 3 m., one target. {@h}15 ({@damage
    2d6 + 8}) slashing damage.'
  limit: 0
  expanded: 0
- name: Tail
  description: '{@atk mw} {@hit 15} to hit, reach 6 m., one target. {@h}17 ({@damage
    2d8 + 8}) bludgeoning damage.'
  limit: 0
  expanded: 0
- name: Frightful Presence
  description: Each creature of Burney's choice that is within 36 meters of Burney
    and aware of it must succeed on a {@dc 19} Wisdom saving throw or become {@condition
    frightened} for 1 minute. A creature can repeat the saving throw at the end of
    each of its turns, ending the effect on itself on a success. If a creature's saving
    throw is successful or the effect ends for it, the creature is immune to Burney's
    Frightful Presence for the next 24 hours.
  limit: 0
  expanded: 0
- name: Breath Weapons ({@recharge 5-6})
  description: Burney uses one of the following breath weapons.<br/><ul><li><b>Acid
    Breath</b> Burney exhales acid in a 27-meter line that is 3 meters wide. Each
    creature in that line must make a {@dc 22} Dexterity saving throw, taking 63 ({@damage
    14d8}) acid damage on a failed save, or half as much damage on a successful one.</li><li><b>Slowing
    Breath</b> Burney exhales gas in a 27-meter cone. Each creature in that area must
    succeed on a {@dc 22} Constitution saving throw. On a failed save, the creature
    can't use reactions, its speed is halved, and it can't make more than one attack
    on its turn. In addition, the creature can use either an action or a bonus action
    on its turn, but not both. These effects last for 1 minute. The creature can repeat
    the saving throw at the end of each of its turns, ending the effect on itself
    with a successful save.</li></ul>
  limit: 1
  expanded: 0
- name: Change Shape
  description: Burney magically polymorphs into a humanoid or beast that has a challenge
    rating no higher than its own, or back into its true form. It reverts to its true
    form if it dies. Any equipment it is wearing or carrying is absorbed or borne
    by the new form (Burney's choice).<br/>In a new form, Burney retains its alignment,
    hit points, Hit Dice, ability to speak, proficiencies, Legendary Resistance, lair
    actions, and Intelligence, Wisdom, and Charisma scores, as well as this action.
    Its statistics and capabilities are otherwise replaced by those of the new form,
    except any class features or legendary actions of that form.
  limit: 0
  expanded: 0
reactions: []
variant:
- name: Customizing Dragons
  description: 'You can customize any dragon''s stat block to reflect Burney''s unique
    character. Minor changes such as those below are easy to make and have no impact
    on a dragon''s challenge rating.<br/><b>Languages</b> Most dragons prefer to speak
    Draconic but learn Common for dealing with allies and minions. But given their
    high Intelligence and long life span, dragons can easily learn additional languages.
    You can add languages to a dragon''s stat block.<br/><b>Skills</b> Most dragons
    are proficient in the {@skill Perception} and {@skill Stealth} skills, and many
    dragons have additional skill proficiencies. As with languages, you can customize
    a dragon''s skill list (even doubling their proficiency bonus with certain skills)
    to reflect particular interests and activities. You can also give a dragon tool
    proficiencies, particularly if Burney spends time in Humanoid form.<br/><b>Spells</b>
    {@note See the ''Variant: Dragons as Innate Spellcasters'' inset(s), below.}<br/><b>Other
    Traits and Actions</b> You can borrow traits and actions from other monsters to
    add unique flavor to a dragon. Consider these examples:<br/><ul><li><b>Change
    Shape</b> You can decide that a dragon acquires this action at a younger age than
    usual, particularly if you want to feature a dragon in Humanoid form in your campaign:<br/>Burney
    magically polymorphs into a humanoid or beast that has a challenge rating no higher
    than its own, or back into its true form. It reverts to its true form if it dies.
    Any equipment it is wearing or carrying is absorbed or borne by the new form (Burney''s
    choice).<br/>In a new form, Burney retains its alignment, hit points, Hit Dice,
    ability to speak, proficiencies, Legendary Resistance, lair actions, and Intelligence,
    Wisdom, and Charisma scores, as well as this action. Its statistics and capabilities
    are otherwise replaced by those of the new form, except any class features or
    legendary actions of that form.</li><li><b>Flyby</b> Burney is an agile flier,
    quick to fly out of enemies'' reach.<br/>Burney doesn''t provoke an opportunity
    attack when it flies out of an enemy''s reach.</li><li><b>Mimicry</b> Impersonating
    characters or their allies could be a fun trick for a crafty dragon.<br/>Burney
    can mimic any sounds it has heard, including voices. A creature that hears the
    sounds can tell they are imitations with a successful {@dc 19} Wisdom ({@skill
    Insight}) check.</li><li><b>Rejuvenation</b> You might decide that dragons in
    your campaign, being an essential part of the Material Plane, are nearly impossible
    to destroy. A dragon''s life essence might be preserved in the egg from which
    it first emerged, in its hoard, or in a cavernous hall at the center of the world,
    just as a lich''s essence is hidden in a phylactery.<br/>If it has an essence-preserving
    object, a destroyed dragon gains a new body in {@dice 1d10} days, regaining all
    its hit points and becoming active again. The new body appears within 1.5 meters
    of the object.</li><li><b>Special Senses</b> Most dragons have {@sense blindsight}
    and {@sense darkvision}. You might upgrade {@sense blindsight} to {@sense truesight},
    or you could give a dragon with a burrowing speed {@sense tremorsense}.</li></ul>'
  limit: 0
  expanded: 0
legendary_actions_desc: Burney the Barber can take 3 legendary actions, choosing from
  the options below. Only one legendary action can be used at a time and only at the
  end of another creature's turn. Burney the Barber regains spent legendary actions
  at the start of its turn.
legendary_actions_limit: 3
legendary_actions_expanded: 0
legendary_actions:
- name: Detect
  description: Burney makes a Wisdom ({@skill Perception}) check.
  limit: 0
  expanded: 0
- name: Tail Attack
  description: Burney makes a tail attack.
  limit: 0
  expanded: 0
- name: Wing Attack (Costs 2 Actions)
  description: Burney beats its wings. Each creature within 4.5 meters of Burney must
    succeed on a {@dc 23} Dexterity saving throw or take 15 ({@damage 2d6 + 8}) bludgeoning
    damage and be knocked {@condition prone}. Burney can then fly up to half its flying
    speed.
  limit: 0
  expanded: 0
mythic_actions_desc: null
mythic_actions: []
lair_actions_desc: null
lair_actions: []
regional_effects: []
has_spellbook: false
known_spells: []
initiative: 0
conditions: []
visible: false
active: false
