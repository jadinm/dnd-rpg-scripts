name: Arasta
alias: Arasta
image_key: null
image_path: null
appearance: null
player_image_path: null
reference_image_path: bestiary/MOT/Arasta.jpg
source:
- Arasta
- MOT
note: ''
ac: 19
armor_desc: from natural armor
hpmax: 432
hpmax_roll: 24d12 + 144
hp: 432
xp: 33000
other_xp: {}
size: H
type: monstrosity
alignment: neutral evil
speed: 12 m., climb 12 m.
strength: 24
dexterity: 16
constitution: 23
intelligence: 15
wisdom: 22
charisma: 17
saving_throws:
- Dex +10
- Con +13
- Wis +13
abilities:
- Arcana +9
- Deception +10
- Intimidation +10
- Nature +9
- Perception +13
- Stealth +10
damage_weaknesses: []
damage_resistances:
- bludgeoning from nonmagical attacks
- piercing from nonmagical attacks
- slashing from nonmagical attacks
damage_immunities:
- acid
- poison
condition_immunities:
- poisoned
senses:
- blindsight 18 m.
- darkvision 36 m.
- Passive perception 23
languages:
- Celestial
- Common
- Sylvan
traits:
- name: Armor of Spiders (Mythic Trait; Recharges after a Short or Long Rest)
  description: If Arasta is reduced to 0 hit points, she doesn't die or fall {@condition
    unconscious}. Instead, she regains 200 hit points. In addition, Arasta's children
    immediately swarm over her body to protect her, granting her 100 temporary hit
    points.
  limit: 1
  expanded: 0
- name: Legendary Resistance (3/Day)
  description: If Arasta fails a saving throw, she can choose to succeed instead.
  limit: 3
  expanded: 0
- name: Magic Resistance
  description: Arasta has advantage on saving throws against spells and other magical
    effects.
  limit: 0
  expanded: 0
- name: Spider Climb
  description: Arasta can climb difficult surfaces, including upside down on ceilings,
    without needing to make an ability check.
  limit: 0
  expanded: 0
- name: Web Walker
  description: Arasta ignores movement restrictions caused by webbing.
  limit: 0
  expanded: 0
spells: []
actions:
- name: Multiattack
  description: 'Arasta makes three attacks: one with her bite and two with her claws.'
  limit: 0
  expanded: 0
- name: Bite
  description: '{@atk mw} {@hit 14} to hit, reach 1.5 m., one creature. {@h}20 ({@damage
    3d8 + 7}) piercing damage, and the target must make a {@dc 21} Constitution saving
    throw, taking 32 ({@damage 5d12}) poison damage on a failed save, or half as much
    damage on a successful one. If the damage reduces the target to 0 hit points,
    the target is stable but {@condition poisoned} for 1 hour, even after regaining
    hit points, and is {@condition paralyzed} while {@condition poisoned} in this
    way.'
  limit: 0
  expanded: 0
- name: Claws
  description: '{@atk mw} {@hit 14} to hit, reach 1.5 m., one target. {@h}17 ({@damage
    3d6 + 7}) slashing damage.'
  limit: 0
  expanded: 0
- name: Web of Hair ({@recharge 4-6})
  description: Arasta unleashes her hair in the form of webbing that fills a 9-meter
    cube next to her. The web is difficult terrain, its area is lightly obscured,
    and it lasts for 1 minute. Any creature that moves into the web or that starts
    its turn there must make a {@dc 21} Dexterity saving throw. On a failed save,
    the creature is {@condition restrained} while in the web. A creature can use an
    action to make a {@dc 21} Strength check. On a success, it can free itself or
    a creature within 1.5 meters of it that is {@condition restrained} by the web.
    This webbing is immune to all damage except magical fire. A 1.5-meter cube of
    the web is destroyed if it takes at least 20 fire damage from a spell or other
    magical source on a single turn.
  limit: 1
  expanded: 0
reactions: []
variant: []
legendary_actions_desc: The Arasta can take 3 legendary actions, choosing from the
  options below. Only one legendary action can be used at a time and only at the end
  of another creature's turn. The Arasta regains spent legendary actions at the start
  of its turn.
legendary_actions_limit: 3
legendary_actions_expanded: 0
legendary_actions:
- name: Claws
  description: Arasta makes one attack with her claws.
  limit: 0
  expanded: 0
- name: Swarm (Costs 2 Actions)
  description: Arasta causes two {@creature swarm of spiders||swarms of spiders} to
    appear in unoccupied spaces within 1.5 meters of her.
  limit: 0
  expanded: 0
- name: Toxic Web (Costs 3 Actions)
  description: Each creature {@condition restrained} by Arasta's Web of Hair takes
    18 ({@damage 4d8}) poison damage.
  limit: 0
  expanded: 0
mythic_actions_desc: If Arasta's mythic trait is active, she can use the options below
  as legendary actions, as long as she has temporary hit points from her Armor of
  Spiders.
mythic_actions:
- name: Swipe
  description: Arasta makes two attacks with her claws.
  limit: 0
  expanded: 0
- name: Web of Hair (Costs 2 Actions)
  description: Arasta recharges Web of Hair and uses it.
  limit: 0
  expanded: 0
- name: Nyx Weave (Costs 2 Actions)
  description: Each creature {@condition restrained} by Arasta's Web of Hair must
    succeed on a {@dc 21} Constitution saving throw, or the creature takes 26 ({@damage
    4d12}) force damage and any spell of 6th level or lower on it ends.
  limit: 0
  expanded: 0
lair_actions_desc: On initiative count 20 (losing initiative ties), Arasta can take
  a lair action to cause one of the following effects. She can't use the same effect
  two rounds in a row.
lair_actions:
- name: ''
  description: <li>Arasta learns about any creature touching her webs. Each creature
    {@condition restrained} by a web or Arasta's Web of Hair must make a {@dc 21}
    Intelligence saving throw. On a failed save, Arasta gains knowledge of a creature's
    name, race, where they consider home, and what brought them to her web.</li>
  limit: 0
  expanded: 0
- name: ''
  description: <li>Arasta casts the {@spell giant insect} spell (spiders only). It
    lasts until she uses this lair action again or until she dies.</li>
  limit: 0
  expanded: 0
regional_effects:
- name: ''
  description: <li>Spiders and insects within 1.5 kilometers of Arasta's lair serve
    as her eyes and ears. Birds and other flying creatures are absent from the skies
    and occasionally found trapped in webs.</li>
  limit: 0
  expanded: 0
- name: ''
  description: <li>Within 1.5 kilometers of Arasta's lair, webs fill all 3-meter cubes
    of open space, so long as the webs can be anchored between two solid masses (such
    as walls or trees). The webs are flammable. Any webs exposed to fire burn away
    in 1 round. Any destroyed webs are magically repaired at the next dawn.</li>
  limit: 0
  expanded: 0
- name: ''
  description: If Arasta dies, the spiders and insects lose their supernatural link
    to her. The webs remain, but they dissolve within {@dice 1d10} days.
  limit: 0
  expanded: 0
has_spellbook: false
known_spells: []
initiative: 0
conditions: []
visible: false
active: false
