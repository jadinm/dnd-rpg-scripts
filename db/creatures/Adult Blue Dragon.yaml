name: Adult Blue Dragon
alias: Adult Blue Dragon
image_key: null
image_path: null
appearance: null
player_image_path: null
reference_image_path: bestiary/MM/Blue Dragon.jpg
source:
- Adult Blue Dragon
- MM
note: ''
ac: 19
armor_desc: from natural armor
hpmax: 324
hpmax_roll: 18d12 + 108
hp: 324
xp: 15000
other_xp: {}
size: H
type: dragon
alignment: lawful evil
speed: 12 m., burrow 9 m., fly 24 m.
strength: 25
dexterity: 10
constitution: 23
intelligence: 16
wisdom: 15
charisma: 19
saving_throws:
- Dex +5
- Con +11
- Wis +7
- Cha +9
abilities:
- Perception +12
- Stealth +5
damage_weaknesses: []
damage_resistances: []
damage_immunities:
- lightning
condition_immunities: []
senses:
- blindsight 18 m.
- darkvision 36 m.
- Passive perception 22
languages:
- Common
- Draconic
traits:
- name: Legendary Resistance (3/Day)
  description: If the dragon fails a saving throw, it can choose to succeed instead.
  limit: 3
  expanded: 0
spells: []
actions:
- name: Multiattack
  description: 'The dragon can use its Frightful Presence. It then makes three attacks:
    one with its bite and two with its claws.'
  limit: 0
  expanded: 0
- name: Bite
  description: '{@atk mw} {@hit 12} to hit, reach 3 m., one target. {@h}18 ({@damage
    2d10 + 7}) piercing damage plus 5 ({@damage 1d10}) lightning damage.'
  limit: 0
  expanded: 0
- name: Claw
  description: '{@atk mw} {@hit 12} to hit, reach 1.5 m., one target. {@h}14 ({@damage
    2d6 + 7}) slashing damage.'
  limit: 0
  expanded: 0
- name: Tail
  description: '{@atk mw} {@hit 12} to hit, reach 4.5 m., one target. {@h}16 ({@damage
    2d8 + 7}) bludgeoning damage.'
  limit: 0
  expanded: 0
- name: Frightful Presence
  description: Each creature of the dragon's choice that is within 36 meters of the
    dragon and aware of it must succeed on a {@dc 17} Wisdom saving throw or become
    {@condition frightened} for 1 minute. A creature can repeat the saving throw at
    the end of each of its turns, ending the effect on itself on a success. If a creature's
    saving throw is successful or the effect ends for it, the creature is immune to
    the dragon's Frightful Presence for the next 24 hours.
  limit: 0
  expanded: 0
- name: Lightning Breath ({@recharge 5-6})
  description: The dragon exhales lightning in a 27-meter line that is 1.5 meters
    wide. Each creature in that line must make a {@dc 19} Dexterity saving throw,
    taking 66 ({@damage 12d10}) lightning damage on a failed save, or half as much
    damage on a successful one.
  limit: 1
  expanded: 0
reactions: []
variant:
- name: Customizing Dragons
  description: 'You can customize any dragon''s stat block to reflect the dragon''s
    unique character. Minor changes such as those below are easy to make and have
    no impact on a dragon''s challenge rating.<br/><b>Languages</b> Most dragons prefer
    to speak Draconic but learn Common for dealing with allies and minions. But given
    their high Intelligence and long life span, dragons can easily learn additional
    languages. You can add languages to a dragon''s stat block.<br/><b>Skills</b>
    Most dragons are proficient in the {@skill Perception} and {@skill Stealth} skills,
    and many dragons have additional skill proficiencies. As with languages, you can
    customize a dragon''s skill list (even doubling their proficiency bonus with certain
    skills) to reflect particular interests and activities. You can also give a dragon
    tool proficiencies, particularly if the dragon spends time in Humanoid form.<br/><b>Spells</b>
    {@note See the ''Variant: Dragons as Innate Spellcasters'' inset(s), below.}<br/><b>Other
    Traits and Actions</b> You can borrow traits and actions from other monsters to
    add unique flavor to a dragon. Consider these examples:<br/><ul><li><b>Flyby</b>
    The dragon is an agile flier, quick to fly out of enemies'' reach.<br/>The dragon
    doesn''t provoke an opportunity attack when it flies out of an enemy''s reach.</li><li><b>Mimicry</b>
    Impersonating characters or their allies could be a fun trick for a crafty dragon.<br/>The
    dragon can mimic any sounds it has heard, including voices. A creature that hears
    the sounds can tell they are imitations with a successful {@dc 12} Wisdom ({@skill
    Insight}) check.</li><li><b>Rejuvenation</b> You might decide that dragons in
    your campaign, being an essential part of the Material Plane, are nearly impossible
    to destroy. A dragon''s life essence might be preserved in the egg from which
    it first emerged, in its hoard, or in a cavernous hall at the center of the world,
    just as a lich''s essence is hidden in a phylactery.<br/>If it has an essence-preserving
    object, a destroyed dragon gains a new body in {@dice 1d10} days, regaining all
    its hit points and becoming active again. The new body appears within 1.5 meters
    of the object.</li><li><b>Special Senses</b> Most dragons have {@sense blindsight}
    and {@sense darkvision}. You might upgrade {@sense blindsight} to {@sense truesight},
    or you could give a dragon with a burrowing speed {@sense tremorsense}.</li><li><b>Tunneler</b>
    The dragon can burrow through solid rock at half its burrowing speed and leaves
    a tunnel in its wake.</li></ul>'
  limit: 0
  expanded: 0
legendary_actions_desc: The Adult Blue Dragon can take 3 legendary actions, choosing
  from the options below. Only one legendary action can be used at a time and only
  at the end of another creature's turn. The Adult Blue Dragon regains spent legendary
  actions at the start of its turn.
legendary_actions_limit: 3
legendary_actions_expanded: 0
legendary_actions:
- name: Detect
  description: The dragon makes a Wisdom ({@skill Perception}) check.
  limit: 0
  expanded: 0
- name: Tail Attack
  description: The dragon makes a tail attack.
  limit: 0
  expanded: 0
- name: Wing Attack (Costs 2 Actions)
  description: The dragon beats its wings. Each creature within 3 meters of the dragon
    must succeed on a {@dc 20} Dexterity saving throw or take 14 ({@damage 2d6 + 7})
    bludgeoning damage and be knocked {@condition prone}. The dragon can then fly
    up to half its flying speed.
  limit: 0
  expanded: 0
mythic_actions_desc: null
mythic_actions: []
lair_actions_desc: 'On initiative count 20 (losing initiative ties), the dragon takes
  a lair action to cause one of the following effects; the dragon can''t use the same
  effect two rounds in a row:'
lair_actions:
- name: ''
  description: <li>Part of the ceiling collapses above one creature that the dragon
    can see within 36 meters of it. The creature must succeed on a {@dc 15} Dexterity
    saving throw or take 10 ({@damage 3d6}) bludgeoning damage and be knocked {@condition
    prone} and buried. The buried target is {@condition restrained} and unable to
    breathe or stand up. A creature can take an action to make a {@dc 10} Strength
    check, ending the buried state on a success.</li>
  limit: 0
  expanded: 0
- name: ''
  description: <li>A cloud of sand swirls about in a 6-meter-radius sphere centered
    on a point the dragon can see within 36 meters of it. The cloud spreads around
    corners. Each creature in the cloud must succeed on a {@dc 15} Constitution saving
    throw or be {@condition blinded} for 1 minute. A creature can repeat the saving
    throw at the end of each of its turns, ending the effect on itself on a success.</li>
  limit: 0
  expanded: 0
- name: ''
  description: <li>Lightning arcs, forming a 1.5-meter-wide line between two of the
    lair's solid surfaces that the dragon can see. They must be within 36 meters of
    the dragon and 36 meters of each other. Each creature in that line must succeed
    on a {@dc 15} Dexterity saving throw or take 10 ({@damage 3d6}) lightning damage.</li>
  limit: 0
  expanded: 0
- name: Additional Lair Actions
  description: 'At your discretion, a legendary ({@creature Adult Blue Dragon||adult}
    or {@creature Ancient Blue Dragon||ancient}) blue dragon can use one or both of
    the following additional lair actions while in its lair:'
  limit: 0
  expanded: 0
- name: Misleading Mirage
  description: Until initiative count 20 on the next round, at each intersection or
    branching passage in the lair, a creature other than the dragon has a {@chance
    50|50 percent} chance of going in a different direction from the direction it
    intends.
  limit: 0
  expanded: 0
- name: Sudden Sinkhole
  description: The dragon chooses a point on the ground that it can see within 36
    meters of it. A 1.5-meter-radius, 6-meter-deep pit forms centered on that point.
    A creature on the ground above where the pit formed must make a DC 15 Dexterity
    saving throw. On a failed save, a creature falls to the bottom of the pit, taking
    7 ({@damage 2d6}) bludgeoning damage and landing {@condition prone}. On a successful
    save, a creature moves to the nearest unoccupied space instead of falling in the
    pit.
  limit: 0
  expanded: 0
regional_effects:
- name: ''
  description: <li>Thunderstorms rage within 9.5 kilometers of the lair.</li>
  limit: 0
  expanded: 0
- name: ''
  description: <li>Dust devils scour the land within 9.5 kilometers of the lair. A
    dust devil has the statistics of an air elemental, but it can't fly, has a speed
    of 15 meters, and has an Intelligence and Charisma of 1 (-5).</li>
  limit: 0
  expanded: 0
- name: ''
  description: <li>Hidden sinkholes form in and around the dragon's lair. A sinkhole
    can be spotted from a safe distance with a successful {@dc 20} Wisdom ({@skill
    Perception}) check. Otherwise, the first creature to step on the thin crust covering
    the sinkhole must succeed on a {@dc 15} Dexterity saving throw or fall {@dice
    1d6 × 10} feet into the sinkhole.</li>
  limit: 0
  expanded: 0
- name: ''
  description: If the dragon dies, the dust devils disappear immediately, and the
    thunderstorms abate within {@dice 1d10} days. Any sinkholes remain where they
    are.
  limit: 0
  expanded: 0
- name: Additional Regional Effects
  description: 'Any of these effects might appear in the area around a blue dragon''s
    lair, in addition to or instead of the effects described in the {@i Monster Manual}:'
  limit: 0
  expanded: 0
- name: Blue Luster
  description: Creatures that spend a year within 1.5 kilometers of the dragon's lair
    find blue objects fascinating and feel compelled to acquire them at every opportunity.
  limit: 0
  expanded: 0
- name: Mirage Terrain
  description: The area immediately surrounding the lair appears to be a lush oasis.
    A creature carefully examining the illusion can attempt a DC 15 Intelligence ({@skill
    Investigation}) check to disbelieve it. A creature who disbelieves the illusion
    sees it as a vague image superimposed on the underlying terrain.
  limit: 0
  expanded: 0
- name: Sandstorm
  description: A sandstorm blows constantly within 1.5 kilometers of the dragon's
    lair.
  limit: 0
  expanded: 0
has_spellbook: false
known_spells: []
initiative: 0
conditions: []
visible: false
active: false
