import os
import sys
import json

import yaml

from model import Spell
from utils import YAML_ARGS


def download_spells(spell_folder: str):
    """Extract the list of all the spells from the 5e.tools DB"""

    with open(os.path.join(spell_folder, "index.json")) as index_obj:
        spell_files = json.load(index_obj).values()

    spells = {}
    for spell_file in spell_files:
        with open(os.path.join(spell_folder, spell_file)) as spell_file_obj:
            for spell_desc in json.load(spell_file_obj)["spell"]:
                spells[f"{{@spell {spell_desc['name'].lower()}|{spell_desc['source'].lower()}}}"] = {
                    "name": spell_desc["name"],
                    "source": spell_desc["source"],
                    "level": spell_desc["level"],
                    "classes": [dnd_class["name"]
                                for dnd_class in spell_desc.get("classes", {}).get("fromClassList", [])],
                }

    with open(Spell.SPELL_DB, "w") as file_obj:
        yaml.dump(spells, file_obj, **YAML_ARGS)


if __name__ == "__main__":
    if len(sys.argv) > 0:
        download_spells(sys.argv[1])
