"""Extract creatures from 5e tools JSON file"""
import copy
import datetime
import json
import os
import re
import sys
from typing import Union, Dict, List, Optional, Any, FrozenSet, Tuple

import yaml

from db.extract_5e_tools_copy import merge_copy_with_source, trait_list
from db.extract_5e_tools_evolution import get_spell_level_variants, get_proficiency_bonus_variants
from db.query import CREATURES_DB
from model import Creature
from utils import YAML_ARGS

commoner = {
    "name": "Commoner", "source": "MM", "page": 0, "srd": True, "size": "M",
    "type": {"type": "humanoid", "tags": ["any race"]}, "alignment": ["A"],
    "ac": [{"ac": 10}], "hp": {"average": 9, "formula": "2d8"},
    "speed": {"walk": 30}, "str": 10, "dex": 10, "con": 10, "int": 10, "wis": 10, "cha": 10, "passive": 10,
    "languages": ["any one language (usually Common)"], "cr": "0",
    "action": [{"name": "Club",
                "entries": ["{@atk mw} {@hit 2} to hit, reach 5 ft.,"
                            " one target. {@h}2 ({@damage 1d4}) bludgeoning damage."]
                }],
    "hasToken": True, "soundClip": {"type": "internal", "path": "bestiary/commoner.mp3"},
    "languageTags": ["X"], "damageTags": ["B"], "miscTags": ["MW"], "hasFluff": True
}

regex_feet = re.compile(r"(\d+(?:\.\d+)?) (?:(feets?)|(ft\.))")
regex_foot = re.compile(r"(\d+)-(?:foot|feet)")
regex_mile = re.compile(r"(\d+(?:\.\d+)?) miles?")


def feet_to_meters(feet: float) -> Union[float, int]:
    # Round to the nearest 0.5
    meters = round(feet / 5 * 1.5 * 2) / 2
    return int(meters) if meters == int(meters) else meters


def replace_feet_to_meters(match: re.Match) -> str:
    meters = feet_to_meters(float(match.group(1)))
    unit = "meters" if match.group(2) is not None else "m."
    return f"{meters} {unit}"


def replace_foot_to_meters(match: re.Match) -> str:
    meters = feet_to_meters(float(match.group(1)))
    return f"{meters}-meter"


def replace_mile_to_kilometers(match: re.Match) -> str:
    # Round to the nearest 0.5
    km = round(float(match.group(1)) * 1.6 * 2) / 2
    km = int(km) if km == int(km) else km
    return f"{km} kilometers"


def imperial_to_international_metrics(description: Optional[str]) -> Optional[str]:
    if description is None:
        return None
    tmp = regex_feet.sub(replace_feet_to_meters, description)
    tmp = regex_foot.sub(replace_foot_to_meters, tmp)
    tmp = tmp.replace("\"", "'")
    return regex_mile.sub(replace_mile_to_kilometers, tmp)


def creature_ac_5e_tools(ac_list: List[Union[int, Dict[str, Union[int, str]]]]) -> int:
    if isinstance(ac_list[0], dict):
        return ac_list[0]["ac"]
    return ac_list[0]


def creature_armor_desc_5e_tools(ac_list: List[Union[int, Dict[str, Union[int, str]]]]) -> Optional[str]:
    descriptions = []
    if isinstance(ac_list[0], dict):
        if ac_list[0].get("from") is not None:
            descriptions.append(imperial_to_international_metrics("from " + ", ".join(ac_list[0].get("from", []))))
        if ac_list[0].get("condition") is not None:
            descriptions.append(imperial_to_international_metrics(ac_list[0].get("condition", "")))
    for ac in ac_list[1:]:
        description = ""
        if isinstance(ac, dict):
            description += str(ac.get("ac"))
            if ac.get("from") is not None:
                description += " from " + ", ".join(ac.get("from", []))
            if ac.get("condition") is not None:
                description += " " + ac.get("condition", "")
        else:
            description += ac
        descriptions.append(imperial_to_international_metrics(description))

    return ", ".join(descriptions) if len(descriptions) > 0 else None


def creature_cr_5e_tools(cr: Union[str, Dict]) -> Tuple[int, Dict[str, int]]:
    if isinstance(cr, dict):
        # Handle the case with more than one CR: coven or lair
        return Creature.xp_from_challenge(cr.get("cr", "0")), \
               {key: Creature.xp_from_challenge(value) for key, value in cr.items() if key != "cr"}
    return Creature.xp_from_challenge(cr) if cr != "Unknown" else 0, {}


def parse_5e_tools_tags(tags: List[Union[str, Dict[str, str]]]) -> str:
    text = []
    for tag in tags:
        if isinstance(tag, str):
            text.append(tag)
        elif isinstance(tag, dict):
            text.append(" ".join([tag.get("prefix", ""), tag.get("tag", "")]).strip())
    return "(" + ", ".join(text) + ")" if len(text) > 0 else ""


def creature_type_5e_tools(creature_type: Union[str, Dict]) -> str:
    if isinstance(creature_type, dict):
        creature_type = creature_type["type"] + parse_5e_tools_tags(creature_type.get("tags", []))
    return creature_type


alignment = {
    "U": "unaligned",
    "A": "any alignment",
    "N": "neutral",
    "NX": "neutral",
    "NY": "neutral",
    "E": "evil",
    "G": "good",
    "C": "chaotic",
    "L": "lawful"
}

alignment_sets = {
    frozenset({'NX', 'N', 'NY'}): "any neutral alignment",
    frozenset({"C", "G", "NY", "E"}): "any chaotic alignment",
    frozenset({"L", "G", "NY", "E"}): "any lawful alignment",
    frozenset({"L", "NX", "C", "E"}): "any evil alignment",
    frozenset({"L", "NX", "C", "G"}): "any good alignment",
    frozenset({"L", "NX", "C", "NY", "E"}): "any non-good alignment",
    frozenset({"L", "NX", "C", "G", "NY"}): "any non-evil alignment",
    frozenset({"NX", "C", "G", "NY", "E"}): "any non-lawful alignment",
    frozenset({"L", "NX", "G", "NY", "E"}): "any non-chaotic alignment",
}


def creature_alignment_5e_tools(creature_alignment: List[Union[Dict[str, List[str]], str]]) -> str:
    if isinstance(creature_alignment[0], str):
        # Only one alignment description, make it uniform
        creature_alignment = [{"alignment": creature_alignment}]

    alignment_text = []
    for alignment_option in creature_alignment:
        if len(alignment_option.get("alignment", [0, 0, 0])) <= 2:
            # Unaligned, neutral or any classic combination like chaotic good
            alignment_text.append(" ".join([alignment[axis] for axis in alignment_option["alignment"]]))
        elif alignment_option.get("alignment"):
            # Particular combination of possible alignments
            alignment_text.append(alignment_sets[frozenset(alignment_option["alignment"])])
        if alignment_option.get("special"):
            if len(alignment_text) == 0:
                alignment_text.append(alignment_option["special"])
            else:
                alignment_text[-1] += " (" + alignment_option["special"] + ")"

    return " or ".join(alignment_text)


def speed_value_with_conditions(speed_value: Union[int, Dict[str, Union[int, str]]]) -> str:
    if isinstance(speed_value, int):
        return f"{feet_to_meters(speed_value)} m."
    return f"{feet_to_meters(speed_value['number'])} m. {imperial_to_international_metrics(speed_value['condition'])}"


def creature_speed_5e_tools(creature_speed: Union[int, Dict[str, int]]) -> str:
    speed_text = []
    if isinstance(creature_speed, int):
        creature_speed = {"walk": creature_speed}

    for means, speed in creature_speed.items():
        if means == "walk":
            speed_text = [speed_value_with_conditions(speed)] + speed_text
        elif means == "choose":
            raise ValueError(f"[E] A choice of creature speed: {speed}")
        elif means == "alternate":
            for alt_means, alt_speeds in speed.items():
                for alt_speed in alt_speeds:
                    speed_text.append(f"{alt_means} {speed_value_with_conditions(alt_speed)}")
        else:
            speed_text.append(f"{means} {speed_value_with_conditions(speed)}")
    return ", ".join(speed_text)


def creature_skills_5e_tools(skills: Dict[str, Union[str, Dict[str, Dict[str, str]]]]) -> List[str]:
    filtered_skills = []
    for skill, bonus in skills.items():
        if isinstance(bonus, str):
            filtered_skills.append(skill[0].upper() + skill[1:] + " " + bonus)
        elif isinstance(bonus, list):
            # Instead of leaving a choice, put everything (that's funnier for NPCs anyway)
            for spec in bonus:
                if list(spec.keys()) != ["oneOf"]:
                    raise ValueError(f"A skill item in the list has a key that is not 'oneOf'")
                for spec_skill, spec_bonus in spec["oneOf"].items():
                    filtered_skills.append(spec_skill[0].upper() + spec_skill[1:] + " " + spec_bonus)
        else:
            raise ValueError(f"A skill is not a string or a list: {skill}:{bonus}")
    return filtered_skills


def creature_damage_types_5e_tools(key: str, damage_types: List[Union[str, Dict[str, Union[List, str]]]], prefix="") \
        -> List[str]:
    parsed_damage_types = []
    if damage_types is None:
        damage_types = []
    for damage_type in damage_types:
        if isinstance(damage_type, str):
            parsed_damage_types.append(damage_type.replace("\"", "'"))
            if prefix:
                parsed_damage_types[-1] = prefix + parsed_damage_types[-1]
        else:
            if "special" in damage_type:
                parsed_damage_types.append(f"{damage_type['special']}")
            else:
                for d in damage_type.get(key, []):
                    if isinstance(d, str):
                        parsed_damage_types.append((damage_type.get("preNote", prefix) + " " + d
                                                    + " " + damage_type.get("note", "")).strip().replace("\"", "'"))
                    else:  # Recursion
                        parsed_damage_types += creature_damage_types_5e_tools(key, [d], damage_type.get("preNote", ""))
    return parsed_damage_types


def recharge_replace(match: re.Match) -> str:
    number = match.group(1)
    if not number:
        number = "6"
    else:
        number += "-6"
    return f"({{@recharge {number}}})"


regex_ordered_item = re.compile(r"^\d+\. ?(.+)$")
recharge_regex = re.compile(r"{@recharge ?(\d)?(?:\|m)?}")


def creature_5e_description(title: Optional[str], data: Union[str, List, Dict], spells: List, bullets=False,
                            break_title=False) -> str:
    if title and break_title:
        title = f"<strong><em>{re.sub(recharge_regex, recharge_replace, title)}</em></strong>.<br/>"
    elif title:
        title = f"<b>{re.sub(recharge_regex, recharge_replace, title)}</b> "
    else:
        title = ""
    if isinstance(data, str):
        return title + imperial_to_international_metrics(re.sub(recharge_regex, recharge_replace, data))
    elif isinstance(data, list):
        if len(data) > 1:
            tag = "ul"
            if isinstance(data[0], str):
                match = regex_ordered_item.match(data[0])
                if match:  # Ordered list because the first element starts with "1. "
                    tag = "ol"
            description = []
            for x in data:
                if tag == "ol" and isinstance(x, str):
                    match = regex_ordered_item.match(x)
                    if match:  # Ordered list because the first element starts with "1. "
                        x = match[1]
                description.append(creature_5e_description(None, x, spells))
            if bullets:
                return f"{title}<{tag}><li>" + "</li><li>".join(description) + f"</li></{tag}>"
            return title + "<br/>".join(description)
        return title + "".join([creature_5e_description(None, x, spells) for x in data])
    elif data["type"] == "entries":
        return title + creature_5e_description(data.get("name"), data["entries"], spells)
    elif data["type"] == "list":
        return title + creature_5e_description(data.get("name"), data["items"], spells, bullets=True)
    elif data["type"] == "item":
        if "entries" in data:
            return title + creature_5e_description(data.get('name'), data['entries'], spells)
        return f"{title}{creature_5e_description(data.get('name'), data['entry'], spells)}"
    elif data["type"] == "inline":
        description = ""
        for item in data.get("entries", []):
            if isinstance(item, str):
                description += item
            elif item.get("type") == "link":
                suffix = item.get("href", {}).get("hash", "")
                if suffix:
                    suffix = "|" + suffix
                description += "{@url " + item.get("href", {}).get("path") + suffix + "|" + item.get("text", "") + "}"
            else:
                raise ValueError("Cannot parse action inline sub-entry type", item, data)
        return title + creature_5e_description("", description, spells)
    elif data["type"] == "spellcasting":
        spells.extend(creature_spells_5e_tools([data]))
        data["headerEntries"][-1] = data["headerEntries"][-1][:-1] + " (see below)"  # replace ":" with " (see below)"
        return title + imperial_to_international_metrics("<br/>".join(data["headerEntries"]
                                                                      + data.get("footerEntries", [])))

    elif data["type"] == "table":
        caption = f'''<caption>{data["caption"]}</caption>''' if "caption" in data else ""
        headers = f'''<thead><tr><th scope='col'>{"</th><th scope='col'>".join(data["colLabels"])}</th></tr></thead>'''
        body = '<tbody>'
        for row in data["rows"]:
            row = map(lambda y: creature_5e_description(None, y, spells), row)
            body += f'''<tr><td>{"</td><td>".join(row)}</td></tr>'''
        return title + f'''<table class='table table-hover'>{caption}{headers}{body}</table>'''
    elif data["type"] == "variantInner":
        return title + creature_5e_description(data.get("name"), data["entries"], spells, bullets=True,
                                               break_title=True)
    elif data["type"] == "variantSub":
        return title + creature_5e_description(data.get("name"), data["entries"], spells, bullets=True)
    else:
        raise ValueError("Cannot parse entry", title, ": ", data)


def creature_5e_tools_trait(t: Dict[str, Union[str, Dict[str, Union[Dict[str, Any], str]]]]) -> Dict[str, Any]:
    name = t.get("name", "")
    name = re.sub(recharge_regex, recharge_replace, name)
    if t.get("name", "") != name or "Recharges" in name:
        limit = 1
    else:
        limit = 0
        match = re.search(r"(\d+)/Day", name)
        if match is not None:
            limit = int(match.group(1))

    spells = []
    return {
        "name": name,
        "description": creature_5e_description(None, t["entries"], spells),
        "limit": limit,
        "spells": spells
    }


spell_level_match = {
    "0": "Cantrip",
    "will": "At will"
}


def creature_spells_5e_tools(spellcasting: List[Dict[str, Any]]) -> List[Dict[str, Any]]:
    spell_levels = []
    for t in spellcasting:

        # Daily innate spell
        for daily_level, spells in t.get("daily", {}).items():
            match = re.match(r"(\d+)(e)?", daily_level)
            day_limit = int(match.group(1))
            limit = day_limit
            suffix = ""
            if match.group(2):  # Limit by spell in the list
                limit *= len(spells)
                suffix = " each"
            spell_levels.append({
                "name": f"{day_limit}/day{suffix}",
                "spells": [s.replace("\"", "'") for s in spells if isinstance(s, str)],
                "limit": limit
            })

        # At will innate spells
        if t.get("will") is not None:
            spell_levels.append({
                "name": "At will",
                "spells": [s.replace("\"", "'") for s in t["will"] if isinstance(s, str)],
                "limit": 0
            })

        # Classical spells and cantrips
        for spell_level, spell_level_desc in t.get("spells", {}).items():
            name = spell_level_match.get(spell_level)
            limit = 0
            if name is None:
                name = f"Level {int(spell_level)}"
                limit = spell_level_desc.get("slots", 0)

            spell_levels.append({
                "name": name,
                "spells": [s.replace("\"", "'") for s in spell_level_desc["spells"] if isinstance(s, str)],
                "limit": limit
            })
    return spell_levels


def default_legendary_header(name: str, legendary_actions: int = 3, is_npc: bool = False) -> List[str]:
    prefix = "The " if not is_npc else ""
    return [f"{prefix}{name} can take {legendary_actions} legendary actions,"
            f" choosing from the options below. Only one legendary action can be used at a time"
            f" and only at the end of another creature's turn."
            f" {prefix}{name} regains spent legendary actions at the start of its turn."]


def creature_5e_tools_lair_actions(lair_actions_5e_tools: List[Union[str, Dict[str, Any]]]) -> List[Dict[str, Any]]:
    if len(lair_actions_5e_tools) <= 1:
        return []

    lair_actions = []
    for lair_action_list in lair_actions_5e_tools[1:]:
        if isinstance(lair_action_list, str):
            spells = []
            lair_actions.append({
                "name": "",
                "description": creature_5e_description(None, lair_action_list, spells),
                "spells": spells
            })
        elif lair_action_list.get("type") == "list":
            for lair_action in lair_action_list.get("items", []):
                if isinstance(lair_action, str):
                    spells = []
                    lair_actions.append({
                        "name": "",
                        "description": "<li>" + creature_5e_description(None, lair_action, spells) + "</li>",
                        "spells": spells
                    })
                elif lair_action.get("type") == "item":
                    spells = []
                    lair_actions.append({
                        "name": lair_action["name"],
                        "description": creature_5e_description(None,
                                                               lair_action.get("entry", lair_action.get("entries")),
                                                               spells),
                        "spells": spells
                    })
                else:
                    raise ValueError("Cannot parse lair action item", lair_action, " of ", lair_actions_5e_tools)
        elif lair_action_list.get("type") == "entries":
            for lair_action in lair_action_list.get("entries", []):
                if isinstance(lair_action, str):
                    spells = []
                    lair_actions.append({
                        "name": lair_action_list.get("name", ""),
                        "description": creature_5e_description(None, lair_action, spells),
                        "spells": spells
                    })
                elif lair_action["type"] == "list":
                    for action in lair_action["items"]:
                        spells = []
                        lair_actions.append({
                            "name": action["name"],
                            "description": creature_5e_description(None, action["entry"], spells),
                            "spells": spells
                        })
                else:
                    spells = []
                    lair_actions.append({
                        "name": lair_action.get("name", ""),
                        "description": creature_5e_description(None,
                                                               {k: v for k, v in lair_action.items() if k != "name"},
                                                               spells),
                        "spells": spells
                    })
        else:
            raise ValueError("Cannot parse lair action", lair_action_list, " of ", lair_actions_5e_tools)

    return lair_actions


def creature_5e_id(creature: Dict[str, Any]) -> FrozenSet[str]:
    return frozenset((creature["name"].lower(), creature["source"]))


def handle_fluff_dependencies(fluff_current_file_data: List[Dict[str, Any]],
                              all_fluff: Dict[FrozenSet[str], Dict[str, Any]]):
    # Save independent fluff entries
    fluff_remaining_file_data = []
    for i, fluff in enumerate(fluff_current_file_data):
        if "_copy" not in fluff:  # Independent fluff
            all_fluff[creature_5e_id(fluff)] = fluff
        else:
            fluff_remaining_file_data.append(fluff)
    fluff_current_file_data = fluff_remaining_file_data

    # Handle dependent fluff entries
    while len(fluff_current_file_data) > 0:
        fluff_remaining_file_data = []
        for i, fluff in enumerate(fluff_current_file_data):
            source_key = creature_5e_id(fluff["_copy"])
            if source_key in all_fluff:
                fluff = merge_copy_with_source(fluff, all_fluff[source_key], [])
                all_fluff[creature_5e_id(fluff)] = fluff
            else:
                fluff_remaining_file_data.append(fluff)

        if len(fluff_current_file_data) == len(fluff_remaining_file_data):
            # We have dependency issues
            raise ValueError(f"Cannot find the sources of remaining fluff {fluff_remaining_file_data}")
        fluff_current_file_data = fluff_remaining_file_data


def find_token(image_folder: str, creature: Dict[str, Any]) -> Optional[str]:
    candidate_path = os.path.join(creature["source"],
                                  creature["name"].replace("\"", "").replace("é", "e").replace("è", "e") + ".png")
    return candidate_path if os.path.exists(os.path.join(image_folder, candidate_path)) else None


def find_images(image_folder: str, fluffs: Dict[FrozenSet[str], Dict[str, Any]],
                reprints: Dict[FrozenSet[str], List[Tuple[str]]], creature: Dict[str, Any]) -> List[str]:
    # Find all the potential sources for the creatures
    sources = [(creature["name"], creature["source"])]
    for source in map(lambda x: x["source"], creature.get("otherSources") or []):
        sources.append((creature["name"], source))
    for reprinted in reprints.get(creature_5e_id(creature)) or []:
        # Only way to discover them it if the name changed from a previous source
        # (because "name" is not kept in otherSources map)
        if reprinted not in sources:
            sources.append(reprinted)
    for name, source in sources:
        # Greatwyrms should take the same image as younger dragons if any
        if "Greatwyrm" in name:
            dragon_type = name.split(" ")[0]
            sources.extend([(f"Ancient {dragon_type} Dragon", "MM"), (f"Ancient {dragon_type} Dragon", "FTD")])

    # Explore both options for any source (direct or indirect)
    image_list = []
    for name, source in sources:
        other_source = copy.deepcopy(creature)
        other_source["name"] = name
        other_source["source"] = source
        # First explore regular fluff images
        fluff = fluffs.get(creature_5e_id(other_source))
        if fluff is not None:
            image_list.extend(map(lambda x: x["href"]["path"], fluff.get("images") or []))
        # Try to find a token
        token = find_token(image_folder, other_source)
        if token is not None:
            image_list.append(token)

    return image_list


def priority(m: Dict[str, Any]):  # Give priority to name
    if "_copy" not in m or m["name"] == "Vistana Assassin":
        return 0
    elif not m.get("isNpc", False):  # NPC likely depends on other creatures
        return 1
    else:
        return 2


def download_creatures(bestiary_folder: str, image_folder: str):
    with open(os.path.join(bestiary_folder, "..", "books.json")) as book_file:
        book_list = json.load(book_file)["book"]
        books = {}
        for book in book_list:
            books[book["id"]] = book
    adventures = {}
    with open(os.path.join(bestiary_folder, "..", "adventures.json")) as book_file:
        book_list = json.load(book_file)["adventure"]
        for book in book_list:
            books[book["id"]] = book
            adventures[book["id"]] = book

    hardcoded_dates = {"PSA": "2017-07-01", "MFF": "2019-12-01", "PSD": "2018-07-01", "PSI": "2016-08-01",
                       "PSK": "2017-02-01", "PSX": "2018-01-01", "PSZ": "2016-04-01", "SADS": "2019-12-12",
                       "ESK": "1900-01-01", "PHB": "1900-01-01", "MM": "1900-01-01"}  # Force them first
    with open(os.path.join(bestiary_folder, "index.json")) as index_file:
        bestiary_filenames = []
        for book_key, bestiary_filename in json.load(index_file).items():
            if book_key[:2] == "UA" or book_key == "Mag":  # Do not consider unearthed arcana
                continue
            elif book_key == "TftYP":
                book_key = "TftYP-AtG"  # There are separated in the adventure json

            if book_key in hardcoded_dates:
                date = hardcoded_dates[book_key]
            else:
                date = books[book_key]["published"]
            timestamp = datetime.datetime.strptime(date, "%Y-%m-%d").timestamp()
            if book_key not in adventures and book_key not in hardcoded_dates:  # References always first
                timestamp /= 2
            bestiary_filenames.append((timestamp, bestiary_filename))

    # By ordering the bestiary files, only the most recent version of a creature is kept
    # since it will overwrite the previous ones
    bestiary_filenames.sort()
    bestiary_filenames = [filename for _, filename in bestiary_filenames]

    # Actual parsing of the bestiary
    names = {}
    creatures = {}
    fluffs = {}
    traits = trait_list(bestiary_folder)
    reprints = {}
    for bestiary_filename in bestiary_filenames:
        with open(os.path.join(bestiary_folder, bestiary_filename)) as bestiary_file:
            bestiary_content = json.load(bestiary_file)

        # Lair actions and regional effects
        with open(os.path.join(bestiary_folder, "legendarygroups.json")) as legendary_group_file:
            legendary_groups = json.load(legendary_group_file)["legendaryGroup"]

        # Note and image info
        fluff_file_name = os.path.join(bestiary_folder, "fluff-" + bestiary_filename)
        if os.path.exists(fluff_file_name):
            with open(fluff_file_name) as fluff_file:
                fluff_content = json.load(fluff_file)["monsterFluff"]
        else:
            fluff_content = []
        handle_fluff_dependencies(fluff_content, fluffs)

        monsters = bestiary_content.get("monster", [])
        if "bestiary-mm.json" in bestiary_filename:
            monsters.append(commoner)

        # Give the priority to creatures that are not based on others

        monsters = [(priority(m), m) for m in monsters]
        monsters.sort(key=lambda x: x[0])

        for _, creature_5e_tools in monsters:
            names[creature_5e_tools["name"]] = creature_5e_tools["source"]

            group = creature_5e_tools.get("legendaryGroup", {}).get("name")
            if creature_5e_tools.get("_copy"):
                # Creatures that are incomplete and based on previous ones
                to_copy_key = creature_5e_id(creature_5e_tools["_copy"])
                source = creatures.get(to_copy_key)
                if not source:
                    raise ValueError(f"The creature {to_copy_key} is not yet present for {creature_5e_tools}")
                creature_5e_tools = merge_copy_with_source(creature_5e_tools, source, traits)
            elif creature_5e_tools.get("_trait"):
                raise ValueError(f"The creature {creature_5e_tools['name']}"
                                 f" has generic traits but no creature to copy from")

            creatures[creature_5e_id(creature_5e_tools)] = creature_5e_tools
            try:
                # Recover lair action and regional effect list
                if group:
                    try:
                        group = next(filter(lambda x: x["name"] == group, legendary_groups))
                    except StopIteration:
                        group = {}
                        if "legendary" not in creature_5e_tools:
                            raise ValueError(f"The creature {creature_5e_tools['name']}"
                                             f" should read legendary group {group} but we cannot find it")
                    group.setdefault("legendary", []).extend(creature_5e_tools.get("legendary", []))

                new_sources = creature_5e_tools.get("reprintedAs") or []
                for source_5e_str in new_sources:
                    future_source = source_5e_str.split("|")
                    reprints.setdefault(creature_5e_id({"name": future_source[0], "source": future_source[1]}), []) \
                        .append((creature_5e_tools["name"], creature_5e_tools["source"]))

                image_list = find_images(image_folder, fluffs, reprints, creature_5e_tools)
                image_path = image_list[0] if len(image_list) > 0 \
                    else creature_5e_tools.get("image_path")  # from copy if any

                if image_path is not None:
                    image_full_path = os.path.join(image_folder, image_path)
                    assert os.path.exists(image_full_path), \
                        f"The image '{image_path}' of {creature_5e_tools['name']} does not exist"
                    creature_5e_tools["image_path"] = image_path
                else:
                    print(f"[W] The creature {creature_5e_tools['name']}"
                          f" from {creature_5e_tools['source']} has no image")

                creature_5e_tools_variants = [creature_5e_tools]
                if "summonedBySpell" in creature_5e_tools or "summonedByClass" in creature_5e_tools \
                        or ("pbNote" in creature_5e_tools and creature_5e_tools["pbNote"] == "equals your bonus"):
                    creature_5e_tools_variants = get_spell_level_variants(creature_5e_tools) \
                        if "summonedBySpell" in creature_5e_tools \
                        else get_proficiency_bonus_variants(creature_5e_tools)

                for creature_5e_variant in creature_5e_tools_variants:
                    xp, other_xp = creature_cr_5e_tools(creature_5e_variant.get("cr", "0"))

                    try:
                        hp_special = int(creature_5e_variant["hp"].get("special", ""))
                    except ValueError:
                        hp_special = 1
                        if creature_5e_variant["hp"].get("average", -1) == -1:
                            print(f"[E] The creature {creature_5e_variant['name']}"
                                  f" has a weird hit point specification: {creature_5e_variant['hp']}")
                    if len(creature_5e_variant["size"]) > 1:
                        print(f"[E] The creature {creature_5e_variant['name']}"
                              f" has multiple sizes: {creature_5e_variant['size']}")
                    creature = Creature(
                        name=creature_5e_variant["name"].replace("\"", "'"),
                        source=[creature_5e_variant["name"].replace("\"", "'"), creature_5e_variant["source"]],
                        ac=creature_ac_5e_tools(creature_5e_variant["ac"]),
                        armor_desc=creature_armor_desc_5e_tools(creature_5e_variant["ac"]),
                        hpmax=creature_5e_variant["hp"].get("average", hp_special),
                        hpmax_roll=creature_5e_variant["hp"].get("formula",
                                                                 creature_5e_variant["hp"].get("special", None)),
                        xp=xp,
                        other_xp=other_xp,
                        size=creature_5e_variant["size"][-1],  # Biggest size by default
                        type=creature_type_5e_tools(creature_5e_variant["type"]),
                        alignment=creature_alignment_5e_tools(creature_5e_variant.get("alignment", ["U"])),
                        speed=creature_speed_5e_tools(creature_5e_variant["speed"]),
                        strength=creature_5e_variant["str"],
                        dexterity=creature_5e_variant["dex"],
                        constitution=creature_5e_variant["con"],
                        intelligence=creature_5e_variant["int"],
                        wisdom=creature_5e_variant["wis"],
                        charisma=creature_5e_variant["cha"],
                        saving_throws=[save[0].upper() + save[1:] + " " + bonus
                                       for save, bonus in creature_5e_variant.get("save", {}).items()],
                        abilities=creature_skills_5e_tools(creature_5e_variant.get("skill", {})),
                        damage_weaknesses=creature_damage_types_5e_tools("vulnerable",
                                                                         creature_5e_variant.get("vulnerable", [])),
                        damage_resistances=creature_damage_types_5e_tools("resist",
                                                                          creature_5e_variant.get("resist", [])),
                        damage_immunities=creature_damage_types_5e_tools("immune",
                                                                         creature_5e_variant.get("immune", [])),
                        condition_immunities=creature_damage_types_5e_tools("conditionImmune",
                                                                            creature_5e_variant.get("conditionImmune",
                                                                                                    [])),
                        senses=[imperial_to_international_metrics(sense)
                                for sense in creature_5e_variant.get("senses", []) or []] +
                               [f"Passive perception {creature_5e_variant['passive']}"],
                        languages=list(creature_5e_variant.get("languages", []) or []),
                        traits=[creature_5e_tools_trait(t) for t in creature_5e_variant.get("trait", []) or []] +
                               [{"name": t["name"],
                                 "description": imperial_to_international_metrics("\n".join(t["headerEntries"]
                                                                                            + t.get("footerEntries",
                                                                                                    [])))}
                                for t in creature_5e_variant.get("spellcasting", []) or []],
                        spells=creature_spells_5e_tools(creature_5e_variant.get("spellcasting", []) or []),
                        actions=[creature_5e_tools_trait(a) for a in creature_5e_variant.get("action", []) or []],
                        reactions=[creature_5e_tools_trait(a) for a in creature_5e_variant.get("reaction", [])],
                        variant=[creature_5e_tools_trait(a) for a in creature_5e_variant.get("variant", [])],
                        legendary_actions_desc=imperial_to_international_metrics("\n".join(
                            creature_5e_variant.get("legendaryHeader",  # Sometimes special text is available
                                                    default_legendary_header(
                                                        creature_5e_variant["name"].replace("\"", "'"),
                                                        creature_5e_variant.get("legendaryActions", 3),
                                                        creature_5e_variant.get("isNpc", False))))
                                                                                 if creature_5e_variant.get("legendary")
                                                                                 else None),
                        legendary_actions_limit=creature_5e_variant.get("legendaryActions", 3),
                        legendary_actions=[creature_5e_tools_trait(a)
                                           for a in creature_5e_variant.get("legendary", []) or []],
                        mythic_actions_desc="\n".join(creature_5e_variant["mythicHeader"])
                        if "mythicHeader" in creature_5e_variant else None,
                        mythic_actions=[creature_5e_tools_trait(a)
                                        for a in creature_5e_variant.get("mythic", []) or []],
                        lair_actions_desc=imperial_to_international_metrics(group["lairActions"][0])
                        if group and group.get("lairActions") else None,
                        lair_actions=creature_5e_tools_lair_actions(group.get("lairActions", [])) if group else [],
                        regional_effects=creature_5e_tools_lair_actions(
                            group.get("regionalEffects", [])) if group else [],
                        reference_image_path=image_path
                    )

                    if "CW" in creature_5e_variant.get("spellcastingTags", []) or any(map(lambda casting: casting.get("ability", "") == "int", creature_5e_variant.get("spellcasting", []) or [])):
                        # If it has wizard spells or uses intelligence has
                        creature.has_spellbook = True

                    # Write result
                    out_path = os.path.join(CREATURES_DB, creature.name + ".yaml")
                    with open(out_path, "w") as file_obj:
                        yaml.dump(creature.serialize(), file_obj, **YAML_ARGS)
            except Exception as e:
                print("Cannot parse:\n", json.dumps(creature_5e_tools, indent=4))
                raise e


if __name__ == "__main__":
    if len(sys.argv) > 1:
        repo_5e = sys.argv[1]
        download_creatures(os.path.join(repo_5e, "data/bestiary"), os.path.join(repo_5e, "img"))
