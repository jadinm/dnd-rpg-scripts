import os
from typing import List, Dict, Any, Optional

import yaml

from utils import aidedd_text

DB = os.path.dirname(os.path.abspath(__file__))
CREATURES_DB = os.path.join(DB, "creatures")
TRANSLATION_DB = os.path.join(DB, "translations.yaml")

with open(TRANSLATION_DB) as translation_obj:
    translations = yaml.load(translation_obj, Loader=yaml.CSafeLoader)


def get_db_data(creature_name: str) -> Optional[Dict[str, Any]]:
    """Get the creature raw data with this name if in the database"""
    db_data = None
    db_path = os.path.join(CREATURES_DB, creature_name + ".yaml")
    if not os.path.exists(db_path) and creature_name in translations:
        db_path = os.path.join(CREATURES_DB, translations[creature_name] + ".yaml")
    if os.path.exists(db_path):
        with open(db_path) as file_obj:
            db_data = yaml.load(file_obj, Loader=yaml.CSafeLoader)
            db_data["name"] = creature_name  # If it was translated
    return db_data


def get_db_data_by_id(creature_id: str) -> Optional[Dict[str, Any]]:
    for root, dirs, files in os.walk(CREATURES_DB):
        for file in files:
            if ".yaml" == file[-5:] and creature_id == aidedd_text(file[:-5]):
                with open(os.path.join(root, file)) as file_obj:
                    db_data = yaml.load(file_obj, Loader=yaml.CSafeLoader)
                    return db_data
    return None


def iterate_db_creatures() -> List[str]:
    """Get the list of all creature files in the database"""
    db_files = []
    for root, dirs, files in os.walk(CREATURES_DB):
        for file in files:
            if ".yaml" == file[-5:]:
                db_files.append(os.path.join(root, file))
    return db_files


def get_all_creature_names() -> List[str]:
    """Get the list of all creature names in the database"""
    creature_names = []
    for file in iterate_db_creatures():
        file = os.path.basename(file)
        creature_names.append(file[:-5])
    return creature_names
