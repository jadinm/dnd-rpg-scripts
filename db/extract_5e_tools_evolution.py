import copy
import re
from typing import Dict, List, Any, Union

from model.spell import Spell

ca_special_regex = re.compile(r"^(\d+) \+ (?:the level of the spell|PB) \(([\w ]+)\) ?(?:\+ (\d+) \(([\w ]+)\))?$")

hp_for_each_spell_level = re.compile(r"(\d+) for each spell level above (\d+)(?:st|nd|rd|th)")
hp_multiply_class_level = re.compile(r"(\w+) times your \w+ level")
hp_dices_by_class_level = re.compile(r"the [ \w]+ has a number of Hit Dice \[d(\d+)s] equal to your \w+ level")
hp_dices_by_spell_level = re.compile(r"the [ \w]+ has a number of Hit Dice \[d(\d+)s] equal to the level of the spell")
hp_class_level = re.compile(r"your \w+ level")
hp_parsed_formula = re.compile(r"^(\d+) \+ (\d+)(?: \((\d+d\d+)\))?$")

skill_regex = re.compile(r"(([+-]?)\d+) (?:\+|plus) \(?(?:PB|summonSpellLevel)(?: × (\d+))?\)?")
dice_prep_regex = re.compile(r"{@dice ((?:\d+)?d\d+(?: [+-] \d+)?)} (?:\+|plus) (PB|summonSpellLevel)")
dice_regex = re.compile(r"{@(dice|damage) ((?:\d+)?d\d+)(?: ([+-]) (\d+))? \+ (?:PB|summonSpellLevel)}")
hit_regex = re.compile(r"{@hit (-?\d+) \+ (?:PB|summonSpellLevel)}")


class_level_by_spell_level = {
    1: 1,
    2: 3,
    3: 5,
    4: 7,
    5: 9,
    6: 11,
    7: 13,
    8: 15,
    9: 17
}


def recursive_replace(data: Union[Dict, List, str, bool, int], replace_value: int) -> Union[Dict, List, str, bool, int]:
    def parse_dice_text(match: re.Match) -> str:
        extra = replace_value + int(match.group(3) + match.group(4)) if match.group(4) else replace_value
        return f"{{@{match.group(1)} {match.group(2)} {'+' if extra >= 0 else '-'} {extra}}}"

    def parse_hit_text(match: re.Match) -> str:
        return f"{{@hit {int(match.group(1)) + replace_value}}}"

    def parse_skill_text(match: re.Match) -> str:
        factor = int(match.group(3)) if match.group(3) else 1
        new_roll = int(match.group(1)) + factor * replace_value
        return f"{'+' if new_roll >= 0 else '-'}{new_roll}" if match.group(2) else str(new_roll)

    if isinstance(data, str):  # Actually replace text
        data = dice_prep_regex.sub(r"{@dice \1 + \2}", data)  # Move modifier inside dice block
        data = dice_regex.sub(parse_dice_text, data)
        data = hit_regex.sub(parse_hit_text, data)
        data = skill_regex.sub(parse_skill_text, data)
        assert "PB" not in data and "summonSpellLevel" not in data, f"Cannot match '{data}'"
        return data

    # More exploration
    if isinstance(data, int) or isinstance(data, bool):
        return data
    if isinstance(data, list):
        return [recursive_replace(elem, replace_value) for elem in data]
    if isinstance(data, dict):
        return {k: recursive_replace(v, replace_value) for k, v in data.items()}
    raise ValueError(f"{data} is of an unexpected type")


def parse_hp(hp: str, replace_value: int, class_level: int) -> Dict[str, Union[int, str]]:
    def multiply_above_level(match: re.Match) -> str:
        return str(int(match.group(1)) * max(0, replace_value - int(match.group(2))))

    def multiply_class_level(match: re.Match) -> str:
        factors = {"four": 4, "five": 5}
        return str(factors[match.group(1)] * class_level)

    hp = hp_for_each_spell_level.sub(multiply_above_level, hp)
    hp = hp_multiply_class_level.sub(multiply_class_level, hp)
    hp = hp_dices_by_class_level.sub(str(class_level) + r"d\1", hp)
    hp = hp_dices_by_spell_level.sub(str(replace_value) + r"d\1", hp)
    hp = hp_class_level.sub(str(class_level), hp)

    assert "level" not in hp, f"Cannot parse HP specs {hp}"

    # e.g., 40 + 10 (4d6)
    match = hp_parsed_formula.match(hp)
    if match is not None:
        hp = {"average": int(match.group(1)) + int(match.group(2))}
        if match.group(3):
            hp["formula"] = match.group(3)
        return hp

    # More complicated example, such as Fiendish Spirit:
    # 50 (Demon only) or 40 (Devil only) or 60 (Yugoloth only) + 15
    return {
        "special": hp.replace(" + 0", "")
    }


def _substitute_value(creature: Dict[str, Any], replace_value: int, class_level: int) -> Dict[str, Any]:
    # Armor Class
    if isinstance(creature["ac"], list) and isinstance(creature["ac"][0], dict) and "special" in creature["ac"][0]:
        match = ca_special_regex.match(creature["ac"][0]["special"])
        assert match is not None, f'Cannot match "{creature["ac"][0]["special"]}" of creature {creature["name"]}'
        creature["ac"] = [
            {
                "ac": int(match.group(1)) + replace_value,
                "from": [match.group(2)]
            }
        ]
        if match.group(3):
            creature["ac"].append({
                "ac": creature["ac"][0]["ac"] + int(match.group(3)),
                "from": [match.group(2)],
                "condition": match.group(4)
            })

    # HP
    if isinstance(creature["hp"], dict) and "special" in creature["hp"]:
        creature["hp"] = parse_hp(creature["hp"]["special"], replace_value, class_level)

    # CR
    creature["cr"] = str(class_level) if class_level > 0 else str(class_level_by_spell_level[replace_value])

    # Go through the rest of data (i.e., descriptions, skills and saves)
    return recursive_replace(creature, replace_value)


def _ordinal_suffix(number: int) -> str:
    suffix = ["th", "st", "nd", "rd"]
    if number % 10 in [1, 2, 3] and number not in [11, 12, 13]:
        return suffix[number % 10]
    else:
        return suffix[0]


def get_spell_level_variants(base: Dict[str, Any]) -> List[Dict[str, Any]]:
    creature_5e_tools_variants = []
    spell = Spell(f'{{@spell {base["summonedBySpell"]}}}')
    for level in range(spell.level, 10):
        creature = copy.deepcopy(base)
        creature["name"] = f"{base['name']} ({level}{_ordinal_suffix(level)}-Level Spell)"
        creature_5e_tools_variants.append(_substitute_value(creature, level, 0))

    return creature_5e_tools_variants


def get_proficiency_bonus_variants(base: Dict[str, Any]) -> List[Dict[str, Any]]:
    creature_5e_tools_variants = []
    for level in range(2, 21):
        creature = copy.deepcopy(base)
        creature["name"] = f"{base['name']} (Class Level {level})"
        creature_5e_tools_variants.append(_substitute_value(creature, ((level - 1) // 4) + 2, level))

    return creature_5e_tools_variants
