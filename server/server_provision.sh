. "${HOME}/.bashrc"
REMOTE_SCRIPTS="${HOME}/dnd-rpg-scripts"

cd "${REMOTE_SCRIPTS}" || exit 1
pkill -9 gunicorn
"${HOME}/miniconda3/bin/pip" install -r requirements.txt
sleep 3
export CAMPAIGN="${HOME}/${1}"
export IMAGE_DIR="${HOME}/${2}"
screen -m -d -L "${HOME}/miniconda3/bin/gunicorn" -b 0.0.0.0:3031 --worker-class eventlet -w 1 fight_tracker:app --access-logfile access.log --log-file error.log --capture-output
cd - || exit 1
