import os
import shlex
import subprocess
import sys
import time

SSH_KEY = "~/.ssh/perso_rsa"
USER = "mj"
REMOTE_SCRIPTS = "dnd-rpg-scripts"
CAMPAIGN_DIR = sys.argv[2] if len(sys.argv) > 2 else ""
IMAGE_DIR = "5etools-mirror-1.github.io/img"

MIXXX_BROADCAST = os.path.expanduser("~/.mixxx/broadcast_profiles/"
                                     "AWS icecast2.bcp.xml")


def stop_local_clients():
    cmd = "pkill mixxx"
    subprocess.run(shlex.split(cmd))
    cmd = "pkill qjackctl"
    subprocess.run(shlex.split(cmd))


def run_ssh(host, remote_cmd, ssh_key=SSH_KEY, remote_user=USER):
    cmd = f"ssh -i {ssh_key} " \
          f"-o StrictHostKeyChecking=no " \
          f"{remote_user}@{host} \"{remote_cmd}\""
    print(cmd)
    subprocess.check_call(shlex.split(cmd))


def can_ssh(host, ssh_key=SSH_KEY, remote_user=USER):
    try:
        run_ssh(host, "ls", ssh_key=ssh_key, remote_user=remote_user)
        return True
    except subprocess.CalledProcessError:
        return False


def setup_server(host, ssh_key=SSH_KEY, remote_user=USER):
    # Update all repositories

    for directory in [REMOTE_SCRIPTS, CAMPAIGN_DIR, IMAGE_DIR]:
        run_ssh(host, f"cd {directory} && git pull", ssh_key=ssh_key,
                remote_user=remote_user)

    # Sync icecast and web server configuration

    run_ssh(host,
            f"cd {REMOTE_SCRIPTS}/server"
            f" && ./server_provision.sh '{CAMPAIGN_DIR}' '{IMAGE_DIR}'",
            ssh_key=ssh_key, remote_user=remote_user)
    print("Icecast2 & web servers up")

    # Update Mixxx configuration

    with open(MIXXX_BROADCAST) as fileobj:
        content = fileobj.readlines()

    with open(MIXXX_BROADCAST, "w") as fileobj:
        new_content = []
        for line in content:
            if "<Host>" in line:
                line = f"    <Host>{host}</Host>\n"
            new_content.append(line)
        fileobj.writelines(new_content)

    # Start Jackd and Mixxx
    if len(sys.argv) > 3 and sys.argv[3] == "music-start":
        cmd = "qjackctl -s"
        print(cmd)
        subprocess.Popen(shlex.split(cmd), close_fds=True)
        time.sleep(5)
        print("Jackd server up")

        cmd = "mixxx"
        print(cmd)
        subprocess.Popen(shlex.split(cmd), close_fds=True)


if __name__ == "__main__" and sys.argv[1] == "start":
    print("Stopping local clients")
    stop_local_clients()

    setup_server("tracker.spectre.lol")
