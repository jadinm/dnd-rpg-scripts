"""
Script to launch or stop the radio and update its configuration
This script is not compatible with Windows
"""

import sys
import time

import boto3

from server import can_ssh, setup_server, stop_local_clients

EC2_INSTANCE = "i-05adcd2ad21fabc66"
SSH_KEY = "~/.ssh/aws-perso.pem"
USER = "ubuntu"

session = boto3.Session(profile_name='perso', region_name='eu-west-3')
ec2 = session.resource('ec2')
instance = ec2.Instance(EC2_INSTANCE)  # type: boto3

print("Stopping local clients")
stop_local_clients()

if sys.argv[1] == "stop":

    state = instance.state["Name"]
    if state != "stopped" and state != "stopping":
        print("Stopping instance")
        instance.stop()
    while ec2.Instance(EC2_INSTANCE).state["Name"] != "stopped":
        print(f"Instance in state {ec2.Instance(EC2_INSTANCE).state['Name']}")
        time.sleep(5)
    print("Instance stopped")
    sys.exit(0)

# Start machine on aws if not already started

if instance.state["Name"] != "running" and instance.state["Name"] != "pending":
    print("Starting instance")
    instance.start()

while ec2.Instance(EC2_INSTANCE).state["Name"] != "running":
    print(f"Instance in state {ec2.Instance(EC2_INSTANCE).state['Name']}")
    time.sleep(5)
print("Instance started")

while not can_ssh(instance.public_dns_name, ssh_key=SSH_KEY, remote_user=USER):
    print("Instance ssh server is not up yet")
    time.sleep(5)
print("SSH server up")

# Setup server
setup_server(instance.public_dns_name, ssh_key=SSH_KEY, remote_user=USER)

# Get DNS name and urls

print("\nDNS domain name:")
print(instance.public_dns_name)
print("Full URL")
print(f"http://{instance.public_dns_name}/")
print(f"http://{instance.public_dns_name}:8000/stream")
