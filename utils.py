import os
import random

import unidecode
import yaml

YAML_ARGS = {"allow_unicode": True, "sort_keys": False,
             "Dumper": yaml.CSafeDumper}
SOURCE_DIR = os.path.dirname(os.path.abspath(__file__))
DEFAULT_CAMPAIGN = os.path.join(SOURCE_DIR, "campaign/example")


def aidedd_text(text):
    if text is None:
        return None
    t = unidecode.unidecode(text.lower().replace(" ", "-").replace("'", "-")
                            .replace("/", "-").replace(",", "")
                            .replace("(", "").replace(")", ""))
    return t


def is_inside_dir(absolute_path: str, directory_name: str):
    """
    :param absolute_path: An absolute file or directory path
    :param directory_name: A directory name
    :return: True if the absolute_path points to a directory with the
      directory_name or if this path is inside a directory with this name
    """
    current_path = absolute_path
    while current_path != "/":
        if os.path.basename(current_path) == directory_name:
            return True
        current_path = os.path.dirname(current_path)
    return False


def roll(dice=20, modifier=0):
    return random.randint(1, dice) + int(modifier)
